import React from 'react';
import './styles/map.css';
import './static/fonts/fonts.css';
import { ThemeProvider } from "@material-ui/core/styles";
import MapView from './views/MapView';
import GlobalView from './views/GlobalView';
import theme from "./config/Theme.js";
import MapModel from "./models/MapModel";
import {
    Route,
    BrowserRouter
  } from "react-router-dom";

const model = new MapModel();
model.fetchWorldData();

const App = (props) =>
<ThemeProvider theme={theme()}>
    <BrowserRouter basename="/maps">
         
        <Route path="/" exact>
            <MapView context={model}/>
        </Route>
        <Route path="/world" exact>
            <GlobalView context={model}/>
        </Route> 
    </BrowserRouter>
</ThemeProvider>
;
export default App;
