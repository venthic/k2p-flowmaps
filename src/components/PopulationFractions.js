
import * as React from "react";
import { observer } from "mobx-react-lite";
import AreasDialog from "./dialogs/AreasDialog";
import { Grid, Typography, Tooltip, IconButton, Card, CardHeader, CardContent, CardActions } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import { makeStyles } from '@material-ui/core/styles';
import AddTwoToneIcon from '@material-ui/icons/AddTwoTone';
import RemoveTwoToneIcon from '@material-ui/icons/RemoveTwoTone';
import ChildCareIcon from '@material-ui/icons/ChildCare';
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';
import LocationCityIcon from '@material-ui/icons/LocationCity';
import NumberFormat from 'react-number-format';
import TextField from '@material-ui/core/TextField';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 6.5 + ITEM_PADDING_TOP,
      maxWidth: 400
    },
  },
};

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    display: 'flex',
    height: 224,
  },
  tabs: {
    borderRight: `1px solid ${theme.palette.divider}`,
  },
  formControl: {
    width: '100%'
  },
  enhancedBox: {
    padding: 20,
    marginTop: 10,
    marginBottom: 10,
    borderRadius: 2,
    backgroundColor: '#eee',
    boxShadow: '0 1px 15px rgba(0,0,0,.04), 0 1px 6px rgba(0,0,0,.04)',
    border: '1px solid #e7e7e7'
  }
}));


const PopulationFractions = observer(({ scenario }) => {

  const classes = useStyles();

  return <div style={{ width: "100%" }}>
    <Box className={classes.enhancedBox}>
      Viewing aggregates for: {(
        scenario.selectedAreasDialogValues[scenario.activeTab].length == 0 ||
        scenario.selectedAreasDialogValues[scenario.activeTab].length == scenario.records.length) ?
        <b>all areas</b> :
        <b>{scenario.selectedAreasDialogValues[scenario.activeTab].length} areas</b>} (<a href="#" onClick={() => { scenario.setSelectedAreasDialogTab(scenario.activeTab) }}>change</a>)
      <div style={{textAlign:'center', paddingTop:'10px'}}>
         
        <TextField
          id="outlined-number"
          margin="dense"
          onChange= {(e) => scenario.setPercentageAmount(e.target.value/100)}
          value={Math.round(scenario.percentageAmount*100)}
          label="Modified % amount"
          type="number"
          InputProps={{ inputProps: { min: 0, max: 100 }, style:{textAlign:'center'} }}
          style={{width: '150px'}}
          className='incrementInput'
          InputLabelProps={{
            shrink: true,
          }}
          variant="outlined"
        /> 
      </div>
    </Box>
    <Grid container spacing={1} className="population-all">

      <Grid item md={4} lg={4} xs={12}>
        <Card className="populationCard">
          <CardHeader className="populationCardHeader" title={<h3><SupervisorAccountIcon /><br />Population</h3>} />
          <CardContent className="populationCardContent">
            {(
              scenario.selectedAreasDialogValues[scenario.activeTab].length != 0 &&
              scenario.selectedAreasDialogValues[scenario.activeTab].length != scenario.records.length) ?
              <div>
                <NumberFormat value={Math.round(scenario.selectedSum('population'))} displayType={'text'} thousandSeparator={true} renderText={value => <Typography variant='h3'>{value}</Typography>} />
                <NumberFormat value={Math.round(scenario.sum('population'))} displayType={'text'} thousandSeparator={true} renderText={value => <Typography className="statsSub" variant='subtitle1'><b>Total: </b>{value}</Typography>} />
              </div>
              : <NumberFormat value={Math.round(scenario.sum('population'))} displayType={'text'} thousandSeparator={true} renderText={value => <Typography variant='h3'>{value}</Typography>} />

            }
          </CardContent>
          <CardActions disableSpacing className="populationActions">
            <Tooltip title={"Decrease by "+Math.round(scenario.percentageAmount*100)+"%"}>
              <IconButton aria-label="decrease" disabled={Math.round(scenario.sum('population') - scenario.sum('population') * scenario.percentageAmount) <= scenario.records.length} onClick={() => { scenario.decreaseSum('population', true) }}>
                <RemoveTwoToneIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title={"Increase by "+Math.round(scenario.percentageAmount*100)+"%"}>
              <IconButton aria-label="increase" onClick={() => { scenario.increaseSum('population', true) }}>
                <AddTwoToneIcon />
              </IconButton>
            </Tooltip>
          </CardActions>
        </Card>
      </Grid>
      <Grid item md={4} lg={4} xs={12}>
        <Card className="populationCard">
          <CardHeader className="populationCardHeader" title={<h3><LocationCityIcon /><br />Urbanization</h3>} />
          <CardContent className="populationCardContent">
            {(
              scenario.selectedAreasDialogValues[scenario.activeTab].length != 0 &&
              scenario.selectedAreasDialogValues[scenario.activeTab].length != scenario.records.length) ?
              <div>
                <NumberFormat value={Number.parseFloat(scenario.avg(['fraction_urban_pop'], true) * 100).toFixed(2)} displayType={'text'} suffix={'%'} thousandSeparator={true} renderText={value => <Typography variant='h3'>{value}</Typography>} />
                <NumberFormat value={Number.parseFloat(scenario.avg(['fraction_urban_pop'], false) * 100).toFixed(2)} displayType={'text'} suffix={'%'} thousandSeparator={true} renderText={value => <Typography className="statsSub" variant='subtitle1'><b>Total: </b>{value}</Typography>} />

              </div>
              : <NumberFormat value={Number.parseFloat(scenario.avg(['fraction_urban_pop'], false) * 100).toFixed(2)} displayType={'text'} suffix={'%'} thousandSeparator={true} renderText={value => <Typography variant='h3'>{value}</Typography>} />
            }
          </CardContent>

          <CardActions disableSpacing className="populationActions">
          <Tooltip title={"Decrease by "+Math.round(scenario.percentageAmount*100)+"%"}>
              <IconButton aria-label="decrease" disabled={scenario.avg(['fraction_urban_pop'], false) < scenario.percentageAmount} onClick={() => { scenario.decreaseAvg('fraction_urban_pop') }}>
                <RemoveTwoToneIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title={"Increase by "+Math.round(scenario.percentageAmount*100)+"%"}>
              <IconButton aria-label="increase" disabled={scenario.avg(['fraction_urban_pop'], false) > 1 - scenario.percentageAmount} onClick={() => { scenario.increaseAvg('fraction_urban_pop') }}>
                <AddTwoToneIcon />
              </IconButton>
            </Tooltip>
          </CardActions>
        </Card>
      </Grid>
      <Grid item md={4} lg={4} xs={12}>
        <Card className="populationCard">
          <CardHeader className="populationCardHeader" title={<h3><ChildCareIcon /><br />Aged under 5</h3>} />
          <CardContent className="populationCardContent">
            {(
              scenario.selectedAreasDialogValues[scenario.activeTab].length != 0 &&
              scenario.selectedAreasDialogValues[scenario.activeTab].length != scenario.records.length) ?
              <div>
                <NumberFormat value={Number.parseFloat(scenario.avg(['fraction_pop_under5'], true) * 100).toFixed(2)} displayType={'text'} thousandSeparator={true} suffix={'%'} renderText={value => <Typography variant='h3'>{value}</Typography>} />
                <NumberFormat value={Number.parseFloat(scenario.avg(['fraction_pop_under5'], false) * 100).toFixed(2)} displayType={'text'} thousandSeparator={true} suffix={'%'} renderText={value => <Typography className="statsSub" variant='subtitle1'><b>Total: </b>{value}</Typography>} />
              </div>
              : <NumberFormat value={Number.parseFloat(scenario.avg(['fraction_pop_under5'], false) * 100).toFixed(2)} displayType={'text'} thousandSeparator={true} suffix={'%'} renderText={value => <Typography variant='h3'>{value}</Typography>} />
            }
          </CardContent>
          <CardActions disableSpacing className="populationActions">

          <Tooltip title={"Decrease by "+Math.round(scenario.percentageAmount*100)+"%"}>
              <IconButton aria-label="decrease" disabled={scenario.avg(['fraction_pop_under5'], false) < scenario.percentageAmount} onClick={() => { scenario.decreaseAvg('fraction_pop_under5') }}>
                <RemoveTwoToneIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title={"Increase by "+Math.round(scenario.percentageAmount*100)+"%"}>
              <IconButton aria-label="increase" disabled={scenario.avg(['fraction_pop_under5'], false) > 1 - scenario.percentageAmount} onClick={() => { scenario.increaseAvg('fraction_pop_under5') }}>
                <AddTwoToneIcon />
              </IconButton>
            </Tooltip>
            
          </CardActions>
        </Card>
      </Grid>

    </Grid>
    { (scenario.records ) &&
    <AreasDialog scenario={scenario} />
    }
  </div >
});

export default PopulationFractions;

