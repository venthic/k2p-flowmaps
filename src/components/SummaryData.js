
import React from "react";
import { observer } from "mobx-react-lite";
import SummaryPopulationChart from './charts/SummaryPopulationChart.js';
import SummarySanitationChart from './charts/SummarySanitationChart.js';
import SummaryTreatmentCharts from './charts/SummaryTreatmentCharts.js';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import SaveAltRoundedIcon from '@material-ui/icons/SaveAltRounded';


const SummaryData = observer(({ context }) => {
    const map = context; 

    function sumDifference(column) {
      var diff = Math.round(( Math.round(map.scenarios[map.activeScenario].payload.sum(column)) -Math.round(map.scenarios[map.baselineScenario].payload.sum(column)) )/Math.round(map.scenarios[map.baselineScenario].payload.sum(column))*100)
      if (diff === 0)
          return '(no change)';
      return (diff > 0) ? '(+'+diff+'%)': '('+diff+'%)'
  }

  function percentDifference(columns, populationWeighted) {
      
      var diff = ((map.scenarios[map.activeScenario].payload.avg(columns, false)*100 - map.scenarios[map.baselineScenario].payload.avg(columns, false)*100)).toFixed(2);
      
      if ( diff >= 0 && diff < 0.001) {
          return '(no change)';
      }
      return (diff > 0) ? '(+'+diff+'%)': '('+diff+'%)'
  }

  function weightedDifference(columns, populationWeighted) {
      var diff = 0;
      
      diff = map.scenarios[map.activeScenario].payload.populationWeightedAvg(columns, "both", false, []) - map.scenarios[map.baselineScenario].payload.populationWeightedAvg(columns, "both", false, []);

      if ( diff >= 0 && diff < 0.001 ) {
          return '(no change)';
      }
      return (diff > 0) ? '(+'+diff.toFixed(2)+'%)': '('+diff.toFixed(2)+'%)'
  }

  function logDifference(columns, populationWeighted) {
      var active = -Math.log10(1 - floatValueFormat(map.scenarios[map.activeScenario].payload.avg(columns, false))).toFixed(1);
      var baseline = -Math.log10(1 - floatValueFormat(map.scenarios[map.baselineScenario].payload.avg(columns, false))).toFixed(1);
      var diff = active - baseline;
      if ( diff >= 0 && diff < 0.001 ) {
          return '(no change)';
      }
      return (diff > 0) ? '(+'+diff+' log)': '('+diff+' log)'
  }

  function floatValueFormat(value) {
      var floatValue = value;
      if (value >= 0.999999) {
        floatValue = Number(value.toPrecision(7));
      } else if ( value >= 0.99999) {
        floatValue = Number(value.toPrecision(6));
      } else if ( value >= 0.9999) {
        floatValue = Number(value.toPrecision(5));
      } else if ( value >= 0.999) {
        floatValue = Number(value.toPrecision(4));
      } else if (value >= 0.99) {
        floatValue = Number(value.toPrecision(3));
      } else {
        floatValue = Number(value.toPrecision(2));
      }
      return floatValue;
    }
    
    return <div className='inputSummary'>
        <Grid container>
            <Grid item xs={12} sm={4}><h2>Input summary</h2></Grid>
            <Grid item xs={12} sm={8} style={{textAlign:'right'}}>
                <ButtonGroup size={'sm'}>
                        <Button variant="outlined" disabled={map.scenarios.filter(scenario => scenario.payload.geoTiff !== '').length === 0} startIcon={<SaveAltRoundedIcon/>} onClick={() => {map.downloadFromServer()}} color="primary">Download all</Button>
                        {/* <Button variant="outlined" onClick={() => {map.setBaselineScenario(map.activeScenario)}} disabled={map.activeScenario === map.baselineScenario} startIcon={<FiberManualRecordTwoToneIcon />} color="secondary">Set as baseline</Button> */}
                    </ButtonGroup>
            </Grid>
        </Grid>
      
      
        <h3>Population</h3>
        <SummaryPopulationChart context={map}/>
        {/* <TableContainer className="summaryCard" component={Paper}>
            <Table size="small">
                <TableBody>
                
                    <TableRow key='population'>
                        <TableCell component="th" scope="row">
                            Population
                        </TableCell>
                        <TableCell align="right">
                            <NumberFormat value={Math.round(map.scenarios[map.activeScenario].payload.sum('population'))} displayType={'text'} thousandSeparator={true} renderText={value => <span className="value">{value}</span>} />
                            <br/>
                            {
                            (map.activeScenario !== map.baselineScenario) &&
                                <span className="difference"> { (sumDifference('population'))}</span>
                            }
                        </TableCell>
                    </TableRow>
                    <TableRow key='urbanization'>
                        <TableCell component="th" scope="row">
                            Urbanization
                        </TableCell>
                        <TableCell align="right">
                            <NumberFormat value={Number.parseFloat(map.scenarios[map.activeScenario].payload.avg(['fraction_urban_pop'], false) * 100).toFixed(2)} displayType={'text'} suffix={'%'} thousandSeparator={true} renderText={value => <span className="value">{value}</span>} />
                            <br/>
                            {
                                (map.activeScenario !== map.baselineScenario) &&
                                    <span className="difference"> {percentDifference(['fraction_urban_pop'], false)}</span>
                            }
                        </TableCell>
                    </TableRow>
                    <TableRow key='under5'>
                        <TableCell component="th" scope="row">
                            Aged under 5
                        </TableCell>
                        <TableCell align="right">
                            <NumberFormat value={Number.parseFloat(map.scenarios[map.activeScenario].payload.avg(['fraction_pop_under5'], false) * 100).toFixed(2)} displayType={'text'} suffix={'%'} thousandSeparator={true} renderText={value => <span className="value">{value}</span>} />
                            <br/>
                            {
                        (map.activeScenario !== map.baselineScenario) &&
                            <span className="difference"> {percentDifference(['fraction_pop_under5'], false)}</span>
                    }
                        </TableCell>
                    </TableRow>
                </TableBody>
            </Table>
            </TableContainer> */}
            
                    <h3>Onsite sanitation</h3>
                    <SummarySanitationChart context={map}/>
                                    {/* <TableContainer className="summaryCard" component={Paper}>
                                        <Table size="small">
                                            <TableBody>
                                            
                                                <TableRow key='no-facility'>
                                                    <TableCell component="th" scope="row">
                                                        No Facility
                                                    </TableCell>
                                                    <TableCell align="right">
                                                    <NumberFormat value={Number.parseFloat(map.scenarios[map.activeScenario].payload.populationWeightedAvg(toiletColumns[0].columns, "both", false, [])).toFixed(2)} displayType={'text'} suffix={'%'} thousandSeparator={true} renderText={value => <span className="value">{value}</span>} />
                                                        <br/>
                                                        {
                                                            (map.activeScenario !== map.baselineScenario) &&
                                                                <span className="difference"> { (weightedDifference(toiletColumns[0].columns, true))}</span>
                                                        }
                                                    </TableCell>
                                                </TableRow>
                                                <TableRow key='dry-toilets'>
                                                    <TableCell component="th" scope="row">
                                                        Dry toilets
                                                    </TableCell>
                                                    <TableCell align="right">
                                                    <NumberFormat value={Number.parseFloat(map.scenarios[map.activeScenario].payload.populationWeightedAvg(toiletColumns[1].columns, "both", false, [])).toFixed(2)} displayType={'text'} suffix={'%'} thousandSeparator={true} renderText={value => <span className="value">{value}</span>} />
                                                        <br/>
                                                        {
                                                    (map.activeScenario !== map.baselineScenario) &&
                                                        <span className="difference"> {weightedDifference(toiletColumns[1].columns, true)}</span>
                                                }
                                                    </TableCell>
                                                </TableRow>
                                                <TableRow key='composting-toilets'>
                                                    <TableCell component="th" scope="row">
                                                        Composting Toilets
                                                    </TableCell>
                                                    <TableCell align="right">
                                                <NumberFormat value={Number.parseFloat(map.scenarios[map.activeScenario].payload.populationWeightedAvg(toiletColumns[2].columns, "both", false, [])).toFixed(2)} displayType={'text'} suffix={'%'} thousandSeparator={true} renderText={value => <span className="value">{value}</span>} />
                                                        <br/>
                                                        {
                                                    (map.activeScenario !== map.baselineScenario) &&
                                                    <span className="difference"> {weightedDifference(toiletColumns[2].columns, true)}</span>
                                                }
                                                    </TableCell>
                                                </TableRow>
                                                <TableRow key='flush-toilets-onsite'>
                                                    <TableCell component="th" scope="row">
                                                        Flush Toilets (onsite)
                                                    </TableCell>
                                                    <TableCell align="right">
                                                    <NumberFormat value={Number.parseFloat(map.scenarios[map.activeScenario].payload.populationWeightedAvg(toiletColumns[3].columns, "both", false, [])).toFixed(2)} displayType={'text'} suffix={'%'} thousandSeparator={true} renderText={value => <span className="value">{value}</span>} />
                                                        <br/>
                                                        {
                                                    (map.activeScenario !== map.baselineScenario) &&
                                                        <span className="difference"> {weightedDifference(toiletColumns[3].columns, true)}</span>
                                                }
                                                    </TableCell>
                                                </TableRow>
                                                <TableRow key='flush-toilets-sewered'>
                                                    <TableCell component="th" scope="row">
                                                        Flush Toilets (sewered)
                                                    </TableCell>
                                                    <TableCell align="right">
                                                    <NumberFormat value={Number.parseFloat(map.scenarios[map.activeScenario].payload.populationWeightedAvg(toiletColumns[4].columns, "both", false, [])).toFixed(2)} displayType={'text'} suffix={'%'} thousandSeparator={true} renderText={value => <span className="value">{value}</span>} />
                                                        <br/>
                                                        {
                                                    (map.activeScenario !== map.baselineScenario) &&
                                                        <span className="difference"> {weightedDifference(toiletColumns[4].columns, true)}</span>
                                                }   
                                                    </TableCell>
                                                </TableRow>
                                            </TableBody>
                                        </Table>
                                        </TableContainer> */}
                                        <h3>Sewage & Faecal Sludge Management</h3>
                    <SummaryTreatmentCharts context={map}/>
            {/* <TableContainer className="summaryCard" component={Paper}>
                    <Table size="small">
                        <TableBody>
                        
                            <TableRow key='faecal-sludge'>
                                <TableCell component="th" scope="row">
                                    Faecal sludge safely managed
                                </TableCell>
                                <TableCell align="right">
                                    <NumberFormat value={Number.parseFloat(map.scenarios[map.activeScenario].payload.avg(["fecalSludgeTreated_urb", "coverBury_rur"], false)*100).toFixed(2)} displayType={'text'} suffix={'%'} thousandSeparator={true} renderText={value => <span className="value">{value}</span>} />
                                    <br/>
                                    {
                                    (map.activeScenario !== map.baselineScenario) &&
                                        <span className="difference"> {percentDifference(["fecalSludgeTreated_urb", "coverBury_rur"], false)}</span>
                                    }
                                </TableCell>
                            </TableRow>
                            <TableRow key='sewage-treated'>
                                <TableCell component="th" scope="row">
                                    Sewage Treated
                                </TableCell>
                                <TableCell align="right">
                                    <NumberFormat value={Number.parseFloat(map.scenarios[map.activeScenario].payload.avg(["sewageTreated_urb", "sewageTreated_rur"], false) * 100).toFixed(2)} displayType={'text'} suffix={'%'} thousandSeparator={true} renderText={value => <span className="value">{value}</span>} />
                                    <br/>
                                    {
                                        (map.activeScenario !== map.baselineScenario) &&
                                            <span className="difference"> {percentDifference(["sewageTreated_urb", "sewageTreated_rur"], false)}</span>
                                    }
                                </TableCell>
                            </TableRow>
                            <TableRow key='pathogen-reduction'>
                                <TableCell component="th" scope="row">
                                    Overall Pathogen Reduction
                                </TableCell>
                                <TableCell align="right">
                                    {-Math.log10(1 - floatValueFormat(map.scenarios[map.activeScenario].payload.avg(['fRemoval_treatment'], false))).toFixed(1)}
                                    <br/>
                                    {
                                (map.activeScenario !== map.baselineScenario) &&
                                    <span className="difference"> {logDifference(['fRemoval_treatment'], false)}</span>
                            }
                                </TableCell>
                            </TableRow>
                        </TableBody>
                    </Table>
                    </TableContainer> */}
        </div>
                   
                
});

export default SummaryData;