import React, {useState} from "react";
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

const  Tutorial = (props) => {
    var {tool, model, data, bugs} = props;
    const classes = useStyles();
    const [activeStep, setActiveStep] = useState(0);
    const steps = getSteps();
  
    const handleNext = () => {
      setActiveStep((prevActiveStep) => prevActiveStep + 1);
    };
  
    const handleBack = () => {
      setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };
  
    const handleReset = () => {
      setActiveStep(0);
    };

    const opts = {
        height: '351', 
        width: '576',
        playerVars: {
          autoplay: 0,
        }
    };
    return  ( tool)  ? <div className={classes.root}>
            <Stepper activeStep={activeStep} alternativeLabel>
            {steps.map((label) => (
                <Step key={label}>
                    <StepLabel>{label}</StepLabel>
                </Step>
            ))}
            </Stepper>
            <div>
                <Box>
                    {activeStep === steps.length ? (
                        <div>
                            <Box display="flex" justifyContent="center">
                                <Typography variant="h3" className={classes.instructions}>You are now ready to start using the Pathogen Flow & Mapping Tool!</Typography>
                            </Box>
                            <Box display="flex" justifyContent="center">
                                <Button variant="contained" color="primary" onClick={handleReset}>Start Over</Button>
                            </Box>
                            
                        </div>
                    ) : (
                        <div>
                            
                            <Typography align='justify' className={classes.instructions}>{getStepContent(activeStep)}</Typography>
                            <div>
                                <Button disabled={activeStep === 0} onClick={handleBack} className={classes.backButton}>Back</Button>
                                <Button variant="contained" color="primary" onClick={handleNext}>
                                    {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
                                </Button>
                            </div>
                        </div>
                    )}
                </Box>
            </div>
        </div>:
        ((bugs) ? 
       
        <div><h3>Known issues:</h3>
            <ul>
                <li>Using the search function to select whole continents might result in model errors.</li>
                <li>The Onsite Sanitation buttons currently round values to a 0.01 precision, often not allowing users to fully set fractions to 0.</li>
                <li>Small screen resolutions might obstruct some of the tool functions and views.</li>
                <li>The tool can support a small number of concurrent users. For more intensive usage, the offline (R) version of the tool is suggested.</li>
            </ul>
            For further information and issue reporting, please contact <i>Panagis Katsivelis (panagis.katsivelis@venthic.com)</i> and <i>Nynke Hofstra (nynke.hofstra@wur.nl)</i>.
        </div> 
        : <div>No information available</div> 
    );


}
function getSteps() {
    return ['What is the Pathogen Flow & Mapping Tool?', 'Step 1. Getting started.', 'Step 2. Selecting an area.', 'Step 3. Running Scenarios.', 
    'Step 4. Summarizing results.'];
}

function getYoutubeLink(step) {
    switch (step) {
        case 2:
            return 'nzhrneh7H-o'
        case 3:
            return 'Oi7ASVFd6os'
        case 5:
            return 'D-Ky75mx2SE'
        default:
            return '';
    }
}

function getStepContent(step) {
    switch (step) {
        case 0: 
            return <div>
                <p>The purpose of the PFM Tool is to evaluate areas with high emissions of pathogens to surface waters and evaluate the potential impact of changes in population growth, access to improved sanitation facilities, and increased conveyance and treatment of wastewater and fecal sludge.</p>
                <p>A model that simulates these emissions drives the tool. The model uses input data on population, urbanization, disease incidence and pathogen shedding rates, sanitation technologies, and the treatment of wastewater and fecal sludge. The current global input data for the default run of the tool are described on the tool website and come from sources that are available online, such as the UN Department of Economic and Social Affairs, and the Joint Monitoring Program of the World Health Organization (WHO) and the United Nations’ Children’s Fund (UNICEF). Users of the tool are able to select their geographic area of interest, and develop up to three alternative scenarios that can be compared to the baseline. These scenarios can include population changes, interventions, such as eradicating open defecation, emptying and treating more waste from onsite systems, or improving the treatment of wastewater and fecal sludge. The tool produces visualizations that help a user assess the relative differences between scenarios to support decision-making. </p>
            </div>
        case 1:
            return <p>On the map you can see the areas for which data from one or more data sources are available that are required to complete model runs. You get started by selecting one country on the map. After clicking on that country, you can see the available data. In case both national and high-resolution data are available, you can either select the high-resolution data or add the country to the selection.</p>;
        case 2:
            return <p>When you added your selected country to the selection, you can in Step 2 add more countries by clicking on the map. When you selected the high-resolution data, you now have the option to select those high-resolution areas or to still go for the national data for the selected country. Afterwards, you are required to select a pathogen group. So far, the choices are viruses and protozoa. A virus is a non-cellular microorganism that must infect a host cell to reproduce. Viruses are comparatively small. Protozoa are plant-like and animal-like eukaryotic unicellular organisms. They are larger than viruses or bacteria and the spores with which they reproduce are persistent outside the human or animal body. The model is run using virus disease incidence and shedding rates from the literature for rotavirus and protozoa disease incidence and shedding rates for <i>Cryptosporidium</i>. Other processes, such as survival in pits and persistence in wastewater treatment plants are calculated using equations generic for viruses and protozoa. Select the required pathogen group and press next.</p>;
        case 3:
            return <div><p>After selecting the area and the pathogen group of choice, the model input data is loaded. Brief instructions pop up that can be closed by clicking on the x. In Step 3 you can view and, if necessary, change the model input data for the situation that you would like to define as the baseline. The baseline data is the data that you would compare your scenarios to. The data loaded are default data available on our online data portal. The data sources and assumptions can also be found on the data portal. The default data are summarized in three tabs called “Population”, “Onsite sanitation” and “Sewage and fecal sludge management”. You can have a look at the data in the tabs and decide whether they match the data that you would expect or have for the selected areas. In case you do not have original data for your area, you can use the default data as baseline data.</p>
                        <p>In the population tab, you can change the population for all selected areas or for the subareas individually. You can set the percentage by which you would like to make the change. The population drives the emissions. You can also change the urban fraction of the population. This is relevant, as the way pathogens flow from the onsite sanitation systems to the surface water is different in urban and rural areas. In rural areas there is, for instance, much more space to cover and bury pit latrines and dig new ones (see the model explanation for more details). Finally, the fraction of children under 5 in the population can be changed. For rotavirus, the disease incidence is higher in young children compared to adults. </p>
                        <p>In the onsite sanitation tab, thirteen onsite sanitation categories also used by the Joint Monitoring Project of WHO and UNICEF are summarize in five categories: “No Facility” (open defecation, hanging toilets and other), “Dry Toilets” (container based, bucket latrine, pit without a slab, pit with a slab) , “Composting Toilets”, “Flush Toilets (onsite)” (flush to open, flush to unknown, flush to pit and flush to septic tank) and “Flush Toilets (sewered)”. The detailed data can be seen when you click “View detailed data” at the bottom left of the screen. Each of the thirteen categories is treated differently in the underlying model (see model explanation). The default onsite sanitation data can be changed by selecting the category from which you would like to move the percentage and the category to which you would like to move the percentage. You can choose to move the percentage incrementally or all at once. To incrementally change the percentages you can click the button several times. For example, the current fraction for “No Facility” is currently 10%, but according to your own data this is only 7%. Instead, the percentage for “Flush Toilets (sewered)” is too small. You can now select “No Facility” in the “from” box and add “Flush Toilets (sewered)” to the “to” box and click three times on “move incrementally”. Afterwards the data in the tool match your own data. You can decide to do the change for each subarea individually, for the general population and for urban and rural areas separately. Finally, you can add “Additional improvement options”. These options change the management of the sanitation facilities. For example, septic tanks that are not watertight can be made watertight. This will lead to more pathogen emissions, unless these septic tanks are provided with leach fields. Additional improvement options can be realized by ticking the box in front of the options.</p> 
                        <p>In the sewage and fecal sludge management tab, you can evaluate and change the percentage of the fecal sludge that is safely managed, the percentage of sewage that is treated and the removal in wastewater treatment plants for all areas selected at once, or for individual areas. Safe management of fecal sludge is in this tool defined as fecal sludge from the onsite facilities (dry toilets, composting toilets and flush toilets (onsite)) covered and buried in rural areas and onsite facilities emptied and the fecal sludge brought to treatment in urban areas. The percentages can be changed by clicking on the bubble with the percentage and moving the slider bar. The reduction in pathogens from the sewage and fecal sludge that are brought to the treatment plant is based on results from the Treatment Plant Sketcher Tool. Only the pathogens in the liquid effluent reach the surface water. When in Step 2 high resolution input data are selected, the model uses input data based on the actual wastewater treatment plants in the area. When lower resolution data are used, the reduction is an estimate of the average treatment systems in the subarea. It is important to achieve high reduction of pathogen in treatment plants, because sewage usually contains high concentrations of pathogens. Achieving 90% (or 1 log unit) reduction would normally be insufficient. However, traditionally wastewater treatment plants are not built to remove pathogens and therefore potentially have low reduction. The reduction in pathogens after treatment can be changed by clicking on the correct reduction. </p>
                        <p>Now that the input data are checked and potentially changed, the baseline input data can be run. To do so, you can click ‘run’ at the top right of the screen. You are first prompted to name the baseline, to provide a description and to set this first input data as the baseline. After you have done so, you can click run again. Give the model some time to run. Then you will see a map with output data. See Step 4 for an explanation of the data.</p>
                        <p>After you ran the baseline, you can develop up to three of your own scenarios. You can do so by clicking on “+ New Scenario”. A scenario tab is added to the baseline tab. You can edit the name for this scenario. Then you can change the input data in the same way as you did for the baseline. Think carefully about scenarios that make sense. For example, adding more flush toilets connected to sewers is only realistic in the further future when the population will also have changed. So for such a scenario, you will need to change the population, potentially the urbanization percentage and also the percentage for the flush toilets (sewered). Finally, you need to think about adding more treatment and the removal after treatment to make it into a comprehensive scenario. You can run each scenario and every time a new map is produced. </p>
            </div>;
        case 4:
            return <div><p>You have now run the model between one and four times. One of the outputs is the maps. These maps show the log-transformed emission of the selected pathogen to the surface water. The results are gridded, which means that the results are provided for gridboxes. The size of these gridboxes depends on the resolution of the input data. For national data, the size is 0.5 x 0.5 degree latitude x longitude, which is roughly 50 x 50 km at the equator. For the high-resolution input data, the output resolution is 0.008333 x 0.008333 degree latitude x longitude, which is roughly 1 x 1 km. The results that you see are the emissions for a year. That year depends on the year for which the input data were provided. For the default data this year is a recent year. The actual emissions in the maps are not so relevant. It is much more interesting to look at differences in space and between scenarios. When comparing the gridded emissions in space, you will be able to find areas with high emissions (hotspot areas) and areas with lower emissions. The hotspot areas likely require more attention. You can compare the maps between scenarios by switching from one scenario tab to the next. </p>
                        <p>Maps are not the only outcome of the model. Additionally, you can click on the map to see the contribution of the different onsite sanitation facilities to the total emissions for each subarea. This way the main sources of the emissions become visible.</p>
                        <p>To analyse all of the input and output data of the model, you can click “results summary” at the bottom left of the page. Here the input data are summarized, the map is provided and the total emissions with their source contributions. You can again click from one scenario tab to the next to evaluate the differences. Here you can also download all of the model outputs at the top left of the screen.</p>
                    </div>;
        default:
         return 'Unknown step';
    }
}

const useStyles = makeStyles((theme) => ({
    root: {
      width: '100%',
    },
    backButton: {
      marginRight: theme.spacing(1),
    },
    instructions: {
      marginTop: theme.spacing(1),
      marginBottom: theme.spacing(1),
    },
}));

export default Tutorial;