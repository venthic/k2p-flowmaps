import { observer } from "mobx-react-lite";
import * as React from "react";

import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import ToiletFractions from './ToiletFractions';
import TreatmentFractions from './TreatmentFractions';
import PopulationFractions from './PopulationFractions';

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`scrollable-auto-tabpanel-${index}`}
            aria-labelledby={`scrollable-auto-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        width: '100%',
        backgroundColor: theme.palette.background.paper,
    },
    formControl: {
        display:'block',
        marginBottom: '10px'
    }
}));


const ScenarioDashboard = observer(({ context, scenario }) => {
    const map = context;
    const classes = useStyles();

    return (
        <div style={{padding:'20px'}}>
            <Tabs
                value={scenario.activeTab}
                onChange={(event, value) => scenario.setActiveTab(event, value)}
                indicatorColor="primary"
                textColor="primary"
                centered
                variant='fullWidth'
                scrollButtons="auto"
            >
                <Tab label="Population" />
                <Tab label={"Onsite Sanitation"} />
                <Tab label={"Sewage & Fecal Sludge Management"} />
            </Tabs>
            <TabPanel value={scenario.activeTab} index={0}>
                <PopulationFractions scenario={scenario}/>
            </TabPanel>
        <TabPanel value={scenario.activeTab} index={1}>
            <ToiletFractions scenario={scenario}/>
    </TabPanel>
            <TabPanel value={scenario.activeTab} index={2}>
                <div style={{ width: "100%" }}>
                    <TreatmentFractions map={map} scenario={scenario}/>
             </div>
            </TabPanel>
       </div>
    )
});

export default ScenarioDashboard;
