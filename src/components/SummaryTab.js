
import React from "react";
import { observer } from "mobx-react-lite";
import { toJS } from "mobx";
import placeholder from "../static/img/placeholder-square.jpg";
import SummaryChart from '../components/charts/SummaryChart';
import Button from '@material-ui/core/Button';
import SaveRoundedIcon from '@material-ui/icons/SaveRounded';
import Card from '@material-ui/core/Card';
import Tooltip from "@material-ui/core/Tooltip";
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined';
import { withStyles } from '@material-ui/core/styles';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import PictureAsPdfTwoToneIcon from '@material-ui/icons/PictureAsPdfTwoTone';


const LightTooltip = withStyles((theme) => ({
    tooltip: {
      backgroundColor: theme.palette.common.white,
      color: 'rgba(0, 0, 0, 0.87)',
      boxShadow: '0 1px 15px rgba(0,0,0,.04),0 1px 6px rgba(0,0,0,.04)',
      fontSize: 14,
      fontWeight: 500
    },
  }))(Tooltip);

const SummaryTab = observer(({ context }) => {
    const map = context; 
    var scenarios = toJS(map.scenarios);

    scenarios.forEach(function(item,i){
        if ( item.id === map.baselineScenario ) {
            scenarios.splice(i, 1);
            scenarios.unshift(item);
        }
    });
    
    return <div className="summaryTab">
    {(!map.animationRendering) &&                
    <Grid container style={{marginTop:'20px'}}>
        <Grid item xs={12} sm={12} md={12}>
            <h2>Output summary</h2>
            
            <ButtonGroup className='infoPrompt rightPadded'>
            <LightTooltip interactive={true} placement='left' title='In the list below, you can find the different scenarios that you have defined. After running the model, you are able to view the generated map in PNG format and a bar plot showing the total emissions per region per toilet category per capita.'>
            <IconButton style={{color:'rgba(0, 0, 0, 0.8)'}} variant='primary' aria-label="info">
            <InfoOutlinedIcon/>
            </IconButton>
            

        </LightTooltip>
        <Button disabled={map.scenarios.filter((scenario) => scenario.payload.png !== '').length === 0} startIcon={<PictureAsPdfTwoToneIcon/>} variant="secondary" color="secondary" aria-label="Generate Report" onClick={()=> map.setGenerateReportModalState(true)}>          
                Generate Report
            </Button>
            </ButtonGroup>
        </Grid>
        <Grid item xs={12} sm={12} md={12}>
        {scenarios.map((scenario) =>
        <Card className="outputCard">
            <CardContent>
            <Grid container>
                
                <Grid item xs={12} md={12} sm={12} style={{position:'relative'}}>
                    <h3>{map.scenarios[scenario.id].title}</h3>
                    
                </Grid>
                <Grid container direction="row" justify="flex-start" alignItems="center">
                <Grid md={3} sm={3} xs={12} style={{paddingTop:'0px'}}>
                    <Card style={{ position:'relative' }}>
                        <div className='pngOptions'>
                            <Tooltip title="Save image">
                                <IconButton disabled={map.scenarios[scenario.id].payload.png === ''} disableRipple aria-label="Save image" onClick={()=> {map.downloadFromUrl(map.scenarios[scenario.id].payload.png, 'map.png')}} variant='outlined'>
                                    <SaveRoundedIcon fontSize="inherit" />
                                </IconButton>
                            </Tooltip>
                        </div>
                        { (map.scenarios[scenario.id].payload.png !== '') ? 
                        <CardMedia
                            component="img"
                            alt="Pathogen Emissions"
                            image={map.scenarios[scenario.id].payload.png}
                            title="Pathogen Emissions"
                        />:
                        <CardMedia
                            component="img"
                            alt="No data yet."
                            image={placeholder}
                            title="No data yet."
                        />
                        
                        }
                        
                    </Card>
                </Grid>
                <Grid item md={9} sm={9} xs={12} style={{paddingTop:0}}>
                    <SummaryChart context={map} scenario={scenario.id}/>
                </Grid>
            </Grid>
            </Grid>
            </CardContent>
            </Card>
        )}
        </Grid>
    </Grid>
    }
</div>
                   
                
});

export default SummaryTab;