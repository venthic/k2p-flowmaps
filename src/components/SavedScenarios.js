import { observer } from "mobx-react-lite";
import * as React from "react";

import ScenarioDashboard from './ScenarioDashboard';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Tooltip from '@material-ui/core/Tooltip';
import PlayCircleFilledTwoToneIcon from '@material-ui/icons/PlayCircleFilledTwoTone';
import AddBoxTwoToneIcon from '@material-ui/icons/AddBoxTwoTone';
import EditTwoToneIcon from '@material-ui/icons/EditTwoTone';
import ReplayRoundedIcon from '@material-ui/icons/ReplayRounded';
import PrintRoundedIcon from '@material-ui/icons/PrintRounded';
import HelpRoundedIcon from '@material-ui/icons/HelpRounded';
import { ThemeProvider } from "@material-ui/core/styles";
import theme from "../config/Theme.js";
import "../styles/map.css";
import StorageRoundedIcon from '@material-ui/icons/StorageRounded';

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`scrollable-auto-tabpanel-${index}`}
            aria-labelledby={`scrollable-auto-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={2}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function truncate(input) {
    if (input.length > 12)
       return input.substring(0,12) + '...';
    else
       return input;
 };



const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        width: '100%',
        backgroundColor: theme.palette.background.paper,
    },
    savedScenarios: {
        height: '100px',
        backgroundColor: '#003347',
        alignItems: 'flex-end',
        '& .MuiButtonBase': {
            paddingBottom: 15,
            color: '#fff' 
        }
    }
}));


const SavedScenarios = observer(({ context }) => {
    const map = context;
    const classes = useStyles();

    return (
        <ThemeProvider theme={theme()}>
        <div className="savedScenarios">
            <Tabs
                value={map.activeScenario}
                onChange={(event, value) => map.setActiveScenario(event, value)}
                indicatorColor="#fff"
                textColor="primary"
                className={classes.savedScenarios}
                variant="scrollable"
                scrollButtons="auto"
                TabScrollButtonProps={{ className: 'tabScrollButton' }}
            >
                {map.scenarios.map((scenario, key) => {
                    return <Tab key={key} className='savedScenario' 
                                // disabled={map.summaryMode && scenario.payload.geoTiff===''}
                                label={(key == map.activeScenario) ? 
                                    <div>{truncate(scenario.title)}<IconButton onClick={()=> map.setMetadataModalState(true)} size="small" color="primary" aria-label="edit metadata" component="span"> 
                    <EditTwoToneIcon />
                    </IconButton></div>:
                                    truncate(scenario.title)
                                } 
                                />
                })}
                {/* <Tab key={-1} className='savedScenario' onClick={() => {map.addScenario(map.scenarios[map.scenarios.length-1])}}
                label={<AddBoxTwoToneIcon />}></Tab> */}
                
            </Tabs>
            {map.scenarios.map((scenario, key) => {
            return <TabPanel key={key} value={map.activeScenario} index={scenario.id} style={{position:'relative'}}>
                <div>
                <ButtonGroup style={{height: '30px'}} variant="text" size="small">
                    <Tooltip title="Reset scenario">
                        <IconButton disableRipple aria-label="Reset Scenario"  onClick={()=> {scenario.payload.revert();}} variant='outlined'>
                            <ReplayRoundedIcon fontSize="inherit" />
                        </IconButton>
                    </Tooltip>
                    <Tooltip title="Print map">
                        <IconButton disableRipple aria-label="Print map" onClick={()=> {map.printControl.printMap('A4Landscape', scenario.title);}} variant='outlined'>
                            <PrintRoundedIcon fontSize="inherit" />
                        </IconButton>
                    </Tooltip>
                    <Tooltip title="Data explanation">
                        <IconButton disableRipple aria-label="Print map" onClick={() => map.setDisclaimerState(true)} target='_blank' variant='outlined'>
                            <StorageRoundedIcon fontSize="inherit" />
                        </IconButton>
                    </Tooltip>
                    <Tooltip title="Instructions">
                        <IconButton disableRipple aria-label="View Instructions" onClick={()=> {map.toggleInfoMapDialogState(!map.infoMapDialogState)}} variant='outlined'>
                            <HelpRoundedIcon fontSize="inherit" />
                        </IconButton>
                    </Tooltip>
                    
                </ButtonGroup>
                <ButtonGroup style={{right:20, top: 20, position: 'absolute'}}>
                    <Button variant="contained" disabled={map.scenarios.length >= 4} onClick={() => {map.addScenario(scenario)}} startIcon={<AddBoxTwoToneIcon />} color="secondary">New scenario</Button>
                    <Button variant="contained" disabled={map.loadingTiff} startIcon={<PlayCircleFilledTwoToneIcon/>} onClick={() => {(map.baselineScenario === -1 ) ? map.setMetadataModalState(true) : map.run()}} color="primary">Run</Button>
                </ButtonGroup>
                <br/>
                <ScenarioDashboard style={{marginTop:'35px'}} context={map} scenario={scenario.payload} />
                </div> 
                
            </TabPanel>
            })
        }
            
        </div>
        </ThemeProvider>
    )
});

export default SavedScenarios;
