
import React, {useEffect} from "react";
import { observer } from "mobx-react-lite";
import Chart from 'react-apexcharts';
import ApexCharts from "apexcharts";


const SummaryChart = observer(({ context, scenario }) => {

    
    const map = context; 
    const current = scenario;
    const maxValues = map.scenarios.map( (scenario) => { return scenario.payload.getMaximumPerCapita  }).sort((function(a, b) {
      return b - a;
    }));
    const maxValue = maxValues[0];

    useEffect(() => {
      setTimeout(
        () => {
          ApexCharts.exec("outputLadder-"+map.scenarios[current].id, "dataURI").then(({ imgURI }) => {
            map.scenarios[current].payload.setOutputLadder(imgURI);
          });
        }, 1000);
    });

    const chartData = { series: [{
                name: 'No Facility',
                data: map.scenarios[current].payload.outputRecords.map((obj) => { 
                  var pop = map.scenarios[current].payload.records.filter(record => record.subarea === obj.subarea)[0]['population']; 
                  var value = (obj['total_openDefecation_out']+obj['total_other_out']+obj['total_hangingToilet_out'])/pop;
              return value})
              }, {
                name: 'Dry Toilets',
                data: map.scenarios[current].payload.outputRecords.map((obj) => {
                  var pop = map.scenarios[current].payload.records.filter(record => record.subarea === obj.subarea)[0]['population'];
                  var value = (obj['total_pitSlab_out']+obj['total_pitNoSlab_out']+obj['total_bucketLatrine_out']+obj['total_containerBased_out'])/pop
                  return value})
              }, {
                name: 'Composting Toilets',
                data: map.scenarios[current].payload.outputRecords.map((obj) => {
                  var pop = map.scenarios[current].payload.records.filter(record => record.subarea === obj.subarea)[0]['population'];
                  var value = obj['total_compostingToilet_out']/pop
                  return value})
              }, {
                name: 'Flush toilets (onsite)',
                data: map.scenarios[current].payload.outputRecords.map((obj) => {
                  var pop = map.scenarios[current].payload.records.filter(record => record.subarea === obj.subarea)[0]['population'];
                  var value = (obj['total_flushSeptic_out']+obj['total_flushPit_out']+obj['total_flushOpen_out']+obj['total_flushUnknown_out'])/pop
              return value})
              }, {
                name: 'Flush toilets (sewered)',
                data: map.scenarios[current].payload.outputRecords.map((obj) => {
                  var pop = map.scenarios[current].payload.records.filter(record => record.subarea === obj.subarea)[0]['population'];
                  var value = obj['total_flushSewer_out']/pop
              return value})
              }],
              options: {
                colors:['rgb(191, 64, 64)', 'rgb(188,123,80)', 'rgb(191, 172, 64)', 'rgb(102, 191, 64)', 'rgb(64, 140, 191)'],
                chart: {
                  type: 'bar',
                  stacked: true,
                  animations: {
                    enabled: false
                  },
                  id: 'outputLadder-'+map.scenarios[current].id,
                  colors:['rgb(191, 64, 64)', 'rgb(188,123,80)', 'rgb(191, 172, 64)', 'rgb(102, 191, 64)', 'rgb(64, 140, 191)'],
                  fontFamily: 'Cabin, sans-serif',
                  toolbar: {
                    show: true,
                    offsetX: 0,
                    offsetY: 0,
                    tools: {
                      download: true,
                      zoom: true,
                      zoomin: true,
                      zoomout: true,
                      pan: true,
                      reset: true,
                    }
                  }
                  // events: {
                  //   click: function(event, chartContext, config) {
                  //     console.log(config);
                  //   }
                  // }
                },
                plotOptions: {
                  bar: {
                    horizontal: true
                  },
                },
                stroke: {
                  width: 1,
                  colors: ['#fff']
                },
                // title: {
                //   align: 'left',
                //   text: 'Emissions per onsite technology per year per capita',
                //   style: {
                //     fontSize: '15px'
                //   }
                // },
                noData: {
                  text: 'No data available. Model has not been executed for this scenario.',
                  align: 'center',
                  verticalAlign: 'middle',
                  offsetX: 0,
                  offsetY: 0,
                  style: {
                    fontSize: '16px',
                    fontStyle: 'italic' 
                  }
                },
                xaxis: {
                  categories: map.scenarios[current].payload.outputRecords.map((obj) => {return obj['subarea']}),
                  labels: {
                    formatter: function (val) {
                      return val.toExponential(2)
                    }
                  },
                  max: maxValue
                },
                yaxis: {
                  title: {
                    text: undefined
                  },
                  labels: {
                    show: map.scenarios[current].payload.outputRecords.length > 1
                  }
                },
                tooltip: {
                  y: {
                    formatter: function (val) {
                      return val.toExponential(2)
                    }
                  }
                },
                fill: {
                  colors:['rgb(191, 64, 64)', 'rgb(188,123,80)', 'rgb(191, 172, 64)', 'rgb(102, 191, 64)', 'rgb(64, 140, 191)'],
                  opacity: 1
                },
                dataLabels: {
                    enabled: false
                },
                legend: {
                  show: (current === map.baselineScenario) ? true: false,
                  position: 'top',
                  horizontalAlign: 'center'
                }
              },
            
            
            };

    
    

    // const colorScheme = chroma
    //     .scale(['rgb(191, 64, 64)', 'rgb(188,123,80)', 'rgb(191, 172, 64)', 'rgb(102, 191, 64)', 'rgb(64, 140, 191)'])
    //     .mode('lch')
    //     .colors(5);

    return <div>
        {(!map.animationRendering) && [
            <Chart options={chartData.options} height={map.scenarios[current].payload.outputRecords.length > 25 ? map.scenarios[current].payload.outputRecords.length > 50 ? 1000 : 500 : 300} width='100%' series={chartData.series} type="bar" style={{ marginTop:'15px' }} />
            // <br/>,
            // <span><b>Minimum emissions value: </b>{map.scenarios[current].payload.minValue}</span>,
            // <br/>,
            // <span><b>Maximum emissions value: </b>{map.scenarios[current].payload.maxValue}</span>
            ]
        }
        </div>
                   
                
});

export default SummaryChart;