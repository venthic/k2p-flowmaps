
import React, {useEffect} from "react";
import { observer } from "mobx-react-lite";
import Chart from 'react-apexcharts';
import toiletCategories from "../../config/toiletColumns.json";



const ToiletChart = observer(({ scenario, context }) => {
     
    const cat1 = Math.round(scenario.populationWeightedAvg(toiletCategories[0].columns, context, true, []));
    const cat2 = Math.round(scenario.populationWeightedAvg(toiletCategories[1].columns, context, true, []));
    const cat3 = Math.round(scenario.populationWeightedAvg(toiletCategories[2].columns, context, true, []));
    const cat4 = Math.round(scenario.populationWeightedAvg(toiletCategories[3].columns, context, true, []));
    const cat5 = Math.round(scenario.populationWeightedAvg(toiletCategories[4].columns, context, true, []));

    const chartData = { series: [{
                name: 'No Facility',
                data: [cat1]
              }, {
                name: 'Dry Toilets',
                data: [cat2]
              }, {
                name: 'Composting Toilets',
                data: [cat3]
              }, {
                name: 'Flush toilets (onsite)',
                data: [cat4]
              }, {
                name: 'Flush toilets (sewered)',
                data: [cat5]
              }],
              options: {
                colors:['rgb(191, 64, 64)', 'rgb(188,123,80)', 'rgb(191, 172, 64)', 'rgb(102, 191, 64)', 'rgb(64, 140, 191)'],
                chart: {
                  type: 'bar',
                  width: '100%',
                  id: 'sanitationLadder',
                  stacked: true,
                  stackType: '100%',//(context === 'both') ? '100%' : (cat1+cat2+cat3+cat4+cat5)+'%',
                  colors:['rgb(191, 64, 64)', 'rgb(188,123,80)', 'rgb(191, 172, 64)', 'rgb(102, 191, 64)', 'rgb(64, 140, 191)'],
                  fontFamily: 'Cabin, sans-serif',
                  toolbar: {
                    show: false
                  }
                },
                plotOptions: {
                  bar: {
                    horizontal: true,
                    barHeight: '100%'
                  },
                },
                stroke: {
                  width: 1,
                  colors: ['#fff']
                },
                noData: {
                  text: 'No data available for the selected region and population context.',
                  align: 'center',
                  verticalAlign: 'middle',
                  offsetX: 0,
                  offsetY: 0,
                  style: {
                    fontSize: '16px',
                    fontStyle: 'italic' 
                  }
                },
                xaxis: {
                  categories: [(context === 'both') ? 'General Population' : (context === 'urb') ? 'Urban': 'Rural'],
                  labels: {
                    formatter: function (val) {
                      return val+'%'
                    }
                  }
                },
                yaxis: {
                  title: {
                    text: undefined
                  },
                  labels : {
                    show: false
                  }
                },
                fill: {
                  colors:['rgb(191, 64, 64)', 'rgb(188,123,80)', 'rgb(191, 172, 64)', 'rgb(102, 191, 64)', 'rgb(64, 140, 191)'],
                  opacity: 1
                },
                dataLabels: {
                    enabled: true
                },
                legend: {
                  position: 'bottom',
                  horizontalAlign: 'center'
                //   offsetX: 40
                }
              },
            
            
            };

    useEffect(() => {
      // setTimeout(
      //   () => {
      //     ApexCharts.exec("sanitationLadder", "dataURI").then(({ imgURI }) => {
      //       scenario.setSanitationLadder(imgURI);
      //     });
      //   }, 1000);
    });
    

    // const colorScheme = chroma
    //     .scale(['rgb(191, 64, 64)', 'rgb(188,123,80)', 'rgb(191, 172, 64)', 'rgb(102, 191, 64)', 'rgb(64, 140, 191)'])
    //     .mode('lch')
    //     .colors(5);

    return <div style={{width:'100%'}}>
            <Chart options={chartData.options} height={130} series={chartData.series} type="bar" />
            
        </div>
                   
                
});

export default ToiletChart;