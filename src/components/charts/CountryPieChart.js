
import React, {useEffect} from "react";
import { observer } from "mobx-react-lite";
import Chart from 'react-apexcharts';
import { createFalse } from "typescript";


const CountryPieChart = observer(({ context, scenario }) => {

    
    const map = context; 
    const current = scenario;
    const obj = map.scenarios[current].payload.outputRecords;
    const population = map.scenarios[current].payload.records[0].population;
   
    const values = [
      (parseFloat(obj[0]['total_openDefecation_out'])+parseFloat(obj[0]['total_other_out'])+parseFloat(obj[0]['total_hangingToilet_out'])),
      (parseFloat(obj[0]['total_pitSlab_out'])+parseFloat(obj[0]['total_pitNoSlab_out'])+parseFloat(obj[0]['total_bucketLatrine_out'])+parseFloat(obj[0]['total_containerBased_out'])),
      parseFloat(obj[0]['total_compostingToilet_out']),
      (parseFloat(obj[0]['total_flushSeptic_out'])+parseFloat(obj[0]['total_flushPit_out'])+parseFloat(obj[0]['total_flushOpen_out'])+parseFloat(obj[0]['total_flushUnknown_out'])),
      parseFloat(obj[0]['total_flushSewer_out'])
    ];
    console.log(values);
    const chartData = { 
            series: [ {
              data: [
                (values[0] > 0) ? Math.log10(values[0]/population) : 0,
                (values[1] > 0) ? Math.log10(values[1]/population) : 0,
                (values[2] > 0) ? Math.log10(values[2]/population) : 0,
                (values[3] > 0) ? Math.log10(values[3]/population) : 0,
                (values[4] > 0) ? Math.log10(values[4]/population) : 0
              ]}
              ],
               
              options: {
                colors:['rgb(191, 64, 64)', 'rgb(188,123,80)', 'rgb(191, 172, 64)', 'rgb(102, 191, 64)', 'rgb(64, 140, 191)'],
                
                  //  labels: ['No Facility', 'Dry toilets', 'Composting Toilets', 'Flush Toilets (onsite)', 'Flush Toilets (sewered)'], 
                chart: {
                  type: 'bar',
                  
                  animations: {
                    enabled: true
                  },
                  id: 'outputLadder-'+map.scenarios[current].id,
                  colors: ['rgb(191, 64, 64)', 'rgb(188,123,80)', 'rgb(191, 172, 64)', 'rgb(102, 191, 64)', 'rgb(64, 140, 191)'],
                  fontFamily: 'Cabin, sans-serif',
                  toolbar: {
                    show: true,
                    offsetX: 0,
                    offsetY: 0,
                    tools: {
                      download: true,
                      zoom: true,
                      zoomin: true,
                      zoomout: true,
                      pan: true,
                      reset: true,
                    }
                  }
                  // events: {
                  //   click: function(event, chartContext, config) {
                  //     console.log(config);
                  //   }
                  // }
                },
                stroke: {
                  width: 1,
                  colors: ['#fff']
                },
                title: {
                  align: 'left',
                  text: 'Annual emissions per onsite technology per capita',
                  style: {
                    paddingBottom: '20px',
                    fontSize: '15px'
                  }
                },
                
                fill: {
                  colors:['rgb(191, 64, 64)', 'rgb(188,123,80)', 'rgb(191, 172, 64)', 'rgb(102, 191, 64)', 'rgb(64, 140, 191)'],
                  opacity: 1
                },
                plotOptions: {
                  bar: {
                    columnWidth: '45%',
                    distributed: true
                  }
                },
                dataLabels: {
                    enabled: false
                },
                legend: {
                  show: false
                },
                xaxis: {
                  categories: [
                    'No Facility',
                    'Dry toilets',
                    ['Composting', 'Toilets'],
                    ['Flush', 'Toilets', '(onsite)'],
                    ['Flush', 'Toilets', '(sewered)']
                  ],
                  labels: {
                    style: {
                      colors: ['rgb(191, 64, 64)', 'rgb(188,123,80)', 'rgb(191, 172, 64)', 'rgb(102, 191, 64)', 'rgb(64, 140, 191)'],
                      fontSize: '12px'
                    }
                  }
                },
                yaxis: {
                  labels: {
                    formatter: (value) => { return value.toFixed(1) },
                  }
                }
              },
            
            
            };

    
    

    // const colorScheme = chroma
    //     .scale(['rgb(191, 64, 64)', 'rgb(188,123,80)', 'rgb(191, 172, 64)', 'rgb(102, 191, 64)', 'rgb(64, 140, 191)'])
    //     .mode('lch')
    //     .colors(5);

    return <div>
        {(!map.animationRendering) && [
            <Chart options={chartData.options} series={chartData.series} height={300} width='100%' type="bar" style={{ marginTop:'30px', margin:'auto' }} />
            // <br/>,
            // <span><b>Minimum emissions value: </b>{map.scenarios[current].payload.minValue}</span>,
            // <br/>,
            // <span><b>Maximum emissions value: </b>{map.scenarios[current].payload.maxValue}</span>
            ]
        }
        </div>
                   
                
});

export default CountryPieChart;