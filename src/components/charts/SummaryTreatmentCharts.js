
import React from "react";
import { observer } from "mobx-react-lite";
import Chart from 'react-apexcharts';
import { Grid } from '@material-ui/core';


const SummaryTreatmentCharts = observer(({ context }) => {
    const map = context; 
    
    const sludgeData = { series: [{
        name: 'Fecal sludge safely managed',
        data: map.scenarios.map((scenario => {
          return (scenario.payload.avg(["fecalSludgeTreated_urb", "coverBury_rur"], true)*100).toFixed(2);
        }))}
        ],
              options: {
                colors:['#003347'],
                chart: {
                  type: 'bar',
                  colors:['#003347'],
                  fontFamily: 'Cabin, sans-serif',
                  // sparkline: {
                  //   enabled: true
                  // },
                  toolbar: {
                    show: true,
                    offsetX: 0,
                    offsetY: 0,
                    tools: {
                      download: true,
                      zoom: true,
                      zoomin: true,
                      zoomout: true,
                      pan: true,
                      reset: true,
                    }
                  }
                  // events: {
                  //   click: function(event, chartContext, config) {
                  //     console.log(config);
                  //   }
                  // }
                },
                plotOptions: {
                  bar: {
                    horizontal: false,
                    barHeight: '50%'
                  },
                },
                dataLabels: {
                  enabled: true,
                  offsetX: -6,
                  style: {
                    fontSize: '12px',
                    colors: ['#42919e']
                  },
                  formatter: function (val, opts) {
                    return val+'%'
                  }
                },
                stroke: {
                  width: 1,
                  colors: ['#fff']
                },
                title: {
                  align: 'left',
                  text: 'Fecal sludge safely managed',
                  style: {
                    fontSize: '12px'
                  }
                },
                xaxis: {
                  categories: map.scenarios.map((scenario) => {return scenario.title}),
                  labels: {
                    formatter: function (val) {
                      return val
                    },
                    show: map.scenarios.length > 1
                    
                  }
                },
                yaxis: {
                  title: {
                    text: undefined
                  },
                  labels: {
                    formatter: function (val) {
                      return parseInt(val)+'%'
                    }
                  }
                },
                tooltip: {
                  y: {
                    formatter: function (val) {
                      return Math.round(val)+'%'
                    }
                  }
                },
                fill: {
                  colors:['#003347'],
                  opacity: 1
                },
                legend: {
                  show: true,
                  position: 'top',
                  horizontalAlign: 'center'
                }
              },
            
            
            };

            const sewageData = { series: [{
              name: 'Sewage treated',
              data: map.scenarios.map((scenario => {
                return (scenario.payload.avg(["sewageTreated_urb", "sewageTreated_rur"], true)*100).toFixed(2);
              }))}
              ],
                    options: {
                      colors:['#003347'],
                      chart: {
                        type: 'bar',
                        colors:['#003347'],
                        fontFamily: 'Cabin, sans-serif',
                        // sparkline: {
                        //   enabled: true
                        // },
                        toolbar: {
                          show: true,
                          offsetX: 0,
                          offsetY: 0,
                          tools: {
                            download: true,
                            zoom: true,
                            zoomin: true,
                            zoomout: true,
                            pan: true,
                            reset: true,
                          }
                        }
                        // events: {
                        //   click: function(event, chartContext, config) {
                        //     console.log(config);
                        //   }
                        // }
                      },
                      plotOptions: {
                        bar: {
                          horizontal: false,
                          barHeight: '50%' 
                        },
                      },
                      dataLabels: {
                        enabled: true,
                        offsetX: -6,
                        style: {
                          fontSize: '12px',
                          colors: ['#42919e']
                        },
                        formatter: function (val, opts) {
                          return val+'%'
                        }
                      },
                      stroke: {
                        width: 1,
                        colors: ['#fff']
                      },
                      title: {
                        align: 'left',
                        text: 'Sewage treated',
                        style: {
                          fontSize: '12px'
                        }
                      },
                      xaxis: {
                        categories: map.scenarios.map((scenario) => {return scenario.title}),
                        labels: {
                          formatter: function (val) {
                            return val
                          },
                          show: map.scenarios.length > 1
                        }
                      },
                      yaxis: {
                        title: {
                          text: undefined
                        },
                        labels: {
                          formatter: function (val) {
                            return parseInt(val)+'%'
                          }
                        }
                      },
                      tooltip: {
                        y: {
                          formatter: function (val) {
                            return Math.round(val)+'%'
                          }
                        }
                      },
                      fill: {
                        colors:['#003347'],
                        opacity: 1
                      },
                      legend: {
                        show: true,
                        position: 'top',
                        horizontalAlign: 'center'
                      }
                    },
                  
                  
                  };
    
                  const reductionData = { series: [{
                    name: 'Pathogen reduction',
                    data: map.scenarios.map((scenario => {
                      return (-Math.log10(1- scenario.payload.avg(['fRemoval_treatment'], true))).toFixed(2);
                    }))}
                    ],
                          options: {
                            colors:['#003347'],
                            chart: {
                              type: 'bar',
                              colors:['#003347'],
                              fontFamily: 'Cabin, sans-serif',
                              // sparkline: {
                              //   enabled: true
                              // },
                              toolbar: {
                                show: true,
                                offsetX: 0,
                                offsetY: 0,
                                tools: {
                                  download: true,
                                  zoom: true,
                                  zoomin: true,
                                  zoomout: true,
                                  pan: true,
                                  reset: true,
                                }
                              }
                              // events: {
                              //   click: function(event, chartContext, config) {
                              //     console.log(config);
                              //   }
                              // }
                            },
                            plotOptions: {
                              bar: {
                                horizontal: false,
                                barHeight: '50%'
                              },
                            },
                            dataLabels: {
                              enabled: true,
                              offsetX: -6,
                              style: {
                                fontSize: '12px',
                                colors: ['#42919e']
                              }
                            },
                            stroke: {
                              width: 1,
                              colors: ['#fff']
                            },
                            title: {
                              align: 'left',
                              text: 'Pathogen reduction',
                              style: {
                                fontSize: '12px'
                              }
                            },
                            xaxis: {
                              categories: map.scenarios.map((scenario) => {return scenario.title}),
                              labels: {
                                show: map.scenarios.length > 1
                              }
                            },
                            yaxis: {
                              title: {
                                text: undefined
                              },
                            },
                            tooltip: {
                              y: {
                                formatter: function (val) {
                                  return val.toFixed(2)+' log'
                                }
                              }
                            },
                            fill: {
                              colors:['#003347'],
                              opacity: 1
                            },
                            legend: {
                              show: true,
                              position: 'top',
                              horizontalAlign: 'center'
                            }
                          },
                        
                        
                        };

    return <div>
        {(!map.animationRendering) && [
            <Grid container spacing={2}>
              <Grid item sm={4} xs={12}>
                <Chart options={sludgeData.options} height={200} series={sludgeData.series} type="bar" style={{ marginTop:'15px' }} />
              </Grid>
              <Grid item sm={4} xs={12}>
                <Chart options={sewageData.options} height={200} series={sewageData.series} type="bar" style={{ marginTop:'15px' }} />
              </Grid>
              <Grid item sm={4} xs={12}>
                <Chart options={reductionData.options} height={200} series={reductionData.series} type="bar" style={{ marginTop:'15px' }} />
              </Grid>
            </Grid>
            // <br/>,
            // <span><b>Minimum emissions value: </b>{map.scenarios[current].payload.minValue}</span>,
            // <br/>,
            // <span><b>Maximum emissions value: </b>{map.scenarios[current].payload.maxValue}</span>
            ]
        }
        </div>
                   
                
});

export default SummaryTreatmentCharts;