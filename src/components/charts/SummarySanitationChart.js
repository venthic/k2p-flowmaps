
import React, {useEffect} from "react";
import { observer } from "mobx-react-lite";
import Chart from 'react-apexcharts';
import toiletCategories from "../../config/toiletColumns.json";
import ApexCharts from "apexcharts";


const SummarySanitationChart = observer(({ context }) => {
    const map = context; 

    useEffect(() => {
      setTimeout(
        () => {
          ApexCharts.exec("sanitationLadder", "dataURI").then(({ imgURI }) => {
            map.setSanitationLadder(imgURI);
          });
        }, 1000);
    });
    
    const chartData = { series: [{
        name: 'No Facility',
        data: map.scenarios.map((scenario => {
          return scenario.payload.populationWeightedAvg(toiletCategories[0].columns, 'both', false, []);
        }))}, {
          name: 'Dry Toilets',
          data: map.scenarios.map((scenario => {
            return scenario.payload.populationWeightedAvg(toiletCategories[1].columns, 'both', false, []);
          }))}
        , {
          name: 'Composting Toilets',
          data: map.scenarios.map((scenario => {
            return scenario.payload.populationWeightedAvg(toiletCategories[2].columns, 'both', false, []);
          }))}
        , {
          name: 'Flush toilets (onsite)',
          data: map.scenarios.map((scenario => {
            return scenario.payload.populationWeightedAvg(toiletCategories[3].columns, 'both', false, []);
          }))}
        , {
          name: 'Flush toilets (sewered)',
          data: map.scenarios.map((scenario => {
            return scenario.payload.populationWeightedAvg(toiletCategories[4].columns, 'both', false, []);
          }))
          }
        ],
              options: {
                colors:['rgb(191, 64, 64)', 'rgb(188,123,80)', 'rgb(191, 172, 64)', 'rgb(102, 191, 64)', 'rgb(64, 140, 191)'],
                chart: {
                  type: 'bar',
                  animations: {
                    enabled: false
                  },
                  stacked: true,
                  id: 'sanitationLadder',
                  colors:['rgb(191, 64, 64)', 'rgb(188,123,80)', 'rgb(191, 172, 64)', 'rgb(102, 191, 64)', 'rgb(64, 140, 191)'],
                  stacked: true,
                  stackType: '100%',
                  fontFamily: 'Cabin, sans-serif',
                  toolbar: {
                    show: true,
                    offsetX: 0,
                    offsetY: 0,
                    tools: {
                      download: true,
                      zoom: true,
                      zoomin: true,
                      zoomout: true,
                      pan: true,
                      reset: true,
                    }
                  }
                  // events: {
                  //   click: function(event, chartContext, config) {
                  //     console.log(config);
                  //   }
                  // }
                },
                plotOptions: {
                  bar: {
                    horizontal: true
                  },
                },
                stroke: {
                  width: 1,
                  colors: ['#fff']
                },
                // title: {
                //   align: 'left',
                //   text: 'Emissions per onsite technology per year per capita',
                //   style: {
                //     fontSize: '15px'
                //   }
                // },
                noData: {
                  text: 'No data available. Model has not been executed for this scenario.',
                  align: 'center',
                  verticalAlign: 'middle',
                  offsetX: 0,
                  offsetY: 0,
                  style: {
                    fontSize: '16px',
                    fontStyle: 'italic' 
                  }
                },
                xaxis: {
                  categories: map.scenarios.map((scenario) => {return scenario.title}),
                  labels: {
                    formatter: function (val) {
                      return val+'%'
                    }
                  }
                },
                yaxis: {
                  title: {
                    text: undefined
                  },
                  labels: {
                    show: map.scenarios.length > 1
                  }
                },
                tooltip: {
                  y: {
                    formatter: function (val) {
                      return val.toFixed(2)+'%'
                    }
                  }
                },
                fill: {
                  colors:['rgb(191, 64, 64)', 'rgb(188,123,80)', 'rgb(191, 172, 64)', 'rgb(102, 191, 64)', 'rgb(64, 140, 191)'],
                  opacity: 1
                },
                dataLabels: {
                  enabled: true,
                  offsetX: -6,
                  hideOverflowingLabels: true,
                  style: {
                    fontSize: '12px',
                    colors: ['#fff']
                  }
                },
                legend: {
                  show: true,
                  position: 'top',
                  horizontalAlign: 'center'
                }
              },
            
            
            };

    
    

    // const colorScheme = chroma
    //     .scale(['rgb(191, 64, 64)', 'rgb(188,123,80)', 'rgb(191, 172, 64)', 'rgb(102, 191, 64)', 'rgb(64, 140, 191)'])
    //     .mode('lch')
    //     .colors(5);

    return <div>
        {(!map.animationRendering) && [
            <Chart options={chartData.options} height={200} series={chartData.series} type="bar" style={{ marginTop:'15px' }} />
            // <br/>,
            // <span><b>Minimum emissions value: </b>{map.scenarios[current].payload.minValue}</span>,
            // <br/>,
            // <span><b>Maximum emissions value: </b>{map.scenarios[current].payload.maxValue}</span>
            ]
        }
        </div>
                   
                
});

export default SummarySanitationChart;