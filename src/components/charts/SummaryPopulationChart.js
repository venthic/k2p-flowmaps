
import React, {useEffect} from "react";
import { observer } from "mobx-react-lite";
import Chart from 'react-apexcharts';
import NumberFormat from 'react-number-format';


const SummaryPopulationChart = observer(({ context }) => {
    const map = context; 
    
    const chartData = { series: [{
                name: 'Urban',
                data: map.scenarios.map((scenario) => { 
                  var pop = Math.round(scenario.payload.sum('population')*scenario.payload.avg(['fraction_urban_pop'], true)); 
                  return pop; 
              })}, {
                name: 'Rural',
                data: map.scenarios.map((scenario) => { 
                  var pop = Math.round(scenario.payload.sum('population')*(1-scenario.payload.avg(['fraction_urban_pop'], true))); 
                  return pop;
              })}],
              options: {
                // colors:['rgb(191, 64, 64)', 'rgb(188,123,80)', 'rgb(191, 172, 64)', 'rgb(102, 191, 64)', 'rgb(64, 140, 191)'],
                chart: {
                  type: 'bar',
                  stacked: true,
                  // colors:['rgb(191, 64, 64)', 'rgb(188,123,80)', 'rgb(191, 172, 64)', 'rgb(102, 191, 64)', 'rgb(64, 140, 191)'],
                  fontFamily: 'Cabin, sans-serif',
                  toolbar: {
                    show: true,
                    offsetX: 0,
                    offsetY: 0,
                    tools: {
                      download: true,
                      zoom: true,
                      zoomin: true,
                      zoomout: true,
                      pan: true,
                      reset: true,
                    }
                  }
                  // events: {
                  //   click: function(event, chartContext, config) {
                  //     console.log(config);
                  //   }
                  // }
                },
                plotOptions: {
                  bar: {
                    horizontal: true
                  },
                },
                dataLabels: {
                  enabled: true,
                  offsetX: -6,
                  style: {
                    fontSize: '12px',
                    colors: ['#fff']
                  },
                  formatter: function (val, opts) {
                    
                    //var sum = opts.seriesIndex[0][0] + index.series[1][0]; 
                    //var sum = 1;
                    if (opts.seriesIndex === 0) {
                    return (map.scenarios[opts.dataPointIndex].payload.avg(['fraction_urban_pop'], true)*100).toFixed(2)+'%'
                    }
                    else {
                      return ((1 - map.scenarios[opts.dataPointIndex].payload.avg(['fraction_urban_pop'], true))*100).toFixed(2)+'%'
                    }
                  }
                },
                stroke: {
                  width: 1,
                  colors: ['#fff']
                },
                // title: {
                //   align: 'left',
                //   text: 'Emissions per onsite technology per year per capita',
                //   style: {
                //     fontSize: '15px'
                //   }
                // },
                noData: {
                  text: 'No data available. Model has not been executed for this scenario.',
                  align: 'center',
                  verticalAlign: 'middle',
                  offsetX: 0,
                  offsetY: 0,
                  style: {
                    fontSize: '16px',
                    fontStyle: 'italic' 
                  }
                },
                xaxis: {
                  categories: map.scenarios.map((scenario) => {return scenario.title}),
                  labels: {
                    formatter: function (val) {
                      if (val /1000000 > 1000) {
                        return (val / 1000000000).toFixed(1)+' B'
                      }
                      return (val / 1000000).toFixed(1)+' M'
                    }
                  }
                },
                yaxis: {
                  title: {
                    text: undefined
                  },
                  labels: {
                    show: map.scenarios.length > 1
                  }
                },
                tooltip: {
                  y: {
                    formatter: function (val, index) { 
                      return val
                    }
                  }
                },
                
                fill: {
                  // colors:['rgb(191, 64, 64)', 'rgb(188,123,80)', 'rgb(191, 172, 64)', 'rgb(102, 191, 64)', 'rgb(64, 140, 191)'],
                  opacity: 1
                },
                legend: {
                  show: true,
                  position: 'top',
                  horizontalAlign: 'center'
                }
              },
            
            
            };

    
    

    // const colorScheme = chroma
    //     .scale(['rgb(191, 64, 64)', 'rgb(188,123,80)', 'rgb(191, 172, 64)', 'rgb(102, 191, 64)', 'rgb(64, 140, 191)'])
    //     .mode('lch')
    //     .colors(5);

    return <div>
        {(!map.animationRendering) && [
            <Chart options={chartData.options} height={200} series={chartData.series} type="bar" style={{ marginTop:'15px' }} />
            // <br/>,
            // <span><b>Minimum emissions value: </b>{map.scenarios[current].payload.minValue}</span>,
            // <br/>,
            // <span><b>Maximum emissions value: </b>{map.scenarios[current].payload.maxValue}</span>
            ]
        }
        </div>
                   
                
});

export default SummaryPopulationChart;