
import * as React from "react";
import { observer } from "mobx-react-lite";
import {Grid, Box, Typography, Slider, Card, Tooltip } from '@material-ui/core';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import ReactSlider from 'react-slider';
import styled from 'styled-components';
import AreasDialog from "./dialogs/AreasDialog";
import IconButton from '@material-ui/core/IconButton';
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined';


const useStyles = makeStyles((theme) => ({
  root: {
      flexGrow: 1,
      width: '100%',
      backgroundColor: theme.palette.background.paper,
  },
  formControl: {
      display:'block',
      marginBottom: '10px'
  },
  enhancedBox: {
    padding: 20,
    marginTop: 10,
    marginBottom: 10,
    borderRadius: 2,
    backgroundColor: '#eee',
    boxShadow: '0 1px 15px rgba(0,0,0,.04), 0 1px 6px rgba(0,0,0,.04)',
    border: '1px solid #e7e7e7',
    width: '100%'
  }
}));

const StyledSlider = styled(ReactSlider)`
width: 100%;
height: 15px;
`;

const StyledThumb = styled.div`
height: 25px;
top: -4px;
line-height: 25px;
width: 25px;
text-align: center;
color: #fff;
border-radius: 50%;
cursor: grab;
`;

const Thumb = (props, state) => { 
  return <StyledThumb {...props}>{state.valueNow}
</StyledThumb>};

const StyledTrack = styled.div`
  top: 0;
  bottom: 0;
  background: ${props => props.index === 1 ?  'rgba(13, 50, 79, 0.3)': '#003347'};
  border-radius: 999px;
`;


const LightTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: theme.palette.common.white,
    color: 'rgba(0, 0, 0, 0.87)',
    boxShadow: '0 1px 15px rgba(0,0,0,.04),0 1px 6px rgba(0,0,0,.04)',
    fontSize: 14,
    fontWeight: 500
  },
}))(Tooltip);

function ValueLabelComponent(props) {
  const { children, open, value } = props;

  return (
    <Tooltip open={open} enterTouchDelay={0} placement="bottom" title={value}>
      {children}
    </Tooltip>
  );
}

const marks = [
  {
    value: 0,
    label: '0',
  },
  {
    value: 1,
    label: '1',
  },
  {
    value: 2,
    label: '2',
  },
  {
    value: 3,
    label: '3',
  },
  {
    value: 4,
    label: '4',
  },
  {
    value: 5,
    label: '5',
  },
  {
    value: 6,
    label: '6',
  },
];

function valueFormat(value) {
  if (value <= 99) {
    return parseInt(value);
  } else if ( value <= 99.9) {
    return value.toFixed(1);
  } else if ( value <= 99.99) {
    return value.toFixed(2);
  } else if ( value <= 99.999) {
    return value.toFixed(3);
  } else {
    return value.toFixed(4);
  }
}

function floatValueFormat(value) {
  var floatValue = value;
  if (value >= 0.999999) {
    floatValue = Number(value.toPrecision(7));
  } else if ( value >= 0.99999) {
    floatValue = Number(value.toPrecision(6));
  } else if ( value >= 0.9999) {
    floatValue = Number(value.toPrecision(5));
  } else if ( value >= 0.999) {
    floatValue = Number(value.toPrecision(4));
  } else if (value >= 0.99) {
    floatValue = Number(value.toPrecision(3));
  } else {
    floatValue = Number(value.toPrecision(2));
  }
  return floatValue;
}

const Track = (props, state) => <StyledTrack {...props} index={state.index} />;

const TreatmentFractions = observer(({ map, scenario }) => {

  const classes = useStyles();

  return <div className='treatmentFractions' style={{ width: "100%" }}>
    
      
      <Grid container spacing={2}>
      <Grid item md={12} sm={12}>
        <Box className={classes.enhancedBox} style={{width: 'auto'}}>
        Viewing aggregates for: {(
          scenario.selectedAreasDialogValues[scenario.activeTab].length === 0 ||
          scenario.selectedAreasDialogValues[scenario.activeTab].length === scenario.records.length) ?
          <b>all areas</b> :
          <b>{scenario.selectedAreasDialogValues[scenario.activeTab].length} areas</b>} (<a href="#" onClick={() => { scenario.setSelectedAreasDialogTab(scenario.activeTab) }}>change</a>)
      </Box>
      </Grid>
      <Grid item md={6} lg={6} xs={12}>
        <Card style={{padding:'10px 40px 40px 40px', position:'relative'}}>
        <LightTooltip interactive={true} placement='left' title='The ‘Fecal sludge safely managed’ slider bar represents the percentage of fecal sludge in rural areas that is covered and buried and the percentage of fecal sludge in urban areas that is taken from the onsite facilities and brought to treatment. You can change this percentage by moving the slider bar to the left or the right.'>
          <IconButton aria-label="info" className='infoPrompt'>
            <InfoOutlinedIcon/>
          </IconButton>
        </LightTooltip>
        <h4>Fecal sludge<br/>safely managed (%)</h4>

        <StyledSlider
            value={scenario.avg(["fecalSludgeTreated_urb", "coverBury_rur"], true)*100}
            onAfterChange={(e) => scenario.setFaecalSlider(e)}
            renderTrack={Track}
            renderThumb={Thumb}
            snapDragDisabled={true}
        />
        {(
              scenario.selectedAreasDialogValues[scenario.activeTab].length !== 0 &&
              scenario.selectedAreasDialogValues[scenario.activeTab].length !== scenario.records.length) &&
              <div style={{textAlign:'center'}} class="difference">Total: {'('+Math.round(scenario.avg(["fecalSludgeTreated_urb", "coverBury_rur"], false)*100) + '%)'}</div>
        }
        </Card>
        
      </Grid>
      <Grid item md={6} lg={6} xs={12}>
      <Card style={{padding:'10px 40px 40px 40px', position:'relative'}}>
      <LightTooltip interactive={true} placement='left' title='The ‘Sewage treatment’ slider bar represents the percentage of sewage from “Flush onsite (sewered)” that actually reaches the treatment plant. You can change this percentage by moving the slider bar to the left or the right.'>
          <IconButton aria-label="info" className='infoPrompt'>
            <InfoOutlinedIcon/>
          </IconButton>
        </LightTooltip>
      <h4>Sewage<br/>treated (%)</h4>
      <StyledSlider
            value={scenario.avg(["sewageTreated_urb", "sewageTreated_rur"], true)*100}
            onAfterChange={(e)=>scenario.setSewageSlider(e)}
            renderTrack={Track}
            renderThumb={Thumb}
            snapDragDisabled={true}
        />
        {(
              scenario.selectedAreasDialogValues[scenario.activeTab].length !== 0 &&
              scenario.selectedAreasDialogValues[scenario.activeTab].length !== scenario.records.length) &&
              <div style={{textAlign:'center'}} class="difference">Total: {'('+Math.round(scenario.avg(["sewageTreated_urb", "sewageTreated_rur"], false)*100) + '%)'}</div>
        }
        </Card>

      </Grid>
      <Box className={classes.enhancedBox}>
  
  
    
      <Grid container>
        <Grid item xs={12} md={8} lg={8}> 
          <Typography style={{display:'inline-block'}} variant="h3" gutterBottom>Pathogen reduction after treatment</Typography>
          <LightTooltip interactive={true} placement='left' title='The ‘Pathogen reduction after treatment’ slider bar represent the reduction (in percentage or the log removal) that the treatment in the subarea achieves. For pathogens high reductions (e.g. 3 log units or more) are required to sufficiently reduce the number of pathogens from the effluent. These reductions are rarely achieved, also in developed countries. The Treatment Plant Sketcher Tool can help you to understand what can be done to increase the reduction after treatment for your subarea. You can change the percentage by clicking on the required log removal.'>
          <IconButton aria-label="info" style={{paddingLeft:'10px', marginTop:'-5px'}}>
            <InfoOutlinedIcon/>
          </IconButton>
        </LightTooltip>
        </Grid>
        <Grid item xs={12} md={4} lg={4} style={{textAlign:'right'}}> 
          <span className="logValue">{-Math.log10(1 - floatValueFormat(scenario.avg(['fRemoval_treatment'], true))).toFixed(1)} log<sub>10</sub></span>
           <span className="percentageValue">({valueFormat(scenario.avg(['fRemoval_treatment'], true)*100)}%)</span>
        </Grid>
      </Grid>

      <Slider
        min={0} //-log10(1 – Percent Reduction/100)
        step={0.01}
        max={6}
        value={-Math.log10(1- scenario.avg(['fRemoval_treatment'], true))}
        scale={(x) => 100*(1 - Math.pow(10, -x))}
        getAriaValueText={valueFormat}
        valueFormat={valueFormat}
        onChangeCommitted={(e, value) => scenario.logChange(value, map.pathogen)}
        marks={marks}
        valueLabelDisplay="auto"
        aria-labelledby="non-linear-slider"
        ValueLabelComponent={ValueLabelComponent}
      />
      {(
              scenario.selectedAreasDialogValues[scenario.activeTab].length !== 0 &&
              scenario.selectedAreasDialogValues[scenario.activeTab].length !== scenario.records.length) &&
              <div style={{textAlign:'center'}} class="difference">Total: {'('+valueFormat(scenario.avg(['fRemoval_treatment'], false)*100) + '%)'}</div>
        }
    </Box>
    </Grid>
    { (scenario.records ) &&
    <AreasDialog scenario={scenario} />
    }
  </div>
});

export default TreatmentFractions;

