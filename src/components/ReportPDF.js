import React from "react";
import { pdf , Page, Text, View, Document, StyleSheet, Image, Font } from '@react-pdf/renderer';
import styled from '@react-pdf/styled-components';
import cover from "../static/img/cover.jpg";
import qr from "../static/img/qr.png";
import gwpp from "../static/img/logo_dark_blue.png";
import cabin from "../static/fonts/cabin-v14-latin-regular.woff";
import toiletCategories from "../config/toiletColumns.json";
import DataHeaders from "../config/DataHeaders.js";
import { toJS } from "mobx";


Font.register({ family: 'Cabin', src: cabin });

const styles = StyleSheet.create({
    page: {
      flexDirection: 'column',
      backgroundColor: 'white',
      fontFamily: 'Cabin'
    },
    footer: {
        backgroundColor: '#a0a0a0',
        width: '100%',
        display:'block',
        padding: '15px',
        textAlign: 'center',
        color: '#fff',
        fontSize:15
      },
    pageNumber: {
        backgroundColor: '#003347',
        width: '120px',
        display:'block',
        padding: '15px',
        textAlign: 'center',
        color: '#fff',
          fontSize:15
      },
      section: {
        marginTop: 20,
        marginBottom: 20,
        marginLeft: 24,
        paddingLeft: 24,
        marginRight: 20,
        paddingRight: 20,
        paddingBottom: 20,
        flexGrow: 1,
        fontFamily: 'Cabin'
      },
  	whiteLine: {
      display:'block',
      width:'50px',
      height: '7px',
      marginTop: '15px',
      backgroundColor: '#fff'
    },
    cover:{
        minWidth: '100%',
        minHeight: '100%',
        height: '100%',
        width: '100%',
        objectFit: 'cover'
    },
    coverHolder: {
      position:'absolute',
      top: '150px',
      backgroundColor: '#003347',
      padding: 40,
      opacity: 0.9
    },
    coverTitle: {
        zIndex:2000,
        fontSize:42,
      	color: '#fff',
    },
  	coverSubtitle: {
        fontSize:32,
      	color: '#fff',
    	marginTop: 10
    },
    coverMeta: {
        fontSize:20,
      	color: '#fff',
    	marginTop: 120
    },
    coverSubMeta: {
        fontSize:14,
      	color: '#fff',
    marginTop:10
    },
  	coverFooter: {
      position:'absolute',
      bottom: '10px',
    	left: '10px'
    },
    qr: {
        height: '100px',
        width: '100px',
        bottom:'10px',
        position:'absolute'
      },
      gwpp: {
        height: '80px',
        left: '120px',
        bottom:'15px'
      },
    image2:{
        width: 250,
        marginRight: 'auto',
        marginLeft: 'auto'
    },
    image:{
        width: 400,
        padding: 20,
        marginRight: 'auto',
        marginLeft: 'auto'
    },
    imageFlow:{
        width: 500,
        padding: 20,
        marginRight: 'auto',
        marginLeft: 'auto'
    },
    table: { 
        marginRight: 10,
        marginLeft:10,
        marginBottom:10,
        borderStyle: "solid", 
        borderWidth: 1, 
        borderRightWidth: 0, 
        borderBottomWidth: 0 
    },
    tableRow: { 
        /*margin: "auto",*/ 
        flexDirection: "row" 
    },
    tableCol: { 
        borderStyle: "solid", 
        borderWidth: 1, 
        borderLeftWidth: 0, 
        borderTopWidth: 0 
    },
    tableCell: { 
        margin: "auto", 
        padding: 5, 
        fontSize: 8 
    },
    imgTable: { 
        marginRight: 10,
        marginLeft:10,
        marginBottom:10,
        borderStyle: "solid", 
        borderWidth: 0, 
        borderRightWidth: 0, 
        borderBottomWidth: 0 
    },
	imgRow: { 
        margin: "auto", 
        flexDirection: "row" 
    },
    imgCol: { 
        borderStyle: "solid", 
        borderWidth: 0, 
        borderLeftWidth: 0, 
        borderTopWidth: 0 
    },
    imgCell: { 
        margin: "auto", 
        padding: 5, 
        fontSize: 8,
        height: '200px'
    },
    caption: {
        fontSize: 10,
        fontFamily: 'Cabin',
        marginTop: "10px",   
    },
    tableImg: {
        width: 150,
        height: 150
    }
});

const Title = styled.Text`
    margin: 20px;
    font-size: 22px;
    font-family: 'Helvetica';
`;

const Subtitle = styled.Text`
    margin: 20px;
    font-size: 10px;
    font-family: 'Helvetica';
`;

const Header = styled.Text`
    margin: 20px;
    font-size: 18px;
    font-family: 'Helvetica';
`;

const Paragraph = styled.Text`
    margin: 20px;
    font-size: 12px;
    font-family: 'Helvetica';
    text-align: justify;
`;

const Caption = styled.Text`
    font-size: 12px;
    font-family: 'Helvetica';
`;

const Subcaption = styled.Text`
    font-size: 8px;
    font-family: 'Helvetica';
`;

const saveBlob = (blob, filename) => {
    var a = document.createElement("a");
    document.body.appendChild(a);
    a.style.display = "none";
    let url = window.URL.createObjectURL(blob);
    a.href = url;
    a.download = filename;
    a.click();
    window.URL.revokeObjectURL(url);
};
  
const savePdf = async (document, filename) => {
    saveBlob(await pdf(document).toBlob(), filename);
};


const getCurrentDate = () => {
    const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    const d = new Date();
    return "Created on: "+monthNames[d.getMonth()]+" "+d.getDate()+", "+d.getFullYear();
}

const sum = (records, fields) => {
    var sum = 0;
    for (var i = 0; i < records.length; i++) {
        for (var j = 0; j < fields.length; j++) {
            sum += records[i][fields[j]];
        }
    }
    return sum;
}

const indexOfMax = (arr) => {
    
    if (arr.length === 0) {
        return -1;
    }

    var max = arr[0];
    var maxIndex = 0;

    for (var i = 1; i < arr.length; i++) {
        if (arr[i] > max) {
            maxIndex = i;
            max = arr[i];
        }
    }
    return maxIndex;
}


const MyDocument = (map) => {
    const report = map.reportData;
    const baseline = map.scenarios[map.baselineScenario].payload;
    const sanitationFractions = [ 
        {name: toiletCategories[0].title, value: Math.round(toJS(baseline.populationWeightedAvg(toiletCategories[0].columns, 'both', false, [])))},
        {name: toiletCategories[1].title, value: Math.round(toJS(baseline.populationWeightedAvg(toiletCategories[1].columns, 'both', false, [])))},
        {name: toiletCategories[2].title, value: Math.round(toJS(baseline.populationWeightedAvg(toiletCategories[2].columns, 'both', false, [])))},
        {name: toiletCategories[3].title, value: Math.round(toJS(baseline.populationWeightedAvg(toiletCategories[3].columns, 'both', false, [])))},
        {name: toiletCategories[4].title, value: Math.round(toJS(baseline.populationWeightedAvg(toiletCategories[4].columns, 'both', false, [])))}
    ].sort(function compare (a,b) { return (b.value-a.value) ? -1:1 });
    
    var scenarios = toJS(map.scenarios);

    const maxEmissionsIndex = indexOfMax(scenarios.map( (scenario) => {
        return scenario.payload.maxValue;
    }));
    const maxScenario = map.scenarios[maxEmissionsIndex];

    const maxEmissionsCategory = indexOfMax(toiletCategories.map((cat, key) => {
        switch(key) {
            case 1:
              return sum(maxScenario.payload.outputRecords, ['total_pitSlab_out','total_pitNoSlab_out','total_bucketLatrine_out','total_containerBased_out']) 
              break;
            case 2:
              return sum(maxScenario.payload.outputRecords, ['total_compostingToilet_out'])
              break;
            case 3:
                return sum(maxScenario.payload.outputRecords, ['total_flushSeptic_out','total_flushPit_out','total_flushOpen_out','total_flushUnknown_out'])
            break;
            case 4:
                return sum(maxScenario.payload.outputRecords, ['total_flushSewer_out'])
            break;
            default:
                return sum(maxScenario.payload.outputRecords, ['total_openDefecation_out','total_other_out','total_hangingToilet_out'])
              // code block
          }
    }));

    var headers = new DataHeaders();
    const columns = headers.analysisEditorfields;
    var firstColumns = columns.splice(0,23);
    var otherColumns = columns.splice(0, columns.length-1);

    scenarios.forEach(function(item,i){
        if ( item.id === map.baselineScenario ) {
            scenarios.splice(i, 1);
            scenarios.unshift(item);
        }
    });
    
    var nonBSscenarios = scenarios;
    nonBSscenarios.shift();


    return <Document>
        
        <Page size="A4" wrap={true} style={styles.page}>
        
        <View>
              
              <Image
                  style={styles.cover}
                  src={cover}
              />
            <View style={styles.coverHolder}>
              <Text style={styles.coverTitle}>
                  Pathogen Flows and
              </Text>
              <Text style={styles.coverTitle}>
                  Mapping Tool Report
              </Text>
              <View style={styles.whiteLine}></View>
                <View><Text style={styles.coverSubtitle}>{report.name}</Text></View>
                <View style={styles.coverMeta}>
                    
                <Text>{report.authorName}</Text>
                <Text style={styles.coverSubMeta}>{report.organization}</Text>
                <Text style={styles.coverSubMeta}>{report.city}, {report.country}</Text>
                <Text style={styles.coverSubMeta}>{report.email}</Text>
                {(report.phone && report.phone !== '') ? <Text style={styles.coverSubMeta}>{report.phone}</Text>:<Text></Text> }
                
              </View>
            </View>
            <View style={styles.coverFooter}>
              <Image
                  style={styles.qr}
                  src={qr}
              />
              <Image
                    style={styles.gwpp}
                    src={gwpp}
                />
            </View>
          </View>
            
        </Page>
        <Page size="A4" wrap style={styles.page}>
        <Text style={styles.pageNumber} render={({ pageNumber }) => (
        `${pageNumber}`
      )} fixed />
            <View style={styles.section}>
                <Text style={{textAlign: 'center', marginTop:'10px', fontFamily: 'Cabin'}}>
                    <Title style={{fontFamily: 'Cabin'}}>Pathogen Flows and Mapping Tool Report: </Title>
                </Text>
                <Text style={{textAlign: 'center', paddingTop: '10px', fontFamily: 'Cabin'}}>
                    <Subtitle style={{fontFamily: 'Cabin', fontSize: '16px'}}>{report.name}</Subtitle>
                </Text>
                <Text style={{textAlign: 'center', paddingTop: '10px', fontFamily: 'Cabin'}}>
                    <Subtitle style={{fontFamily: 'Cabin'}}>Prepared by {report.authorName}</Subtitle>
                </Text>
                <Text style={{textAlign: 'center', paddingTop: '10px', fontFamily: 'Cabin'}}>
                    <Subtitle style={{fontFamily: 'Cabin'}}>{getCurrentDate()}</Subtitle>
                </Text>
                
                <Text style={{paddingTop: '20px', fontFamily: 'Cabin'}}>
                    <Header style={{fontFamily: 'Cabin'}}>Introduction</Header>
                </Text>
                <Text style={{paddingTop: '10px', fontFamily: 'Cabin'}}>
                    <Paragraph style={{fontFamily: 'Cabin'}}>
                        This report presents a summary of the results produced using the GWPP-K2P Pathogen Flow and Mapping Tool (PFM Tool). The purpose of the PFM Tool is to predict areas with high emissions of pathogens to surface waters and evaluate the potential impact of changes in population growth and changes in access to improved sanitation facilities and increased conveyance and treatment of wastewater and fecal sludge. A model that simulates these emissions using input data on population, urbanization, disease incidence and pathogen shedding rates, sanitation technologies, and the treatment of wastewater and fecal sludge drives the tool. The model is explained in more detail on the website of the tool (http://www.waterpathogens.org/). The current global input data for the default run of the tool are described on the tool website  and come from sources that are available online, such as the UN Department of Economic and Social Affairs, and the Joint Monitoring Program of the World Health Organization (WHO) and the United Nations’ Children’s Fund (UNICEF). 
                    </Paragraph>   
                </Text>
                <Text style={{paddingTop: '10px', fontFamily: 'Cabin'}}>
                    <Paragraph style={{ fontFamily: 'Cabin' }}>
                        Users of the tool are able to select their geographic area of interest, adapt the baseline (default data) using their own data, and develop up to three alternative scenarios that can be compared to the baseline. These scenarios can include interventions, such as eradicating open defecation, emptying and treating more waste from onsite systems, or improving the treatment of wastewater and fecal sludge. The tool produces visualizations that help a user assess the relative differences between scenarios to support decision-making. 
                    </Paragraph>
                </Text>
                
            </View>
            <View style={styles.footer} fixed><Text>www.waterpathogens.org</Text></View>
        </Page>
        <Page size="A4" wrap style={styles.page}>
        <Text style={styles.pageNumber} render={({ pageNumber }) => (
        `${pageNumber}`
      )} fixed />
            <View style={styles.section}>
                <Text style={{paddingTop: '20px', fontFamily: 'Cabin'}}>
                    <Header style={{fontFamily: 'Cabin'}}>Area Demographics, Disease Incidence and Sanitation Systems</Header>
                </Text>
                <Text style={{paddingTop: '10px', fontFamily: 'Cabin'}}>
                    <Paragraph style={{ fontFamily: 'Cabin' }}>
                    <Text style={{fontFamily: 'Cabin'}} >This report presents the estimated pathogen emissions to surface waters and the sources of these emissions for </Text> 
                        {map.selectedAreasList.map((area, key) => {
                            var records = map.selectedAreasDictionary.filter(r => r.id === area);
                            return <Text style={{fontFamily: 'Cabin'}} key={key}>
                                {(records.length > 0 ?
                                    records[0]["area"] : area
                                )}
                                {(key < map.selectedAreasList.length - 1) ? ', ' : '. '}
                            </Text>
                        })
                        }
                        <Text style={{fontFamily: 'Cabin'}}>The emissions for {map.pathogen === 'Virus' ? <Text style={{fontFamily: 'Cabin'}}>viruses</Text>: <Text style={{fontFamily: 'Cabin'}}>protozoa</Text>} are simulated using an 
                        average disease incidence for {map.pathogen === 'Virus' ? <Text style={{fontFamily: 'Cabin'}}>Rotavirus</Text>: <Text style={{fontFamily: 'Cabin', fontStyle: 'italic'}}>Cryptosporidium</Text>} of <Text style={{fontFamily: 'Cabin'}}>{baseline.records[0]["incidence_urban_5plus"]}</Text> episodes per person per year. 
                        Under the baseline condition, the sanitation technologies used in the area, in order of most commonly used to least commonly used, are: </Text>
                        {
                            sanitationFractions.map((fraction, key) => {
                                if (fraction.value > 0) {
                                    return <Text>"{fraction.name}"
                                        {(key < sanitationFractions.filter( fraction => fraction.value > 0).length - 1) ? ', ' : '. '}
                                    </Text>
                                }
                                else {
                                    return <Text></Text>;
                                }
                                
                            })
                        }                       
                    </Paragraph>
                </Text>
                
                <Text style={{paddingTop: '10px', fontFamily: 'Cabin'}}>
                    {(map.scenarios.length > 1) ?
                        <Paragraph style={{ fontFamily: 'Cabin' }}>
                        The baseline run was developed together with {map.scenarios.length - 1 } {map.scenarios.length === 2 ? <Text>scenario</Text>:<Text>scenario</Text>}, called
                        { nonBSscenarios.map((scenario, key) => {
                            return <Text>
                                {" \""+scenario.title+"\""}
                                {(key < nonBSscenarios.length - 1) ? ',' : '. '}
                            </Text>
                        })} A summary of the input data for each of the runs is available in Figure 1 and Appendix I. Detailed model inputs can be found in the .csv output file of the tool.
                        </Paragraph>
                    :
                        <Paragraph style={{ fontFamily: 'Cabin' }}>
                            A summary of the input data for the baseline is available in Figure 1 and Appendix I. Detailed model input can be found in the .csv output file of the tool.
                        </Paragraph>
                    }
                </Text>
                
                                                                
                <View style={styles.image}>
                    <Image src={map.sanitationLadder}></Image>
                    <Caption style={styles.caption}>Figure 1. A ladder of the availability of sanitation categories in the selected study area for the defined scenarios. Fractions are population corrected.</Caption>      
                </View>
                
                <Text style={{paddingTop: '10px', fontFamily: 'Cabin'}}>
                    <Paragraph style={{ fontFamily: 'Cabin' }}>
                    The maps (Figure 2) show the predicted pathogen emissions to surface waters for the baseline and for each of the scenario(s). The pathogen emissions are illustrated on the maps using a color scheme, where each color corresponds with a 10% change in the pathogen emissions. The cells with colors that are closer to red have the highest emissions. By comparing the baseline scenario with alternative scenario(s), hotspot areas (with high emissions) can be identified. 
                    </Paragraph>
                </Text>
                <View style={[styles.imgTable], {marginTop:'10px'}}> 
                    {/* TableHeader */} 
                    <View style={styles.imgRow}>
                        <View style={[styles.imgCol,{width:'50%'}]}> 
                            { (baseline.png !== '') ? 
                                <Text style={styles.imgCell}>
                                    <Image style={styles.tableImg} src={baseline.png}></Image>
                                    {/* <Caption style={styles.caption}>{scenarios[0].title}</Caption> */}
                                </Text> :
                                <Text></Text>
                            }
                        </View>
                        <View  style={[styles.imgCol,{width:'50%'}]}>  
                            { (nonBSscenarios.length > 0 && nonBSscenarios[0].payload.png !== '') ? 
                                <Text style={styles.imgCell}>
                                    <Image style={styles.tableImg} src={nonBSscenarios[0].payload.png}></Image>
                                    {/* <Caption style={styles.caption}>{scenarios[1].title}</Caption> */}
                                </Text> :
                                <Text></Text>
                            } 
                        </View> 
                    </View> 
                    { (scenarios.length > 1 && (nonBSscenarios[1].payload.png !== '' || nonBSscenarios[2].payload.png !== '')) && 
                    <View style={styles.imgRow}>
                        <View style={[styles.imgCol,{width:'50%'}]}>  
                            { (nonBSscenarios[1].payload.png !== '') ? 
                                <Text style={styles.imgCell}>
                                    <Image style={styles.tableImg} src={nonBSscenarios[1].payload.png}></Image>
                                    {/* <Caption style={styles.caption}>{scenarios[2].title}</Caption> */}
                                </Text> :
                                <Text></Text>
                            } 
                        </View> 
                        <View  style={[styles.imgCol,{width:'50%'}]}>  
                            { (nonBSscenarios.length > 2 && nonBSscenarios[2].payload.png !== '') ? 
                                <Text style={styles.imgCell}>
                                    <Image style={styles.tableImg} src={nonBSscenarios[2].payload.png}></Image>
                                    {/* <Caption style={styles.caption}>{scenarios[3].title}</Caption> */}
                                </Text> :
                                <Text></Text>
                            } 
                        </View>  
                    </View> 
                    }
                    {/* TableContent */}
                </View>
                <Text style={{paddingTop: '10px', fontFamily: 'Cabin'}}>
                    <Paragraph style={{ fontFamily: 'Cabin' }}>
                        The bar charts (Figure 3) illustrate the breakdown of pathogen emissions to surface waters by sanitation 
                        technology type for the baseline and the scenario(s). Figure 3 shows that {"\""+maxScenario+"\""} 
                produces the highest emissions. The main contributor to these emissions is the {"\""+toiletCategories[maxEmissionsCategory].title+"\""} category.    
                    </Paragraph>
                </Text>
                <View style={styles.image}>
                    <Image src={baseline.outputLadder}></Image>
                    <Caption style={styles.caption}>Figure 3.1. A ladder of pathogen emissions by sanitation category for the baseline scenario.</Caption>
                </View>
                {nonBSscenarios.map((scenario,key) => { 
                    return <View style={styles.image}>
                                <Image src={scenario.payload.outputLadder}></Image>
                                <Caption style={styles.caption}>Figure 3.{key+2}. A ladder of pathogen emissions by sanitation category for the {"\""+scenario.title+"\""} scenario.</Caption>

                            </View>
                })}

                     

            </View>
            <View style={styles.footer} fixed><Text>www.waterpathogens.org</Text></View>
        </Page>
        <Page size="A4" wrap style={styles.page}>
        <Text style={styles.pageNumber} render={({ pageNumber }) => (
        `${pageNumber}`
      )} fixed />
            <View style={styles.section}>
            <Text style={{textAlign: 'center', marginTop:'10px', fontFamily: 'Cabin'}}>
                    <Title style={{fontFamily: 'Cabin'}}>Contact us!</Title>
                </Text>
                <Text style={{textAlign: 'center', paddingTop: '20px', fontFamily: 'Cabin'}}>
                    <Header style={{fontFamily: 'Cabin'}}>Case Study Lead Author</Header>
                </Text>
                <Text style={{textAlign: 'center',paddingTop: '10px', fontFamily: 'Cabin'}}>
                    <Paragraph style={[{textAlign: 'center'},{fontFamily: 'Cabin'}]}>{report.authorName}</Paragraph>
                </Text>
                <Text style={{textAlign: 'center',paddingTop: '5px', fontFamily: 'Cabin'}}>
                    <Paragraph style={[{textAlign: 'center'},{fontFamily: 'Cabin'}]}>{report.organization}</Paragraph>
                </Text>
                {(report.state && report.state !== '') ?
                <Text style={{textAlign: 'center',paddingTop: '5px', fontFamily: 'Cabin'}}>
                    <Paragraph style={[{textAlign: 'center'},{fontFamily: 'Cabin'}]}>{report.street}</Paragraph>
                </Text>:<Text></Text>}
                <Text style={{textAlign: 'center',paddingTop: '5px', fontFamily: 'Cabin'}}>
                    <Paragraph style={[{textAlign: 'center'},{fontFamily: 'Cabin'}]}>{report.city}, {(report.state && report.state !== '') ? <Text>{report.state}, </Text>:<Text></Text>}{report.country}</Paragraph>
                </Text>
                <Text style={{textAlign: 'center',paddingTop: '5px', fontFamily: 'Cabin'}}>
                    <Paragraph style={[{textAlign: 'center'},{fontFamily: 'Cabin'}]}>{report.email}</Paragraph>
                </Text>
                {(report.phone && report.phone !== '') ?
                    <Text style={{textAlign: 'center',paddingTop: '5px', fontFamily: 'Cabin'}}>
                        <Paragraph style={[{textAlign: 'center'},{fontFamily: 'Cabin'}]}>{report.phone}</Paragraph>
                    </Text>:<Text></Text>
                }
               

                <Text style={{textAlign: 'center', paddingTop: '20px', fontFamily: 'Cabin'}}>
                    <Header style={{fontFamily: 'Cabin'}}>Pathogen Flow &amp; Mapping Tool</Header>
                </Text>
                <Text style={{textAlign: 'center',paddingTop: '20px', fontFamily: 'Cabin'}}>
                    <Paragraph style={[{textAlign: 'center'},{fontFamily: 'Cabin'}]}>K2P Project Team</Paragraph>
                </Text>
                
                <Text style={{paddingTop: '120px', fontFamily: 'Cabin'}}>
                    <Subcaption style={{fontFamily: 'Cabin'}}>The functioning of the pathogen flow and mapping tool is based on models developed by the K2P Team with support data from the Global Water Pathogen Project (www.waterpathogens.org/) to predict the fate and transport of pathogens in the environment throughout the sanitation service chain. More details about the models and key assumptions can be found on our website (https://www.waterpathogens.org) and (https://www.waterpathogens.org/tools/pathogen-flow-and-mapping-tool).
                    </Subcaption>
                </Text>
            </View>
            <View style={styles.footer} fixed><Text>www.waterpathogens.org</Text></View>
        </Page>
        {baseline.records.map((record, key) => {
                    return [<Page size="A4" wrap style={styles.page}>
            <Text style={styles.pageNumber} render={({ pageNumber }) => (
                `${pageNumber}`
            )} />
                <View style={styles.section}>
                    {(key === 0 ) && <Text style={{textAlign: 'center', marginTop:'10px', fontFamily: 'Cabin'}}>
                            <Title style={{fontFamily: 'Cabin'}}>Appendix A</Title>
                        </Text>
                    }
                    <Text style={{paddingTop: '10px', paddingBottom:'10px', fontFamily: 'Cabin'}}>
                    <Header style={{fontFamily: 'Cabin'}}>{baseline.records[key].subarea}</Header>
                </Text>
                <View style={styles.table}> 
                    {/* TableHeader */} 
                    <View style={styles.tableRow}>
                        <View  style={[styles.tableCol,{width:((4-nonBSscenarios.length)*20+'%')}]}> 
                            <Text style={styles.tableCell}>Variable</Text> 
                        </View>
                        <View  style={[styles.tableCol,{width:'20%'}]}>  
                            <Text style={styles.tableCell}>Baseline</Text> 
                        </View> 
                        {(nonBSscenarios.length > 0 ) &&
                            <View  style={[styles.tableCol,{width:'20%'}]}> 
                                <Text style={styles.tableCell}>{nonBSscenarios[0].title}</Text> 
                            </View> 
                        }
                        {(nonBSscenarios.length > 1 ) &&
                            <View  style={[styles.tableCol,{width:'20%'}]}>  
                                <Text style={styles.tableCell}>{nonBSscenarios[1].title}</Text> 
                            </View> 
                        }
                        {(nonBSscenarios.length > 2 ) &&
                            <View  style={[styles.tableCol,{width:'20%'}]}>  
                                <Text style={styles.tableCell}>{nonBSscenarios[2].title}</Text> 
                            </View> 
                        }
                    </View> 
                    {/* TableContent */}
                    {firstColumns.map((column) => { 
                        if (column.header.indexOf('_rur') !== -1 || column.omitFromPdf) {
                            return <Text></Text>
                        }
                        else {
                        return <View style={styles.tableRow}>
                                <View style={[styles.tableCol,{width:((4-nonBSscenarios.length)*20+'%')}]}> 
                                    <Text style={styles.tableCell}>{(column.header.indexOf('_urb') !== -1 && column.header.indexOf('_urban') === -1) ? column.header.replace("_urb",""): column.header}</Text> 
                                </View> 
                                {map.scenarios.map((scenario) => {
                                    
                                        if (column.header.indexOf('_urb') !== -1 && column.header.indexOf('_urban') === -1) {
                                            return <View style={[styles.tableCol,{width:'20%'}]}>  
                                                    <Text style={styles.tableCell}>
                                                        {scenario.payload.records[key][column.header] + ' (urban)'}
                                                    </Text> 
                                                    <Text style={styles.tableCell}>
                                                        {scenario.payload.records[key][column.header.replace('_urb', '_rur')] + ' (rural)'}
                                                    </Text> 
                                                </View>
                                        }
                                        else {
                                            return <View style={[styles.tableCol,{width:'20%'}]}>  
                                                    <Text style={styles.tableCell}>
                                                        {scenario.payload.records[key][column.header]}
                                                    </Text> 
                                                </View>
                                        }
                                    
                                })}
                                
                            </View>
                        }
                    }) }
                    
                </View>
                
                    
                </View>
            <View style={styles.footer} fixed><Text>www.waterpathogens.org</Text></View>
        </Page>,
        <Page size="A4" wrap style={styles.page}>
            <Text style={styles.pageNumber} render={({ pageNumber }) => (
                `${pageNumber}`
            )} />
            <View style={styles.section}>
            <View style={styles.table}> 
                    {/* TableHeader */} 
                    <View style={styles.tableRow}>
                        <View  style={[styles.tableCol,{width:((4-nonBSscenarios.length)*20+'%')}]}> 
                            <Text style={styles.tableCell}>Variable</Text> 
                        </View>
                        <View  style={[styles.tableCol,{width:'20%'}]}>  
                            <Text style={styles.tableCell}>Baseline</Text> 
                        </View> 
                        {(nonBSscenarios.length > 0 ) &&
                            <View  style={[styles.tableCol,{width:'20%'}]}> 
                                <Text style={styles.tableCell}>{nonBSscenarios[0].title}</Text> 
                            </View> 
                        }
                        {(nonBSscenarios.length > 1 ) &&
                            <View  style={[styles.tableCol,{width:'20%'}]}>  
                                <Text style={styles.tableCell}>{nonBSscenarios[1].title}</Text> 
                            </View> 
                        }
                        {(nonBSscenarios.length > 2 ) &&
                            <View  style={[styles.tableCol,{width:'20%'}]}>  
                                <Text style={styles.tableCell}>{nonBSscenarios[2].title}</Text> 
                            </View> 
                        }
                    </View> 
                    {/* TableContent */}
                    {otherColumns.map((column) => { 
                        if (column.header.indexOf('_rur') !== -1 || column.omitFromPdf) {
                            return <Text></Text>
                        }
                        else {
                        return <View style={styles.tableRow}>
                                <View style={[styles.tableCol,{width:((4-nonBSscenarios.length)*20+'%')}]}> 
                                    <Text style={styles.tableCell}>{(column.header.indexOf('_urb') !== -1 && column.header.indexOf('_urban') === -1) ? column.header.replace("_urb",""): column.header}</Text> 
                                </View> 
                                {map.scenarios.map((scenario) => {
                                    
                                        if (column.header.indexOf('_urb') !== -1 && column.header.indexOf('_urban') === -1) {
                                            return <View style={[styles.tableCol,{width:'20%'}]}>  
                                                    <Text style={styles.tableCell}>
                                                        {scenario.payload.records[key][column.header] + ' (urban)'}
                                                    </Text> 
                                                    <Text style={styles.tableCell}>
                                                        {scenario.payload.records[key][column.header.replace('_urb', '_rur')] + ' (rural)'}
                                                    </Text> 
                                                </View>
                                        }
                                        else {
                                            return <View style={[styles.tableCol,{width:'20%'}]}>  
                                                    <Text style={styles.tableCell}>
                                                        {scenario.payload.records[key][column.header]}
                                                    </Text> 
                                                </View>
                                        }
                                    
                                })}
                                
                            </View>
                        }
                    }) }

                </View>
            </View>
            <View style={styles.footer} fixed><Text>www.waterpathogens.org</Text></View>
            </Page>
        ]
        })}
    </Document>
}

const ReportPDF = (map, setButton) => {
    savePdf(MyDocument(map), "report.pdf").then(() => setButton())
}



export default ReportPDF;