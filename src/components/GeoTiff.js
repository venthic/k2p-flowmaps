import React, { Component } from "react";
import { withLeaflet, MapLayer } from "react-leaflet";
import L from "leaflet";
import "leaflet-geotiff";
import "leaflet-geotiff/leaflet-geotiff-plotty";

class GeotiffLayer extends MapLayer {
  createLeafletElement(props) {
    const { url, options } = props;
    return L.leafletGeotiff(url, options);
  }

  componentDidMount() {
    const { map } = this.props.leaflet;
    this.leafletElement.addTo(map);
  }
}

export const PlottyGeotiffLayer = withLeaflet(props => {
  // console.log(L.LeafletGeotiff.plotty);
  // L.LeafletGeotiff.plotty.Plotty.addColorScale("panagis", ["grey","white","#D1BBD7","#BA8DB4","#AA6F9E","#882E72","#1965B0","#5289C7","#7BAFDE","#4EB265","#90C987","#CAE0AB","#F7F056","#F6C141","#F1932D","#E8601C","#DC050C","#72190E"]);
  const { options, layerRef } = props;
  options.renderer = new L.LeafletGeotiff.Plotty(options);
  // console.log(options.renderer);
  return <GeotiffLayer ref={layerRef} {...props} />;
});
