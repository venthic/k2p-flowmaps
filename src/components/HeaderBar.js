import AppBar from "@material-ui/core/AppBar/AppBar";
import Toolbar from "@material-ui/core/Toolbar/Toolbar";
import Grid from "@material-ui/core/Grid/Grid";
import MailRoundedIcon from '@material-ui/icons/MailRounded';
import Link from '@material-ui/core/Link';
import React from "react";
import logo from '../static/img/logo_dark_blue.png';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import InfoRoundedIcon from '@material-ui/icons/InfoRounded';
import LayersTwoToneIcon from '@material-ui/icons/LayersTwoTone';
import { observer } from "mobx-react-lite";
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import ArrowLeftRoundedIcon from '@material-ui/icons/ArrowLeftRounded';
import { useLocation } from 'react-router-dom';
import PublicTwoToneIcon from '@material-ui/icons/PublicTwoTone';
import Tooltip from "@material-ui/core/Tooltip";

const useStyles = makeStyles(theme => ({
    root: {
      padding: theme.spacing(1, 2),
    },
    link: {
      display: 'flex',
      marginTop: '5px'
    },
    k2pLink: {
        fontWeight: '800',
        fontFamily: '"Roboto", sans-serif',
        color: '#003347',
        borderLeft: '1px dotted #ccc',
        paddingLeft: '20px',
      },
    icon: {
      marginRight: theme.spacing(0.5),
      width: 20,
      height: 20,
    },
  }));

  const LightTooltip = withStyles((theme) => ({
    tooltip: {
      backgroundColor: theme.palette.common.white,
      color: 'rgba(0, 0, 0, 0.87)',
      boxShadow: '0 1px 15px rgba(0,0,0,.04),0 1px 6px rgba(0,0,0,.04)',
      fontSize: 14,
      fontWeight: 500
    },
  }))(Tooltip);

const HeaderBar = observer(({ context }) => {
    const classes = useStyles();
    const map = context; 
    const location = useLocation();

    return <AppBar position='fixed' style={{zIndex:1201, height: 65}} color="default">
        <Toolbar>
            <Grid container>
                <Grid item xs={6} style={{paddingTop: '10px'}}>
                    <img src={logo} style={{float:'left', paddingRight:'20px'}} width={100}/>
                    <Breadcrumbs aria-label="breadcrumb" style={{float:'left'}}>
                    
                    <Link color="inherit" href="#" className={classes.link}>
                    <LayersTwoToneIcon className={classes.icon} />
                        K2P Pathogen Flow & Mapping Tool
                    </Link>
                </Breadcrumbs>
                </Grid>
                <Grid item xs={6} >
                <Button style={{
                    float: 'right',
                    marginTop: '12px',
                    marginLeft: '5px'
                    }} 
                    size='small'
                    startIcon={<MailRoundedIcon/>} 
                    variant="outlined" 
                    color="secondary" 
                    aria-label="Suggest a case study" 
                    target='_blank'
                    href='https://www.waterpathogens.org/tools/suggest-a-case-study'>          
                        Suggest a case study
                </Button>
                <Button style={{
                    float: 'right',
                    marginTop: '12px',
                    marginLeft: '5px'
                    }} 
                    size='small'
                    startIcon={<InfoRoundedIcon/>} 
                    variant="outlined" 
                    color="secondary" 
                    aria-label="More Information" 
                    onClick={()=> map.setInfoState(true)}>          
                        More Information
                </Button>
                <ButtonGroup aria-label="Back" size='small' style={{float:'right', marginTop:'12px'}}>
                    {(map.scenarioMode || map.summaryMode) &&
                        <Button 
                            startIcon={<ArrowLeftRoundedIcon />} 
                            onClick={() => {
                                if (map.summaryMode) {
                                    map.toggleSummaryMode();
                                }
                                else {
                                    map.setBaselineScenario(-1);
                                    map.toggleGeofencingMode(true);
                                    if (map.colorLegendState) {
                                        map.toggleColorLegendState();
                                    }
                                    if (map.infoMapDialogState) {
                                            map.toggleInfoMapDialogState();
                                        }
                                    if (map.scenarioMode) {
                                        map.toggleScenarioMode();
                                        map.eraseScenarios();
                                    }
                                    if (map.summaryMode) {
                                        map.toggleSummaryMode();
                                    }
                                }
                            }}
                        >Back
                        </Button>
                    }
                    {((map.searchedCountry != '' || map.selectedAreasList.length != 0) && !location.pathname.includes('world')) &&
                    <Button onClick={() => {document.location.href = `http://${ window.location.host }/maps`}}>Cancel</Button>
                    }
                    
                </ButtonGroup>
                {(!location.pathname.includes('world')) &&
                    <LightTooltip interactive={true} placement='bottom' title='View the Global Map'>
                    <IconButton
                    component="span" 
                    style={{float: 'right',
                        marginTop: '3px',
                        marginLeft: '5px'
                    }} 
                    color='secondary'
                    onClick={() => {document.location.href = `http://${ window.location.host }/maps/world`}}>
                        <PublicTwoToneIcon/>
                    </IconButton>
                    </LightTooltip>
                    }
                {/* <ButtonGroup aria-label="Publishing options" size='small' style={{float:'right', paddingTop:'12px'}}>
                        <Button style={{ 
                                    color: '#fff',
                                    backgroundColor: 'rgb(56, 194, 122)',
                                    width: '120px'
                                }}
                                onClick={(e) => {context.setImportModalState(true)}}
                                startIcon={<FolderOpenTwoToneIcon fontSize="small"/>}>
                             Import
                        </Button>
                        <Button style={{ 
                                    color: '#fff',
                                    backgroundColor: 'rgb(24, 150, 85)',
                                    width: '120px'
                                }}
                                onClick={(e) => {context.download()}}
                                startIcon={<GetAppTwoToneIcon fontSize="small"/>}>Download
                        </Button>
                        <Button size='small' style={{
                                    backgroundColor:'rgb(13, 132, 70)',
                                    color: '#fff',
                                    width: '120px'
                                }} 
                                onClick={(e)=>{(context.isValid()) && context.setCatalogueModalState(true)}}
                                startIcon={<PublishTwoToneIcon fontSize="small"/>}>Publish
                        </Button>
                    </ButtonGroup>
                          <div style={{float: 'right'}}>
                            <FormControl style={{width: '130px'}}>
                                <InputLabel id="pan-label">Pan to Node</InputLabel>
                                <Select
                                    labelId="pan-label"
                                    id="pan-select"
                                    value={(graph.selectedNode != undefined) ? graph.selectedNode[NODE_KEY]: ''}
                                    onChange={graph.onSelectPanNode}
                                    style={{ width: '100%'}}
                                >
                                <MenuItem value="">
                                    <em>None</em>
                                </MenuItem>
                                {nodes.map(node => (
                                    <MenuItem value={node[NODE_KEY]}>{node.title}</MenuItem>
                                ))}
                                </Select>
                            </FormControl>
                            <IconButton style={{ paddingTop:'20px'}} color="secondary" aria-label="More Information" onClick={()=> graph.setInfoState(true)}>
                        <HelpTwoToneIcon  size='large'/>
                        
                    </IconButton>
                    <IconButton style={{paddingTop:'20px', paddingLeft:'0px'}} color="secondary" aria-label="Screenshot" onClick={graph.takeScreenshot}>
                                <PhotoCameraTwoToneIcon size='large'/>
                            </IconButton>
                            </div> */}
                
                </Grid>
            </Grid>
        </Toolbar>
    </AppBar>
});

export default HeaderBar;