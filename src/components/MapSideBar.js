import { observer } from "mobx-react-lite";
import { toJS} from "mobx";
import * as React from "react";
import Grid from "@material-ui/core/Grid/Grid";
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import RadioGroup from '@material-ui/core/RadioGroup';
import CircularProgress from '@material-ui/core/CircularProgress';
import Radio from '@material-ui/core/Radio';
import Box from '@material-ui/core/Box';
import { ThemeProvider } from "@material-ui/core/styles";
import theme from "../config/Theme.js";
import SavedScenarios from "./SavedScenarios.js";
import MouseTwoToneIcon from '@material-ui/icons/MouseTwoTone';
import Typography from '@material-ui/core/Typography';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import PlayCircleFilledTwoToneIcon from '@material-ui/icons/PlayCircleFilledTwoTone';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import SearchIcon from '@material-ui/icons/Search';
import ladder from '../static/img/ladder.svg';
import AddToPhotosTwoToneIcon from '@material-ui/icons/AddToPhotosTwoTone';
import SummaryTab from "./SummaryTab.js";
import ZoomInTwoToneIcon from '@material-ui/icons/ZoomInTwoTone';
import { Accordion, AccordionSummary, AccordionDetails } from '@material-ui/core';
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select/Select";
import MenuItem from "@material-ui/core/MenuItem/MenuItem";
import Tooltip from "@material-ui/core/Tooltip";
import OutlinedInput from '@material-ui/core/OutlinedInput';
import L from "leaflet";

const useStyles = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(5, 2),
        color: theme.palette.text.secondary,
        backgroundColor: '#09142714',
    },
    wrapper: {
        height: '100vh',
    },
    welcomeTxt: {
        color: '#003347',
    },
    snackbar: {
        margin: theme.spacing(1),
        backgroundColor: '#003347',
        boxShadow: 'none',
        textAlign: 'center',
        marginBottom: '30px',
    },
    logoBox: {
        padding: '25px',
        textAlign: 'center',
    },
    formControl: {
        width: '100%'
    },
    heading: {
        fontSize: theme.typography.pxToRem(20),
        flexBasis: '33.33%',
        flexShrink: 0,
        '& span': {
            fontWeight: 800
        }
    },
    secondaryHeading: {
        fontSize: theme.typography.pxToRem(15),
        color: theme.palette.text.secondary,
    },
    enhancedBox: {
        padding: 20,
        marginTop: 10,
        marginBottom: 10,
        borderRadius: 2,
        backgroundColor: '#eee',
        boxShadow: '0 1px 15px rgba(0,0,0,.04), 0 1px 6px rgba(0,0,0,.04)',
        border: '1px solid #e7e7e7'
    }
}));

const LightTooltip = withStyles((theme) => ({
    tooltip: {
      backgroundColor: theme.palette.common.white,
      color: 'rgba(0, 0, 0, 0.87)',
      boxShadow: '0 1px 15px rgba(0,0,0,.04),0 1px 6px rgba(0,0,0,.04)',
      fontSize: 14,
      fontWeight: 500
    },
  }))(Tooltip);


const MapSideBar = observer(({ context }) => {

    const map = context;
    const classes = useStyles();
    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    var resolutionLevels = [];
    for (var i = 0; i < 1; i++) {
        resolutionLevels.push(
            <FormControlLabel key={i} value={i + ''} control={
                <Radio value={i + ''} onChange={(e) => {
                    if (e.target.checked) {
                        map.setHighResolutionDatasetId(null, null);
                    }
                }}
                    checked={map.resolution == i} />} label={(i == 0) ? "Country-wide analysis" : "Administrative Level " + i} />
        )
    }

    return (
        <ThemeProvider theme={theme()}>

            <Grid item
                md={(!map.summaryMode) ? 4: 6} 
                lg={(!map.summaryMode) ? 5 : 7} 
                xs={12} 
                style={{ borderLeft: '1px solid #003347', transition:"0.5s" }}>
                <div
                    role="presentation"
                >
                    {(!map.scenarioMode) ?
                        <div>
                            <Accordion square disabled={(map.searchedCountry != '' || map.selectedAreasList.length != 0)} expanded={(map.searchedCountry == '' && map.selectedAreasList.length == 0)}>
                                <AccordionSummary aria-controls="geoselection" id="geoselection">
                                    <Typography className={classes.heading}><span>Step 1:</span> Get started</Typography>
                                    <Typography className={classes.subHeading}>{map.searchedCountry}</Typography>
                                </AccordionSummary>
                                <AccordionDetails>
                                    {(map.focusedCountry === '') ? <div style={{ textAlign: 'center', padding: '40px', backgroundColor: '#fff', borderRadius: '10px' }}>
                                        <p><SearchIcon fontSize='large' style={{ color: '#42919e' }} /></p>
                                        <Typography variant='subtitle1' style={{ color: 'rgb(53, 53, 53)' }}>Search for or click on a country on the map.</Typography>
                                    </div> :
                                        <div style={{ padding: '40px', textAlign: 'center' }} >
                                            <Typography variant='h2' style={{ marginBottom: '30px', fontSize: '25px' }}>{map.focusedCountryLongName}</Typography>
                                            {(map.resolutionDataLoading) ?
                                                <Container style={{ marginBottom: '30px' }}><CircularProgress color="primary" /></Container>
                                                : <Box className={classes.enhancedBox} style={{ marginBottom: '30px' }}>
                                                    { (map.worldData.filter(record => { return record.gid===map.focusedCountry && record['notes1'] === '' }).length > 0) ?

                                                        <Grid container alignItems="center" className={classes.root}>
                                                            <Grid item md={6} lg={6} xs={12} style={{ borderRight: '1px solid #ddd' }}>
                                                                <Grid container alignItems="center" style={{display:'block'}} >
                                                                    <img style={{ padding: '10px', height: '40px' }} src={ladder} />
                                                                    <div style={{ pading: '10px' }}>JMP data available</div>
                                                                </Grid>
                                                            </Grid>
                                                            <Grid item md={6} lg={6} xs={12}>
                                                                <Grid container alignItems="center" style={{display:'block'}} >
                                                                    <div>{map.resolutionDatasets.length} K2P dataset(s) available</div>
                                                                </Grid>
                                                            </Grid>
                                                            {/* <span>Data from the <b>Joint Monitoring Programme</b> for Water Supply and Sanitation by WHO and UNICEF.</span> */}

                                                        </Grid>



                                                        : <div>
                                                            <Grid container alignItems="center" className={classes.root}>
                                                                <Grid item md={6} lg={6} xs={12}>
                                                                    <Grid container alignItems="center" style={{ placeContent: 'flex-end', paddingRight: 20 }}>
                                                                        <img className="nojmp" style={{ padding: '10px', height: '40px' }} src={ladder} />
                                                                        <div style={{ pading: '10px' }}>No JMP data available</div>
                                                                    </Grid>
                                                                </Grid>
                                                                <Grid item md={6} lg={6} xs={12}>
                                                                    <Grid container alignItems="center" style={{ placeContent: 'flex-start', paddingLeft: 20 }}>
                                                                        <div>No K2P datasets available</div>
                                                                    </Grid>
                                                                </Grid>
                                                                {/* <span>Data from the <b>Joint Monitoring Programme</b> for Water Supply and Sanitation by WHO and UNICEF.</span> */}

                                                            </Grid>
                                                        </div>
                                                    }
                                                </Box>
                                            }
                                            <ButtonGroup>

                                                <Button
                                                    variant="contained"
                                                    color="primary"
                                                    startIcon={<ZoomInTwoToneIcon />}
                                                    disabled={map.mapLoading || !map.hasHighResolutionData(map.focusedCountry)}
                                                    onClick={() => {
                                                        var countryId = Object.keys(map.mapRef.current.leafletElement._layers)
                                                            .filter((i) => {
                                                                if (map.mapRef.current.leafletElement._layers[i].feature) {
                                                                    return map.mapRef.current.leafletElement._layers[i].feature.id == map.focusedCountry;
                                                                }
                                                                return false;
                                                            })[0];
                                                        var countryBounds = map.mapRef.current.leafletElement._layers[countryId]._bounds;
                                                        map.isMapLoading(true);
                                                        map.mapRef.current.leafletElement.flyToBounds(countryBounds);
                                                        map.countriesRef.current.leafletElement.resetStyle(map.mapRef.current.leafletElement._layers[countryId]);
                                                            
                                                        //const base = 'https://data.waterpathogens.org/dataset/c4a395f1-5e48-4109-b02f-951b8be89275/resource/4bab8a9d-43e0-454f-9994-1af42b51df1e/download/kampala.zip';
                                                        // const countryDataset = map.countryData.filter((record) => record.tags.filter((tag) => tag.name===map.focusedCountry).length > 0)[0];
                                                        // const base = countryDataset.resources.filter(r => r.name.toLowerCase() === 'shapefile' || r.mimetype==='application.zip')[0].url;
                                                        // const resolution = 
                                                        fetch("https://data.waterpathogens.org/api/3/action/group_package_show?id=countries-geodata")
                                                        .then(res => res.json())
                                                        .then(
                                                        (res) => {
                                                            for (var i in res.result) {
                                                                var tagFilteredData = res.result[i].tags.filter((tag) => tag.name===map.focusedCountry);
                                                                if (tagFilteredData.length > 0) {
                                                                    map.setHighResolutionGeodata(res.result[0].resources);
                                                                    map.setAvailableResolutions( map.highResolutionGeodata.length );
                                                                    map.setResolution( 0 );
                                                                    map.setSearchedCountry( map.focusedCountry );
                                                                    map.toggleGeofencingMode( true );
                                                                    map.toggleJMPDialogState( false );
                                                                    //map.setHighResolutionDatasetId( null );
                                                                    map.isMapLoading( false );
                                                                }
                                                            }
                                                        },
                                                        (error) => {
                                                            console.log(error);
                                                        });
                                                        

                                                        // shp(base).then(function ( data ) {
                                                        //     map.setAvailableResolutions( data.length );
                                                        //     // map.setResolutionsData([ map.mapRef.current.leafletElement._layers[countryId].feature, data]);
                                                        //     map.setResolutionsData( data );
                                                        //     map.setResolution( 0 );
                                                        //     map.setSearchedCountry( map.focusedCountry );
                                                        //     map.toggleGeofencingMode( true );
                                                        //     map.toggleJMPDialogState(false);
                                                        //     map.setHighResolutionDatasetId( null );
                                                        //     map.isMapLoading( false );
                                                        // });
                                                    }}
                                                >
                                                    Get high resolution data
                                        </Button>
                                                <Button
                                                    variant="contained"
                                                    color="secondary"
                                                    startIcon={<AddToPhotosTwoToneIcon />}
                                                    disabled={map.worldData.filter((country) => country.gid === map.focusedCountry && country["notes1"] !== '').length}
                                                    // {map.mapLoading || map.resolutionDatasets.length == 0}
                                                    onClick={() => {
                                                        
                                                        map.setSourceDataset('https://data.waterpathogens.org/dataset/'+map.worldInputId);
                                                        map.toggleGeofencingMode(true);
                                                        map.toggleJMPDialogState(false);
                                                        map.setMultiCountryMode(true);
                                                        map.addArea(map.focusedCountry);
                                                        var worldDataMatch = map.worldData.filter((country) => country.gid === map.focusedCountry);
                                                        map.addAreaToDictionary(map.focusedCountry, worldDataMatch[0].subarea);
                                                        map.setFocusedCountry('');
                                                    }}
                                                >
                                                    Add to selection
                                        </Button>
                                            </ButtonGroup>
                                        </div>
                                    }
                                </AccordionDetails>
                            </Accordion>
                            <Accordion square disabled={!map.geofencingMode} expanded={map.geofencingMode}>
                                <AccordionSummary aria-controls="panel1d-content" id="panel1d-header">
                                    <Typography className={classes.heading}><span>Step 2:</span> Select areas</Typography>
                                </AccordionSummary>
                                <AccordionDetails>
                                    {(!map.multiCountryMode) &&
                                        <div>
                                            <Container>
                                                <Grid container style={{ paddingBottom: "20px" }}>

                                                    <Grid item md={6} lg={6} xs={12}>
                                                        <Typography variant='subtitle1' style={{ marginBottom: '10px', color: 'rgb(53, 53, 53)' }}>Available data:</Typography>
                                                        
                                                        <FormControl component="fieldset">
                                                            <RadioGroup aria-label="resolution" name="resolution" defaultValue={map.worldInputId}>
                                                                {/* {resolutionLevels.map(level => { return level }) } */}
                                                                {map.resolutionDatasets.map((dataset, key) => {
                                                                    var datasetResolution = parseInt(dataset.tags.filter((tag) => tag.name.includes('ADM'))[0].name.replace('ADM', ''));

                                                                    return <FormControlLabel
                                                                        key={key}
                                                                        value={dataset.id}
                                                                        control={
                                                                            <Radio
                                                                                checked={map.highResolutionDatasetId === dataset.id}
                                                                                onChange={(e) => {
                                                                                    if (e.target.checked) {
                                                                                        map.setResolution(datasetResolution);
                                                                                    }
                                                                                }}
                                                                            />
                                                                        }
                                                                        label={dataset.title} />
                                                                })}
                                                            </RadioGroup>

                                                        </FormControl>
                                                    </Grid>

                                                </Grid>
                                            </Container>
                                        </div>
                                    }
                                    {(map.selectedAreasList.length == 0 && map.resolution > 0 && map.highResolutionDatasetId == '') ?
                                        <Box className={classes.enhancedBox} style={{ height: '200px', textAlign: 'center' }}>
                                            <p><MouseTwoToneIcon style={{ marginTop: '50px', color: '#42919e' }} fontSize='large' /></p>
                                            <Typography variant='subtitle1'>Click on one or more areas to start your analysis.</Typography>
                                        </Box>
                                        :
                                        <div>

                                            <Card>
                                                <CardHeader style={{ paddingBottom: '0px' }} 
                                                    title={
                                                        <div>
                                                            <h2 style={{marginBottom: '0px', paddingBottom: '0px'}}>
                                                                {map.selectedAreasList.length} {(map.resolution === 0) ? <span>countries</span> : <span>areas</span>} selected
                                                            </h2>
                                                            {(map.highResolutionDatasetId === 0) &&
                                                                <h4 style={{fontWeight:100, marginTop: '0px'}}>
                                                                    Click on more {(map.resolution === 0) ? <span>countries</span> : <span>areas</span>} to expand your selection.
                                                                </h4>
                                                            }
                                                        </div>
                                                    }>

                                                </CardHeader>
                                                <CardContent>
                                                    <Grid container spacing={4}>
                                                        <Grid item xs={12} md={6} lg={6}>
                                                            <p>
                                                                {map.selectedAreasList.map((area, key) => {
                                                                    var records = map.selectedAreasDictionary.filter(r => r.id === area);
                                                                    return <span key={key}>
                                                                        {(records.length > 0 ?
                                                                            records[0]["area"] : area
                                                                        )}
                                                                        {(key < map.selectedAreasList.length - 1) ? ', ' : '.'}
                                                                    </span>
                                                                })
                                                                }
                                                            </p>
                                                        </Grid>
                                                        <Grid item xs={12} md={6} lg={6} style={{ flexDirection: 'column', alignContent: 'center', alignSelf: 'center' }}>
                                                        <LightTooltip interactive={true} placement='left' title={<p>Read more about <a href="https://www.waterpathogens.org/book/summary-of-excreted-and-waterborne-viruses" target="_blank">Viruses</a> and <a href="https://www.waterpathogens.org/book/section-iii-protists" target="_blank">Protozoa</a> on GWPP.</p>}>

                                                            <FormControl variant='outlined' required className={classes.formControl}>
                                                                <InputLabel
                                                                    id="pathogen">Select Pathogen Group
                                                                </InputLabel>

                                                                <Select
                                                                    style={{ width: '200px' }}
                                                                    labelId="pathogen"
                                                                    onChange={(e) => { map.setPathogen(e.target.value); }}
                                                                    value={map.pathogen}
                                                                    input={
                                                                        <OutlinedInput
                                                                            labelWidth={200}
                                                                            name="pathogen"
                                                                            id="outlined-parameter-simple"
                                                                        />
                                                                    }
                                                                >
                                                                    <MenuItem value={'Virus'}>Viruses</MenuItem>
                                                                    <MenuItem value={'Protozoa'}>Protozoa</MenuItem>
                                                                </Select>
                                                                
                                                            </FormControl>
                                                            </LightTooltip>

                                                            <Button 
                                                                disabled={(map.selectedAreasList.length == 0 && map.resolution == 0) || map.pathogen == ''} 
                                                                style={{ marginTop: '10px' }} 
                                                                variant='contained' 
                                                                size='large' 
                                                                color='secondary' 
                                                                startIcon={<PlayCircleFilledTwoToneIcon />} 
                                                                onClick={() => { 
                                                                    map.flyToPolygons();
                                                                    map.toggleGeofencingMode(false); 
                                                                    map.toggleScenarioMode(); 
                                                                    map.addScenario(null); 
                                                                    map.toggleInfoMapDialogState(true);
                                                                }}>
                                                                    Start
                                                            </Button>
                                                        </Grid>
                                                    </Grid>
                                                    {/* <h4>Serviced population: XXXXXX</h4> */}

                                                </CardContent>
                                                {/* <CardActions style={{justifyContent:'flex-end'}}>
                                                    

                                                    </CardActions> */}
                                            </Card>
                                        </div>
                                    }
                                </AccordionDetails>
                            </Accordion>

                            <Accordion square disabled={!map.scenarioMode} expanded={map.scenarioMode}>
                                <AccordionSummary aria-controls="panel3d-content" id="panel3d-header">
                                    <Typography className={classes.heading}><span>Step 3:</span> Run Scenarios</Typography>
                                </AccordionSummary>
                                <AccordionDetails>
                                </AccordionDetails>
                            </Accordion>
                            {(!map.geofencingMode) &&
                            <Box className={classes.enhancedBox} style={{ fontSize: '12px', margin:'auto', marginTop: '50px', textAlign:'center', maxWidth:'300px' }}>
                                    <p>Developed as part of the GWPP-K2P Project.</p>
                                    <p>Copyright &copy; 2020 | <a style={{textDecoration:'none', color: '#343434'}} href='#' onClick={()=>map.setDisclaimerState(true)}>Disclaimer</a></p>
                            </Box>
                            }
                        </div>
                        :
                        <div>
                            {(!map.summaryMode) ?
                                <SavedScenarios context={map} />
                                : <SummaryTab context={map}/>
                            
                            }
                        </div>
                    }
                </div>
            </Grid>
        </ThemeProvider>
    )
});

export default MapSideBar;
