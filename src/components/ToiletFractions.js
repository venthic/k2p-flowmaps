
import * as React from "react";
import { observer } from "mobx-react-lite";
//import MultiSlider from "multi-slider";
import { Grid, Select, FormControl, RadioGroup, ButtonGroup, FormControlLabel, Radio, Button, Tooltip, Box, InputLabel, Input, MenuItem, Checkbox, ListItemText, Card, CardHeader, CardContent, ListSubheader } from '@material-ui/core';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import SwapHorizRoundedIcon from '@material-ui/icons/SwapHorizRounded';
import PlayForWorkRoundedIcon from '@material-ui/icons/PlayForWorkRounded';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import dataFunctions from "../config/functions.json";
import toiletCategories from "../config/toiletColumns.json";
import AreasDialog from "./dialogs/AreasDialog";
import ToiletChart from "./charts/ToiletChart";
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined';
import ReactSlider from 'react-slider';
import styled from 'styled-components';
import SettingsRoundedIcon from '@material-ui/icons/SettingsRounded';


const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 6.5 + ITEM_PADDING_TOP,
      maxWidth: 400
    },
  },
};

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    display: 'flex',
    height: 224,
  },
  tabs: {
    borderRight: `1px solid ${theme.palette.divider}`,
  },
  formControl: {
    marginTop: '10px'
  },
  enhancedBox: {
    padding: 20,
    marginTop: 10,
    marginBottom: 10,
    borderRadius: 2,
    backgroundColor: '#eee',
    boxShadow: '0 1px 15px rgba(0,0,0,.04), 0 1px 6px rgba(0,0,0,.04)',
    border: '1px solid #e7e7e7',
    position: 'relative'
  }
}));

// const StyledSlider = styled(ReactSlider)`
//     width: 100%;
//     height: 15px;
// `;

// const StyledThumb = styled.div`
//     height: 25px;
//     top: -4px;
//     line-height: 25px;
//     width: 25px;
//     text-align: center;
//     color: #fff;
//     border-radius: 50%;
//     cursor: grab;
// `;

// const Thumb = (props, state) => { 
//   const value = ( state.index != 0 ) ? (state.valueNow - state.value[state.index - 1] ): (state.valueNow); 
//   return <StyledThumb {...props}>{value}
//   </StyledThumb>};

// const StyledTrack = styled.div`
//     top: 0;
//     bottom: 0;
//     background: ${props => props.index === 4 ? 'rgb(64, 140, 191)' : props.index === 3 ? 'rgb(102, 191, 64)' : props.index === 2 ? 'rgb(191, 172, 64)' : props.index === 1 ? 'rgb(188,123,80)' : 'rgb(191, 64, 64)'};
//     border-radius: 999px;
// `;

// const Track = (props, state) => <StyledTrack {...props} index={state.index} />;

const LightTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: theme.palette.common.white,
    color: 'rgba(0, 0, 0, 0.87)',
    boxShadow: '0 1px 15px rgba(0,0,0,.04),0 1px 6px rgba(0,0,0,.04)',
    fontSize: 14,
    fontWeight: 500
  },
}))(Tooltip);

const ToiletFractions = observer(({ scenario }) => {

  const classes = useStyles();

  const isIndeterminate = (df) => {
    const records = (scenario.selectedAreasDialogValues[scenario.activeTab].length === 0 || scenario.selectedAreasDialogValues[scenario.activeTab].length === scenario.records.length) ? scenario.records : scenario.records.filter( record => scenario.selectedAreasDialogValues[scenario.activeTab].includes(record.subarea)); 
    //isIndeterminate = true;
    
    if (df.valueChecked !== null) {
      var checkedRecords = records.filter( r => r[df.columns[0]] === 1 && r[df.columns[0]] === 1);
      return checkedRecords.length !== records.length && checkedRecords.length > 0;
    }
    // else {
    //   var checkedRecords = records.filter( r => {return Math.round(scenario.populationWeightedAvg(df.fromColumns, scenario.selectedContext, true, r)) === 0});
    //   return checkedRecords.length !== records.length;
    // }
    // return isIndeterminate
  }; 

  return <div style={{ width: "100%" }}>
    

    <Box className={classes.enhancedBox}>
      <LightTooltip interactive={true} placement='left' title='The five onsite sanitation categories represent thirteen toilet categories that are also in use by the Joint Monitoring Programme (JMP) of WHO and UNICEF. These comprise “No Facility” (open defecation, hanging toilets and other), “Dry Toilets” (container based, bucket latrine, pit without a slab, pit with a slab) , “Composting Toilets”, “Flush Toilets (onsite)” (flush to open, flush to unknown, flush to pit and flush to septic tank) and “Flush Toilets (sewered)”. Please have a look at the model explanation to understand how the individual onsite sanitation categories are dealt with in the model.'>
        
        <IconButton aria-label="info" className='infoPrompt'>
        <InfoOutlinedIcon/>
        </IconButton>

      </LightTooltip>
      Viewing aggregates for: {(
        scenario.selectedAreasDialogValues[scenario.activeTab].length === 0 ||
        scenario.selectedAreasDialogValues[scenario.activeTab].length === scenario.records.length) ?
        <b>all areas</b> :
        <b>{scenario.selectedAreasDialogValues[scenario.activeTab].length} areas</b>} (<a href="#" onClick={() => { scenario.setSelectedAreasDialogTab(scenario.activeTab) }}>change</a>)
        
      <ToiletChart context={scenario.selectedContext} scenario={scenario}/>
      <FormControl style={{display: 'block'}} component="fieldset">
        <RadioGroup aria-label="context" name="context" className='contextRadio' value={scenario.selectedContext} onChange={(e)=>scenario.setSelectedContext(e.target.value)}>
          <FormControlLabel className='contextRadioOption' value="both" control={<Radio />} label="General Population" />
          <FormControlLabel className='contextRadioOption' value="urb" control={<Radio />} label="Urban only" />
          <FormControlLabel className='contextRadioOption' value="rur" control={<Radio />} label="Rural only" />
        </RadioGroup>
      </FormControl>
      
    </Box>

    <Grid container spacing={2} className="toilets">
      <Grid item md={12} lg={12} xs={12}>
      <Card>
        <CardContent>
          <Grid container spacing={3}>
          <Grid item md={6} lg={6} sm={6} xs={12} style={{textAlign:'right'}}>
          <LightTooltip interactive={false} placement='left' title='Select the onsite sanitation category ‘from’ which you want to move part or all of the percentage to another category, selected in the ‘to’ box.'>

          <FormControl variant='outlined' className={classes.formControl}>
            <InputLabel id="fromCategories">From</InputLabel>
            <Select
              style={{ width: '180px', textAlign:'left' }}
              labelId="fromCategories"
              defaultValue={scenario.fromToilet}
              onChange={(e)=>{scenario.setFromToilet(e.target.value);}}
              input={<OutlinedInput
                  labelWidth={50}
                  name="fromCategories"
                  id="outlined-categories-1"
                />}
              renderValue={(selected) => {return selected > -1 ? toiletCategories.filter(cat => cat.id === selected)[0].title : 'None'}}
              MenuProps={MenuProps}
            >
              
              {toiletCategories.map((category) => (
                <MenuItem key={category.title} value={category.id} style={{whiteSpace:'normal'}}>
                  <ListItemText primary={category.title} />
                </MenuItem>
              ))}
            </Select>
          </FormControl>
          </LightTooltip>
          <LightTooltip interactive={false} placement='left' title='Select the onsite sanitation category ‘from’ which you want to move part or all of the percentage to another category, selected in the ‘to’ box.'>

          <FormControl variant='outlined' className={classes.formControl}>
            <InputLabel id="toCategories">To</InputLabel>
            <Select
              style={{ width: '180px', textAlign:'left' }}
              labelId="toCategories"
              defaultValue={scenario.toToilet}
              onChange={(e)=>{scenario.setToToilet(e.target.value);}}
              input={<OutlinedInput
                name="toCategories"
                labelWidth={50}
                id="outlined-categories-2"
              />}
              renderValue={(selected) => {return selected > -1 ? toiletCategories.filter(cat => cat.id === selected)[0].title : 'None'}}
              MenuProps={MenuProps}
            >
              {toiletCategories.map((category) => (
                <MenuItem key={category.title} value={category.id} style={{whiteSpace:'normal'}}>
                  <ListItemText primary={category.title} />
                </MenuItem>
              ))}
            </Select>
          </FormControl>
          </LightTooltip>

          </Grid>
          <Grid item md={6} lg={6} xs={6} alignItems='center' alignContent='center' style={{display: 'inline-flex', textAlign:'center'}}>
          <ButtonGroup orientation={'vertical'}>
              <Button aria-label="move incrementally" 
                size='small'
                startIcon={<SwapHorizRoundedIcon />}
                disabled={ (scenario.toToilet !== -1 && scenario.fromToilet !== -1) ? 
                  (scenario.toToilet !== scenario.fromToilet) ?
                 (scenario.populationWeightedAvg(toiletCategories[scenario.toToilet].columns, scenario.selectedContext, true, [])/100 >= 1 ||
                 scenario.populationWeightedAvg(toiletCategories[scenario.fromToilet].columns, scenario.selectedContext, true, [])/100 < 0.01): true : true }
                onClick={() => { scenario.movePoints(scenario.fromToilet, scenario.toToilet, true, scenario.selectedContext) }}>
                  Move incrementally
              </Button>
              <Button aria-label="move entirely" 
                size='small'
                startIcon={<PlayForWorkRoundedIcon />}
                disabled={(scenario.toToilet !== -1 && scenario.fromToilet !== -1) ? 
                  (scenario.toToilet !== scenario.fromToilet) ?
                  (scenario.populationWeightedAvg(toiletCategories[scenario.fromToilet].columns, scenario.selectedContext, true, [])/100 < 0.01) : true : true 
                } 
                onClick={() => { 
                  if (scenario.selectedContext === 'both') {
                    scenario.moveAllPoints(toiletCategories[scenario.fromToilet].columns, toiletCategories[scenario.toToilet].columns, 'urb', true);
                    scenario.moveAllPoints(toiletCategories[scenario.fromToilet].columns, toiletCategories[scenario.toToilet].columns, 'rur', true);
                  } 
                  else {
                    scenario.moveAllPoints(toiletCategories[scenario.fromToilet].columns, toiletCategories[scenario.toToilet].columns, scenario.selectedContext, true);
                  }
                }}>
                Move entirely
              </Button>
          </ButtonGroup>
          
          </Grid>
          <LightTooltip interactive={false} placement='left' title='Additional improvement options can reduce the pathogens reaching the environment. You can select the additional management improvements by ticking the box before the option.'>

          <FormControl style={{minWidth: '290px', margin:'auto'}} className={classes.formControl}>
              <InputLabel id="practices">Additional management improvements</InputLabel>
              <Select
                labelId="practices"
                id="practices"
                multiple
                IconComponent={SettingsRoundedIcon}
                defaultValue={scenario.dataFunctions}
                onChange={(e, c)=>{scenario.setDataFunctions(e.target.value);scenario.applyFunction(dataFunctions.filter((func) => func.id ===c.props.value)[0], scenario.dataFunctions.indexOf(c.props.value) > -1);}}
                input={<Input />}
                renderValue={()=> {return scenario.dataFunctions.length+' practice(s) applied.'}}
                //selected.map(f=>dataFunctions.filter(fun => fun.id === f)[0].label).join(', ')
                MenuProps={MenuProps}
              >
                <ListSubheader disableSticky={true}>Onsite flush / pour flush toilets</ListSubheader>
                {dataFunctions.filter((dataFunction) => dataFunction.type === 'flush-toilets').map((dataFunction) => (
                  <MenuItem key={dataFunction.label} value={dataFunction.id} style={{whiteSpace:'normal'}}>
                    <Checkbox indeterminate={isIndeterminate(dataFunction)} checked={scenario.dataFunctions.indexOf(dataFunction.id) > -1} />
                    <ListItemText primary={dataFunction.label} />
                  </MenuItem>
                ))}
                <ListSubheader disableSticky={true}>Onsite dry toilets</ListSubheader>
                {dataFunctions.filter((dataFunction) => dataFunction.type === 'dry-toilets').map((dataFunction) => (
                  <MenuItem key={dataFunction.label} value={dataFunction.id} style={{whiteSpace:'normal'}}>
                    <Checkbox indeterminate={isIndeterminate(dataFunction)} checked={scenario.dataFunctions.indexOf(dataFunction.id) > -1} />
                    <ListItemText primary={dataFunction.label} />
                  </MenuItem>
                ))}
                <ListSubheader disableSticky={true}>Onsite toilets (dry or flush / pour flush)</ListSubheader>
                {dataFunctions.filter((dataFunction) => dataFunction.type === 'all-toilets').map((dataFunction) => (
                  <MenuItem key={dataFunction.label} value={dataFunction.id} style={{whiteSpace:'normal'}}>
                    <Checkbox indeterminate={isIndeterminate(dataFunction)} checked={scenario.dataFunctions.indexOf(dataFunction.id) > -1} />
                    <ListItemText primary={dataFunction.label} />
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
            </LightTooltip>
          </Grid>
            <br/>
      
        </CardContent>
      </Card>
      </Grid>
      {/* <Grid item md={6} lg={6} xs={12}>
        <Card>
          <CardHeader className="toiletCardHeader" titleTypographyProps={{ variant: 'h3' }} title="Onsite systems" />
          <CardContent>
            <StopRoundedIcon style={{ color: 'rgb(188,123,80)' }} /> Dry toilets {
            {(
              scenario.selectedAreasDialogValues[scenario.activeTab].length != 0 &&
              scenario.selectedAreasDialogValues[scenario.activeTab].length != scenario.records.length) &&
              <span class="difference">{'('+Math.round(scenario.populationWeightedAvg(toiletCategories[1].columns, "both", false, [])) + '%)'}</span>
            }
            <ButtonGroup style={{height: '25px', border: '1px solid #ccc'}}>
            <Tooltip title={"Decrease"}>
              <IconButton aria-label="decrease" 
                size='small'
                disabled={Math.round(scenario.populationWeightedAvg(toiletCategories[1].columns, scenario.selectedContext, false, []))/100 >= 1} 
                onClick={() => { scenario.decreaseToiletCategory(1, false, scenario.selectedContext) }}>
                <RemoveTwoToneIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title={"Increase"}>
              <IconButton aria-label="increase" 
                size='small'
                disabled={Math.round(scenario.populationWeightedAvg(toiletCategories[1].columns, scenario.selectedContext, false, []))/100 >= 1} 
                onClick={() => { scenario.increaseToiletCategory(1, false, scenario.selectedContext) }}>
                <AddTwoToneIcon />
              </IconButton>
            </Tooltip>
            
            </ButtonGroup>
            <br />

            
            <StopRoundedIcon style={{ color: 'rgb(191, 172, 64)' }} /> Composting toilets 
            {(
              scenario.selectedAreasDialogValues[scenario.activeTab].length != 0 &&
              scenario.selectedAreasDialogValues[scenario.activeTab].length != scenario.records.length) &&
              <span class="difference">{'('+Math.round(scenario.populationWeightedAvg(toiletCategories[2].columns, "both", false, [])) + '%)'}</span>
            } <br />
            <StopRoundedIcon style={{ color: 'rgb(102, 191, 64)' }} /> Flush toilets 
            {(
              scenario.selectedAreasDialogValues[scenario.activeTab].length != 0 &&
              scenario.selectedAreasDialogValues[scenario.activeTab].length != scenario.records.length) &&
              <span class="difference">{'('+Math.round(scenario.populationWeightedAvg(toiletCategories[3].columns, "both", false, [])) + '%)'}</span>
            } <br />
            
              <br/>
            
          </CardContent>
        </Card>
      </Grid>
      <Grid item md={6} lg={6} xs={12}>
        <Card>
          <CardHeader className="toiletCardHeader" titleTypographyProps={{ variant: 'h3' }} title="Sewered systems" />
          <CardContent>

            <StopRoundedIcon style={{ color: 'rgb(64, 140, 191)' }} /> Flush toilets 
            {(
              scenario.selectedAreasDialogValues[scenario.activeTab].length != 0 &&
              scenario.selectedAreasDialogValues[scenario.activeTab].length != scenario.records.length) &&
              <span class="difference">{'('+Math.round(scenario.populationWeightedAvg(toiletCategories[4].columns, "both", false, [])) + '%)'}</span>
            } <br />
            
          </CardContent>
        </Card>


        <Card style={{ marginTop: '5px' }}>
          <CardHeader className="toiletCardHeader" titleTypographyProps={{ variant: 'h3' }} title="No facility" />
          <CardContent>
            <StopRoundedIcon style={{ color: 'rgb(191, 64, 64)' }} /> No facility 
            {(
              scenario.selectedAreasDialogValues[scenario.activeTab].length != 0 &&
              scenario.selectedAreasDialogValues[scenario.activeTab].length != scenario.records.length) &&
              <span class="difference">{'('+Math.round(scenario.populationWeightedAvg(toiletCategories[0].columns, "both", false, [])) + '%)'}</span>
            } <br />
          </CardContent>
        </Card>
      </Grid> */}

    </Grid>
    { (scenario.records ) &&
    <AreasDialog scenario={scenario} />
    }
  </div >
});

export default ToiletFractions;

