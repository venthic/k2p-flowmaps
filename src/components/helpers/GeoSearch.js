import { Component } from "react";
import L from "leaflet";
import * as ELG from "esri-leaflet-geocoder";
import { withLeaflet } from "react-leaflet";
import codes from "../../config/codes.json"

class GeoSearch extends Component {

  componentWillUnmount() {
    const map = this.props.leaflet.map;
    map._controlContainer.childNodes[1].childNodes[2].innerHTML= '';
  }

  componentDidMount() {
    const model = this.props.model;
    const map = this.props.leaflet.map;
    const search = new ELG.Geosearch({zoomToResult: true, position: 'topright'});
    const searchControl = search.addTo(map);
    const results = new L.LayerGroup().addTo(map);
    const countriesRef = this.props.countriesRef;
    
    searchControl.on("results", function(data) {
      results.clearLayers();
      model.clearSelectedAreas();
      for (let i = data.results.length - 1; i >= 0; i--) {
        
        if (data.results[i].properties.Type == 'City') {
            //dosth
        }
        else {
          if (data.results[i].properties.Type == 'Country') {
            model.setFocusedCountry(data.results[i].properties.Country);
          }
          else {
            if (data.results[i].properties.Type == 'Zone' || data.results[i].properties.Type == 'Continent') {
              
              var countries = codes.filter( (country) =>  
                ( country["sub-region"] == data.results[i].properties.LongLabel  || 
                country["intermediate-region"] == data.results[i].properties.LongLabel|| 
                country["region"] == data.results[i].properties.LongLabel)
              )
              if (countries.length > 0) {
                if (!model.geofencingMode) {
                  model.toggleGeofencingMode(true);
                }
                
                model.setMultiCountryMode(true);
                var layers = countriesRef.current.leafletElement._layers;
                for (var j in countries) {
                  if (model.worldData.filter((country) => country.gid === countries[j]["alpha-3"] && country["notes1"] === '').length > 0) {
                    model.addArea(countries[j]["alpha-3"]);
                    model.addAreaToDictionary(countries[j]["alpha-3"], countries[j]["name"]);
                    Object.keys(layers).forEach(key => {
                      if (layers[key].feature && layers[key].feature.id === countries[j]["alpha-3"]) {
                        layers[key].setStyle({
                          'fillOpacity': 0.85
                        })
                      }
                      else {
                        countriesRef.current.leafletElement.resetStyle(layers[key]);
                      }
                    });
                  }
                } 
              }
              
            }
            else {

              model.setFocusedCountry(data.results[i].properties.Country);
            }
          }
        }
      }
    });
  }

  render() {
    return null;
  }
}

export default withLeaflet(GeoSearch);

