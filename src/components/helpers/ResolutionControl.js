import { Component } from "react";
import L from "leaflet";
import { Map, Marker, Popup, GeoJSON, TileLayer } from 'react-leaflet';
import { withLeaflet } from "react-leaflet";

class ResolutionControl extends MapControl {
  componentDidMount() {
    const map = this.props.leaflet.map;
    const model = this.props.model;
    
    searchControl.on("results", function(data) {
      results.clearLayers();
      model.clearSelectedAreas();
      for (let i = data.results.length - 1; i >= 0; i--) {
        console.log(data.results[i].properties);
        
        if (data.results[i].properties.Type == 'City') {
            //dosth
        }
        else {
          if (data.results[i].properties.Type == 'Country') {
            model.setFocusedCountry(data.results[i].properties.Country);
            // for ( var i in countriesRef.current.leafletElement._layers) {
            //   countriesRef.current.leafletElement._layers[i].style = {
            //     color: '#003347',
            //     weight: 1,
            //     fillColor: '#003347',
            //     fillOpacity: (model.focusedCountry == countriesRef.current.leafletElement._layers[i].id) ? 0.85:0.1
            //   };
            // }
          }
          else {
            if (data.results[i].properties.Type == 'Zone' || data.results[i].properties.Type == 'Continent') {
              
              var countries = codes.filter( (country) =>  
                ( country["sub-region"] == data.results[i].properties.LongLabel  || 
                country["intermediate-region"] == data.results[i].properties.LongLabel|| 
                country["region"] == data.results[i].properties.LongLabel)
              )
              if (countries.length > 0) {
                if (!model.geofencingMode) {
                  model.toggleGeofencingMode();
                }
                var layers = countriesRef.current.leafletElement._layers;
                for (var i in countries) {
                  model.addArea(countries[i].name);
                  Object.keys(layers).forEach(key => {
                    if (layers[key].feature && layers[key].feature.properties.name == countries[i].name) {
                      layers[key].setStyle({
                        'fillOpacity': 0.85
                      })
                    }
                    else {
                      countriesRef.current.leafletElement.resetStyle(layers[key]);
                    }
                  });
                  // for (var layer in Object.keys(map._layers)) {
                  //   console.log(layer);
                  //   console.log(map._layers[layer]);
                  //   if ( map._layers[layer].hasOwnProperty('feature')) {
                  //     if (map._layers[layer].feature.properties.name == countries[i].name) {
                  //       console.log(map._layers[layer]);
                  //       map._layers[layer].setStyle({
                  //         'fillOpacity': 0.85
                  //       })
                  //     }
                  //   } 
                  // }
                } 
              }
              
            }
          }
        }
        
        //results.addLayer(L.marker(data.results[i].latlng));
        
      }
      
    });
  }

  render() {
    return null;
  }
}

//export default Search;
export default withLeaflet(ResolutionControl);

