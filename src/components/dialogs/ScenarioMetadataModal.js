import { observer } from "mobx-react-lite";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import TextField from "@material-ui/core/TextField/TextField";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Button from "@material-ui/core/Button/Button";
import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Dialog from "@material-ui/core/Dialog/Dialog";
import Typography from "@material-ui/core/Typography/Typography";
import FormControl from "@material-ui/core/FormControl/FormControl";
import CheckIcon from '@material-ui/icons/Check';
import ToggleButton from '@material-ui/lab/ToggleButton';
import Container from '@material-ui/core/Container';


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        width: '100%',
        backgroundColor: theme.palette.background.paper,
    },
    formControl: {
        marginBottom: '20px',
        display: 'block'
    }
}));

const ScenarioMetadataModal = observer(({ map }) => {
    
    const classes = useStyles();
    // const [title, setTitle] = React.useState(map.scenarios[map.activeScenario].title);
    // const [description, setDescription] = React.useState(map.scenario[map.activeScenario].description);

    return (
        <div>
        <Dialog
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            open={map.metadataModalState}
            onClose={(e) => {
                map.setMetadataModalState(false)
            }}
        >
            <DialogTitle>
                <center>Scenario Metadata</center>
            </DialogTitle>
            
                <DialogContent>
                <Container>
            <FormControl variant='outlined' required className={classes.formControl}>
                <TextField 
                    id="outlined-basic" 
                    label={'Title'} 
                    style={{width:'300px'}}
                    variant="outlined"
                    type='text' 
                    value={map.scenarios[map.activeScenario].title}
                    onChange={(event)=>map.setActiveScenarioTitle(event.target.value)}
                />

            </FormControl>
            
            <FormControl variant='outlined' required={false} className={classes.formControl}>
            <TextField 
                    id="outlined-basic" 
                    label={'Description'} 
                    variant="outlined"
                    type='text'  
                    style={{width:'400px'}}
                    multiline
                    rows={4}
                    value={map.scenarios[map.activeScenario].description}
                    onChange= {(event)=>map.setActiveScenarioDescription(event.target.value)}
                />
                </FormControl>
                {(map.baselineScenario === -1) && <Typography variant='p' style={{fontSize:'12px', color:'rgb(189, 20, 20)', display: 'block', marginBottom: '20px'}}>You have not set a Baseline scenario yet. Check the box below to set this as your Baseline.</Typography>}
            <ToggleButton
                value="check"
                selected={map.activeScenario === map.baselineScenario}
                onChange={()=>map.setBaselineScenario(map.activeScenario)}
                disabled={map.activeScenario === map.baselineScenario}
                >
            <CheckIcon />&nbsp;Baseline
            </ToggleButton>
                </Container>
                </DialogContent>
            
            <DialogActions>
                
                    <Button onClick={() => {
                    // map.updateActiveScenarioMetadata();
                    map.setMetadataModalState(false);
                }} color="primary" autoFocus>
                    Close
                    </Button>
                    <Button onClick={() => {
                    map.deleteScenario();
                    map.setMetadataModalState(false);
                }} color="danger" disabled={map.scenarios.length === 1} autoFocus>
                    Delete
                    </Button>
                    {(map.baselineScenario === map.activeScenario || map.baselineScenario === -1) && 
                        <Button
                            disabled={(map.baselineScenario === -1) || (map.summaryMode)}
                            onClick={() => {
                            map.run();
                            map.setMetadataModalState(false);
                        }} color="primary">
                            Run
                        </Button>
                    }
            </DialogActions>
        </Dialog>
        
        </div>
    )
});

export default ScenarioMetadataModal;
