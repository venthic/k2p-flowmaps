import { observer } from "mobx-react-lite";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Button from "@material-ui/core/Button/Button";
import Tooltip from "@material-ui/core/Tooltip/Tooltip";
import React, {useState} from "react";
import Dialog from "@material-ui/core/Dialog/Dialog";
import Card from "@material-ui/core/Card";
import Grid from "@material-ui/core/Grid";
import { withStyles } from "@material-ui/core/styles";

  const LightTooltip = withStyles((theme) => ({
    tooltip: {
      backgroundColor: theme.palette.common.white,
      color: 'rgba(0, 0, 0, 0.87)',
      boxShadow: '0 1px 15px rgba(0,0,0,.04),0 1px 6px rgba(0,0,0,.04)',
      fontSize: 14,
      fontWeight: 500
    },
  }))(Tooltip);

const DisclaimerModal = observer(({context}) => {
    const map = context;

    return (
        <Dialog
            maxWidth='md'
            open={map.disclaimerState}
            onClose={(e) => {
                map.setDisclaimerState(false);
            }}
        >
            <DialogTitle>
            {(map.scenarioMode) ? <center>Data explanation</center>:<center>Disclaimer</center>}
            </DialogTitle>     
            <DialogContent>
                <Grid container spacing={2}>
                    <Grid item xs={12} sm={map.scenarioMode ? 6 : 12}>
                        {(map.scenarioMode)  &&
                        <div>
                            <h3>About the results</h3>
                            <p style={{textAlign:'justify'}}>
                            The map shows the log-transformed emission of the selected pathogen to the surface water. The results are gridded, which means that the results are provided for gridboxes. The size of these gridboxes depends on the resolution of the input data. For national data, the size is 0.5 x 0.5 degree latitude x longitude, which is roughly 50 x 50 km at the equator. For the high-resolution input data, the output resolution is 0.008333 x 0.008333 degree latitude x longitude, which is roughly 1 x 1 km. The results that you see are the emissions for a year. That year depends on the year for which the input data were provided. For the default data this year is a recent year. The actual emissions in the maps are not so relevant. It is much more interesting to look at differences in space and between scenarios. When comparing the gridded emissions in space, you will be able to find areas with high emissions (hotspot areas) and areas with lower emissions. The hotspot areas likely require more attention.
                            
                        </p>
                        
                        </div>
                         }
                        
                        {/* <Card><p>Okaali, D. A., & Hofstra, N. (2018). Present and future human emissions of Rotavirus and Escherichia coli to Uganda’s surface waters. Journal of environmental quality.<br/><a target="_blank" href="http://dx.doi.org/10.2134/jeq2017.12.0497">doi:10.2134/jeq2017.12.0497</a>.</p></Card>
                        <p>To access the publication, please contact us <a href="mailto:nynke.hofstra@wur.nl">here</a>.</p> */}
                    </Grid>
                    <Grid item xs={12} sm={map.scenarioMode ? 6 : 12}>
                        <h3>Disclaimer</h3>
                        <p>Please note that the provided maps are the result of model simulations. Although we attempt to provide pathogen emissions as close as possible to real world values and validation of model results is high on our agenda, the main aim of the modelling exercise is to analyse “hotspot” areas with high emissions and changes in emissions for different scenarios.</p>
                        <h3>Assumptions</h3>
                        <p>For more details about the model and its assumptions, see the <a target="_blank" href="https://www.waterpathogens.org/sites/default/files/The%20pathogen%20flow%20&%20mapping%20tool_DRAFT_clean.pdf">Model Explanation</a></p>
                        {map.scenarioMode && 
                            <p><a className="sourceDataset" href={map.sourceDataset} target="_blank">View source dataset</a></p>
                        }
                    </Grid>
                 </Grid>
            </DialogContent>
            <DialogActions>
                <Button onClick={() => {
                    map.setDisclaimerState(false);
                }} color="secondary">
                    Close
                    </Button>
                
            </DialogActions>
        </Dialog>
    )
});

export default DisclaimerModal;
