import React from 'react';
import { observer } from "mobx-react-lite";
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormLabel from '@material-ui/core/FormLabel';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import Checkbox from '@material-ui/core/Checkbox';

const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
}));

const AreasDialog = observer(({ scenario }) => {
  const classes = useStyles();


  return (
    <div>
      
      <Dialog disableBackdropClick disableEscapeKeyDown open={scenario.selectedAreasDialogTab === scenario.activeTab} onClose={()=>{scenario.setSelectedAreasDialogTab(-1)}}>
        <DialogContent>
          <form className={classes.container}>
            <FormControl className={classes.formControl}>
            <FormLabel component="legend">Limit areas:</FormLabel>
            <div style={{height: 200, overflowY: 'auto'}}>
              { scenario.records.map((record) => {
                    return <FormControlLabel style={{display:'block'}}
                    control={<Checkbox checked={scenario.selectedAreasDialogValues[scenario.activeTab].includes(record.subarea) || scenario.selectedAreasDialogValues[scenario.activeTab].length === 0} onChange={(e) => {scenario.handleSelectedAreasDialogValue(record.subarea, e.target.checked)}} name={record.subarea} />}
                    label={record.subarea}
                  />
                  }
                )}
            </div>
            <FormHelperText><a href="#" onClick={()=> scenario.setSelectedAreasDialogValuesAll()}>Select all</a></FormHelperText>
            </FormControl>
            
          </form>
        </DialogContent>
        <DialogActions>
          <Button onClick={()=>scenario.setSelectedAreasDialogTab(-1)} color="primary">
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
});

export default AreasDialog;