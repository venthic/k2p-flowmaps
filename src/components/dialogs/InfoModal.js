import { observer } from "mobx-react-lite";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Button from "@material-ui/core/Button/Button";
import Tooltip from "@material-ui/core/Tooltip/Tooltip";
import React, {useState} from "react";
import Dialog from "@material-ui/core/Dialog/Dialog";
import MenuBookTwoToneIcon from '@material-ui/icons/MenuBookTwoTone';
import EmojiObjectsTwoToneIcon from '@material-ui/icons/EmojiObjectsTwoTone';
import StorageTwoToneIcon from '@material-ui/icons/StorageTwoTone';
import Tutorial from "../Tutorial";
import BugReportTwoToneIcon from '@material-ui/icons/BugReportTwoTone';
import { withStyles } from "@material-ui/core/styles";

  const LightTooltip = withStyles((theme) => ({
    tooltip: {
      backgroundColor: theme.palette.common.white,
      color: 'rgba(0, 0, 0, 0.87)',
      boxShadow: '0 1px 15px rgba(0,0,0,.04),0 1px 6px rgba(0,0,0,.04)',
      fontSize: 14,
      fontWeight: 500
    },
  }))(Tooltip);

const InfoModal = observer(({context}) => {
    const map = context;
    const [showTutorial, setShowTutorial] = useState(false);
    const [showModel, setShowModel] = useState(false);
    const [showData, setShowData] = useState(false);
    const [showBugs, setShowBugs] = useState(false);

    return (
        <Dialog
            maxWidth='xl'
            fullWidth={showTutorial}
            open={map.infoState}
            onClose={(e) => {
                map.setInfoState(false);
                setShowTutorial(false);
                setShowModel(false);
                setShowData(false);
                setShowBugs(false);
            }}
        >
            <DialogTitle>
                <center>Welcome to the Pathogen Flow & Mapping Tool<LightTooltip title='The beta version of the Pathogen Flow & Mapping Tool has been tested with latest versions of Google Chrome (80+) and Mozilla Firefox (74+) browsers.'><sup style={{ textTransform: 'lowercase',
    fontSize: '10px',
    color: '#ea7b7b'}}>beta</sup></LightTooltip></center>
            </DialogTitle>     
            <DialogContent>
                 {(!showTutorial && !showModel && !showData && !showBugs) ?
                    <div className="helpOptions"> 
                    
                    <a className="manualButton" onClick={() => setShowTutorial(true)}>
                        <MenuBookTwoToneIcon/>
                        <span>Tool Manual</span>

                    </a>
                    <a className="manualButton" target="_blank" href="https://www.waterpathogens.org/sites/default/files/The%20pathogen%20flow%20&%20mapping%20tool_DRAFT_clean.pdf">
                        <EmojiObjectsTwoToneIcon/>
                        <span>Model explanation</span>

                    </a>

                    <a className="manualButton" style={{marginTop: '10px'}} onClick={() => {map.setInfoState(false); map.setDisclaimerState(true);}}>
                        <StorageTwoToneIcon/>
                        <span>Data explanation</span>

                    </a>
                    <a className="manualButton" style={{marginTop: '10px'}} onClick={() => {setShowBugs(true);}}>
                        <BugReportTwoToneIcon/>
                        <span>Known issues</span>

                    </a>
                    </div>:
                    <Tutorial tool={showTutorial} model={showModel} data={showData} bugs={showBugs}/>
                }
            </DialogContent>
            <DialogActions>
                <Button onClick={() => {
                    map.setInfoState(false);
                    setShowTutorial(false);
                    setShowModel(false);
                    setShowData(false);
                    setShowBugs(false);
                }} color="secondary">
                    Close
                    </Button>
                
            </DialogActions>
        </Dialog>
    )
});

export default InfoModal;
