import { observer } from "mobx-react-lite";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import TextField from "@material-ui/core/TextField/TextField";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Button from "@material-ui/core/Button/Button";
import React, {useState} from "react";
import Dialog from "@material-ui/core/Dialog/Dialog";
import MuiAlert from '@material-ui/lab/Alert';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

function Alert(props) {
    return <MuiAlert elevation={2} {...props} />;
} 

const GenerateReportModal = observer(({ context }) => {

    const [error, setError] = useState(false);
    const [errorName, setErrorName] = useState(false);
    const [errorAuthorName, setErrorAuthorName] = useState(false);
    const [errorOrganization, setErrorOrganization] = useState(false);
    const [errorEmail, setErrorEmail] = useState(false);
    const [errorCity, setErrorCity] = useState(false);
    const [errorCountry, setErrorCountry] = useState(false);

    const [errorMessage, setErrorMessage] = useState([]);

    const map = context;
    var report = map.reportData;

    const handleImage = (file) => {
        map.setReportValue('photo', file);
        var reader = new FileReader();
        // reader.readAsDataURL( file );
        var baseString = '';
        reader.onloadend = function () {
            baseString = reader.result;
            report.photo = baseString;
        };
        reader.readAsDataURL(file);
    }

    const validate = (report) => {
        
            let flag = true;
            setErrorMessage([]);

            if(!report.authorName || report.authorName === ""){
                setErrorAuthorName(true);
                setErrorMessage(errorMessage => [...errorMessage, 'The name of the author is required!']);
                flag = false;
            } else{
                setErrorAuthorName(false)
            } 

            if(!report.organization || report.organization === ""){
                setErrorOrganization(true);
                setErrorMessage(errorMessage => [...errorMessage, 'The name of the organization is required!']);
                flag = false;
            } else setErrorOrganization(false);

            if(!report.email || report.email === ""){
                setErrorEmail(true);
                setErrorMessage(errorMessage => [...errorMessage,'The email address is required!']);
                flag = false;
            } else { 
                if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(report.email)) {
                    setErrorEmail(false);
                }
                else {
                    setErrorEmail(true);
                    setErrorMessage(errorMessage => [...errorMessage,'The email address provided is not valid!']);
                    flag = false;
                }
                
            }

            // if(!report.phone || report.phone === ""){
            //     setErrorPhone(true);
            //     setErrorMessage(errorMessage => [...errorMessage, 'The phone number is required!']);
            //     flag = false;
            // } else setErrorPhone(false);

            // if(!report.street || report.street === ""){
            //     setErrorStreet(true);
            //     setErrorMessage(errorMessage => [...errorMessage,'The name of the street is required!']);
            //     flag = false;
            // } else setErrorStreet(false);

            if(!report.city || report.city === ""){
                setErrorCity(true);
                setErrorMessage(errorMessage => [...errorMessage,'The name of the city is required!']);
                flag = false;
            } else setErrorCity(false);

            // if(!report.state || report.state === ""){
            //     setErrorState(true);
            //     setErrorMessage(errorMessage => [...errorMessage, 'The name of the Administrative Division is required!']);
            //     flag = false;
            // } else setErrorState(false);

            if(!report.country || report.country === ""){
                setErrorCountry(true);
                setErrorMessage(errorMessage => [...errorMessage, 'The name of the country is required!']);
                flag = false;
            } else setErrorCountry(false);

            if(!report.name || report.name === ""){
                setErrorName(true);
                setErrorMessage(errorMessage => [...errorMessage,'The name of the report is required!']);
                flag = false;
            } else setErrorName(false);


            if (!flag) { setError(true) }
            return flag;
        
    }

    return (
        <div>
        <Dialog
            maxWidth='lg'
            fullWidth={true}
            id="screenshotDialog"
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            open={map.generateReportModalState}
            onClose={(e) => {
                map.setGenerateReportModalState(false);
            }}
        >
            <DialogTitle>
                <center>Generate Report</center>
            </DialogTitle>
            {!map.generatePDFButton  ?
                <DialogContent style={{height: '20px', overflowY:'hidden'}}>
                     {(map.screenshotObject === 'baselineLadder') ?
                        <div style={{width: '400px', marginLeft: 'auto', marginRight: 'auto'}}>
                            <center>Printing Sanitation Ladders...</center>
                            {/* <ToiletChart context={'both'} scenario={map.scenarios[map.baselineScenario].payload}/> */}
                        </div>: 
                         <center>Generating PDF File...</center>
                    }
                </DialogContent>
                :
                <DialogContent>
                    <Grid container spacing={1}>
                        {error &&
                            <Grid item xs={12} md={12}> 
                                <Alert severity="error">
                                    <ul>
                                    {
                                        errorMessage.map((message)=>{
                                            return <li>{message}</li> 
                                        })
                                    }
                                    </ul>
                                </Alert>
                                
                            </Grid>
                        }
                        </Grid>
                    <Grid container spacing={2} style={{marginBottom:'10px'}}>
                        <Grid item xs={12} sm={12}>
                            <Typography variant="h3" gutterBottom>
                                Author
                            </Typography>
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField error={errorAuthorName} defaultValue={report.authorName} required id="authorName" label="Name" fullWidth variant='outlined'
                                onChange={(e) => {map.setReportValue('authorName', e.target.value);}} 
                                onKeyPress={e => {if (e.key === 'Enter') e.preventDefault();}}  />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField error={errorOrganization} defaultValue={report.organization} required id="organization" label="Institution/Organization" fullWidth variant='outlined'
                                onChange={(e) => {map.setReportValue('organization', e.target.value);}} 
                                onKeyPress={e => {if (e.key === 'Enter') e.preventDefault();}}  />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField error={errorEmail} defaultValue={report.email} required id="email" label="Email" fullWidth type="email" variant='outlined'
                                onChange={(e) => {map.setReportValue('email', e.target.value);}} 
                                onKeyPress={e => {if (e.key === 'Enter') e.preventDefault();}}  />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField defaultValue={report.phone} id="phone" label="Phone Number" fullWidth type="tel" variant='outlined'
                                onChange={(e) => {map.setReportValue('phone', e.target.value);}} 
                                onKeyPress={e => {if (e.key === 'Enter') e.preventDefault();}}  />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField defaultValue={report.street} id="street" label="Address Line" fullWidth variant='outlined'
                                onChange={(e) => {map.setReportValue('street', e.target.value);}} 
                                onKeyPress={e => {if (e.key === 'Enter') e.preventDefault();}}  />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField  error={errorCity} defaultValue={report.city} required id="city" label="City" fullWidth variant='outlined'
                                onChange={(e) => {map.setReportValue('city', e.target.value);}} 
                                onKeyPress={e => {if (e.key === 'Enter') e.preventDefault();}}  />
                        </Grid>
                        <Grid item xs={12} md={6}>
                            <TextField defaultValue={report.state} id="state" label="Administrative Division (state, department, province, etc.)" fullWidth variant='outlined'
                                onChange={(e) => {map.setReportValue('state', e.target.value);}} 
                                onKeyPress={e => {if (e.key === 'Enter') e.preventDefault();}}  />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField error={errorCountry} defaultValue={report.country} required id="country" label="Country" fullWidth variant='outlined'
                                onChange={(e) => {map.setReportValue('country', e.target.value);}} 
                                onKeyPress={e => {if (e.key === 'Enter') e.preventDefault();}}  />
                        </Grid>
                        
                    </Grid>
                    <Grid container spacing={2} style={{marginBottom:'10px'}}>
                        <Grid item xs={12} sm={12}>
                            <Typography variant="h3" gutterBottom>
                                Report metadata
                            </Typography>
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField error={errorName} defaultValue={report.name} required id="name" label="Report Title" fullWidth variant='outlined'
                                onChange={(e) => {map.setReportValue('name', e.target.value);}} 
                                onKeyPress={e => {if (e.key === 'Enter') e.preventDefault();}}  />
                        </Grid>
                    </Grid>
                </DialogContent>
            }
            <DialogActions>
                <Button onClick={() => {map.setGenerateReportModalState(false);}} color="primary">Cancel</Button>
                {map.generatePDFButton ?
                    <Button onClick={() => { 
                        if (validate(report)) { 
                            map.prepareReport();
                        }
                    }
                } color="primary" autoFocus >Generate PDF</Button>:
                    <Button color="primary" autoFocus disabled >Generate PDF</Button>
                }
            </DialogActions>
        </Dialog>
        </div>

    )
});

export default GenerateReportModal;