import React,  { useState, useEffect } from "react";
import { observer } from "mobx-react-lite";
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';

const DataViewDrawer =  observer(({ scenario }) => {
    return (
        <div style={{height: 300, marginTop: 15}} className={"ag-theme-alpine"}>
            <AgGridReact
                gridOptions={scenario.payload.gridOptions}
                enableCellChangeFlash={true}
                onGridReady={(params) => scenario.payload.onGridReady}
            >
            </AgGridReact>
        </div>
    );
})

export default DataViewDrawer;