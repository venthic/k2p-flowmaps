import React from "react";
import { observer } from "mobx-react-lite";
import { withLeaflet } from "react-leaflet";
import DialogDefault from 'react-leaflet-dialog';

const Dialog = withLeaflet(DialogDefault);

const CountryLegend = observer(({ context }) => {

  const country = context.country;
  
  return <Dialog ref={context.countryDialog} id="countryDialog" anchor={[20,20]} size={[400,400]}>
  <div>
    {(country !== null) && 
      <h1>{country.subarea}</h1>
    }
    {context.geometry}
  </div>
</Dialog>
});

export default withLeaflet(CountryLegend);

