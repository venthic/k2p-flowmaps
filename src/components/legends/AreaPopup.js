
import React from "react";
import { observer } from "mobx-react-lite";
import { Popup } from 'react-leaflet';
import chroma from "chroma-js";
import { PieChart } from 'react-minimal-pie-chart';


const AreaPopup = observer(({ context }) => {
    const map = context; 
    var chartData = [];
    if (map.popup.area !== undefined ) {
        const data = map.popup.data;
        if (data !== undefined ) {
            var total = data.total_openDefecation_out + data.total_other_out + data.total_hangingToilet_out +
            + data.total_pitSlab_out + data.total_pitNoSlab_out + data.total_bucketLatrine_out + data.total_containerBased_out +
            + data.total_compostingToilet_out + data.total_flushSeptic_out + data.total_flushPit_out + data.total_flushOpen_out + data.total_flushUnknown_out +
            + data.total_flushSewer_out;
            
        } 
        chartData = (data !== undefined ) ? [
        {   "title": "Open Defecation",
            "value": (data.total_openDefecation_out + data.total_other_out + data.total_hangingToilet_out)/total,
            "color": "rgb(191, 64, 64)"
        },
        {
            "title": "Dry Toilets",
            "value": (data.total_pitSlab_out + data.total_pitNoSlab_out + data.total_bucketLatrine_out + data.total_containerBased_out)/total,
            "color": "rgb(188,123,80)"
        },
        {
            "title": "Composting Toilets",
            "value": (data.total_compostingToilet_out)/total,
            "color": "rgb(191, 172, 64)"
        },
        {
            "title": "Flush Toilets (onsite)",
            "value": (data.total_flushSeptic_out + data.total_flushPit_out + data.total_flushOpen_out + data.total_flushUnknown_out)/total,
            "color": "rgb(102, 191, 64)"
        },
        {
            "title": "Flush Toilets (sewered)",
            "value": (data.total_flushSewer_out)/total,
            "color": "rgb(64, 140, 191)"
        }
    ]:[];
    }
    

    const colorScheme = chroma
        .scale(['rgb(191, 64, 64)', 'rgb(188,123,80)', 'rgb(191, 172, 64)', 'rgb(102, 191, 64)', 'rgb(64, 140, 191)'])
        .mode('lch')
        .colors(5);

    return (chartData.length > 0) ? <Popup 
                key={`popup-${map.scenarios[map.activeScenario].id}`}
                position={map.popup.position}
                style={{minWidth: '200px'}}
                >
                <div style={{fontFamily: 'Cabin, sans-serif'}}>
                    <h2 style={{ marginBottom: '0px' }}>{map.popup.area}</h2>
                    <h4 style={{ marginTop: '0px', marginBottom:'20px', padding: 0 }}>
                        Emissions per system category
                    </h4>
                <PieChart
                    // data={chartData}
                    // lineWidth={20}
                    // paddingAngle={18}
                    
                    // label={({ dataEntry }) => {console.log(dataEntry);return dataEntry.value.toExponential(2)}}
                    // labelStyle={(index) => ({
                    //     fill: chartData[index].color,
                    //     fontSize: '8px',
                    //     fontFamily: 'Cabin, sans-serif'
                    // })}
                    // labelPosition={60}

                      data={chartData}
                      lineWidth={100}
                      segmentsStyle={{ transition: 'stroke .3s', cursor: 'pointer' }}
                      animate
                      label={({ dataEntry }) => {return (dataEntry.value !== 0 ) ? Math.round(dataEntry.value*100)+'%': ''}}
                      labelPosition={40}
                      labelStyle={(index) => ({
                        fill: '#fff',
                        opacity: 0.75,
                        fontSize: '8px',
                        pointerEvents: 'none',
                      })}
                      
                    />
                    
                    </div>
            </Popup> : null
});

export default AreaPopup;