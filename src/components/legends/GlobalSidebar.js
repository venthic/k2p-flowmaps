import React from "react";
import { Sidebar, Tab } from 'react-leaflet-sidebarv2';
import { observer } from "mobx-react-lite";
import { withLeaflet } from "react-leaflet";
import "../../styles/leaflet-sidebar.css";
import LayersTwoToneIcon from '@material-ui/icons/LayersTwoTone';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from "@material-ui/core/Button";
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';
import latrine from "../../static/img/latrine.svg";
import treatment from "../../static/img/treatment.svg";
import { ReactSVG } from 'react-svg';
import SummaryPopulationChart from "../../components/charts/SummaryPopulationChart";
import SummarySanitationChart from "../../components/charts/SummarySanitationChart";
import SummaryTreatmentCharts from "../../components/charts/SummaryTreatmentCharts";
import CountryPieChart from "../../components/charts/CountryPieChart";
import skins from '../../config/skins.json';
import Radio from '@material-ui/core/Radio';
import { makeStyles } from '@material-ui/core/styles';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import Box from '@material-ui/core/Box';
import MouseTwoToneIcon from '@material-ui/icons/MouseTwoTone';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import BrushTwoToneIcon from '@material-ui/icons/BrushTwoTone';
import PlayCircleFilledTwoToneIcon from '@material-ui/icons/PlayCircleFilledTwoTone';
import {useHistory} from 'react-router-dom';


const useStyles = makeStyles(theme => ({
    
    enhancedBox: {
        padding: 20,
        marginTop: 10,
        marginBottom: 10,
        borderRadius: 2,
        backgroundColor: '#eee',
        boxShadow: '0 1px 15px rgba(0,0,0,.04), 0 1px 6px rgba(0,0,0,.04)',
        border: '1px solid #e7e7e7'
    }
}));

const GlobalSidebar = observer(({ context, map }) => {

    const classes = useStyles();

    const history = useHistory();

    const onClose = () => {
        context.setSidebarCollapsed(true);
    }
    const onOpen = (id) => {
        context.setSidebarCollapsed(false);
        context.setSidebarSelected(id);
    }
    return <Sidebar id="sidebar" collapsed={context.sidebarCollapsed} selected={context.sidebarSelected}
        onOpen={onOpen.bind(this)} onClose={onClose.bind(this)}>
        <Tab id="home" header="Global Pathogen Emissions" icon={<LayersTwoToneIcon style={{ marginTop: '10px' }} />}>
            {
                (context.country !== null && typeof context.country !== "string") ? 
                    <Grid container style={{paddingTop: '20px'}}><Grid item xs={12} sm={6}><h3 style={{color: '#003347', textShadow: '0 0 black', letterSpacing: '1px'}}>{context.country.subarea}</h3></Grid>
                    {(context.fetchingStatsState || context.stats.length === 0) ? 
                        <Grid item xs={12} sm={12}><div style={{textAlign:'center'}}> <CircularProgress color="primary" /></div></Grid>
                    :
                    [<Grid item xs={12} sm={6} style={{textAlign:'right', fontSize: '13px'}}>
                        <b>Total emissions:</b> {context.stats.sum[0].toFixed(1)}<br/> 
                        <b>Max emissions:</b> {context.stats.max[0].toFixed(1)}<br/> 
                        <b>Min emissions:</b> {context.stats.min[0].toFixed(1)}<br/>
                        <b>Mean emissions:</b> {context.stats.mean[0].toFixed(1)}<br/> 
                    </Grid>,
                    
                    
                    
                        <div style={{width: '100%', marginTop: '30px'}}>
                            { (map.scenarios.length > 0) && 
                                <CountryPieChart style={{width:'100%', textAlign:'center', margin:'auto'}} context={map} scenario={0}/>
                            }
                        </div>,
                        <Button size="large" startIcon={<PlayCircleFilledTwoToneIcon />} 
                        variant='contained'
                        color='secondary' 
                        style={{margin: 'auto', marginTop: '30px'}}
                        onClick={() => { 
                            history.push('/');
                            map.flyToPolygons();
                            map.toggleGeofencingMode(false); 
                            map.toggleScenarioMode();
                            map.toggleInfoMapDialogState(true);
                        }}>Run scenarios</Button>

                    ]
                    
                    }
                </Grid>:
                <div>
                    {(typeof context.country !== "string") && [
                        <p style={{textAlign:'justify'}}>The Global Map has been developed as part of the <a href="https://www.waterpathogens.org" target="_blank">Water-K2P Project</a>. It presents estimates on the amount of pathogens reaching the environment and surface waters. The main sources of pathogens from sanitation systems can be identified and geographic areas of high contamination (hotspots) can be differentiated from areas with lower contamination (pathogen emissions).</p>
                        ,<Box className={classes.enhancedBox} style={{ height: '200px', textAlign: 'center' }}>
                            <p><MouseTwoToneIcon style={{ marginTop: '30px', color: '#42919e' }} fontSize='large' /></p>
                            <Typography variant='subtitle1'>Click on a country to get detailed results.</Typography>
                        </Box>
                    ]
                        
                    }
                </div>
            }
            {
                (typeof context.country === "string") && <div>
                    <h3>{context.country}</h3>
                    <p>No data available.</p>
                </div>
            }
        </Tab>
        {(context.country !== null && (typeof context.country !== "string")) && [
            <Tab id="population" header="Population" icon={<SupervisorAccountIcon style={{ marginTop: '10px' }} />}>
                <h3 style={{color: '#003347', textShadow: '0 0 black', letterSpacing: '1px'}}>{context.country.subarea}</h3>
                <p style={{textAlign:'justify'}}>In the chart below, you can view the total population of <b>{context.country.subarea}</b> and a Urban/Rural breakdown in actual numbers and percentages.</p>
                <SummaryPopulationChart context={map}/>
            </Tab>,
            <Tab id="sanitation" header="Onsite Sanitation" icon={<ReactSVG src={latrine} className="sanitation"/>}>
                <h3 style={{color: '#003347', textShadow: '0 0 black', letterSpacing: '1px'}}>{context.country.subarea}</h3>
                <p style={{textAlign:'justify'}}>The five onsite sanitation categories represent thirteen toilet categories that are also in use by the Joint Monitoring Programme (JMP) of WHO and UNICEF. These comprise: 
                <ul>
                    <li><b>No Facility</b> (open defecation, hanging toilets and other),</li> 
                    <li><b>Dry Toilets</b> (container based, bucket latrine, pit without a slab, pit with a slab),</li>
                    <li><b>Composting Toilets</b>,</li> 
                    <li><b>Flush Toilets (onsite)</b> (flush to open, flush to unknown, flush to pit and flush to septic tank) and</li> 
                    <li><b>Flush Toilets (sewered)</b>.</li>
                </ul> 
                <a href="https://www.waterpathogens.org/sites/default/files/The%20pathogen%20flow%20&%20mapping%20tool_DRAFT_clean.pdf" target="_blank">Learn more</a> on how the individual onsite sanitation categories are dealt with in the model.</p>
                <SummarySanitationChart context={map}/>
            </Tab>,
            <Tab id="treatment" header="Sewage & Fecal Sludge Management" icon={<ReactSVG src={treatment} className="treatment"/>}>
                <h3 style={{color: '#003347', textShadow: '0 0 black', letterSpacing: '1px'}}>{context.country.subarea}</h3>
                <p style={{textAlign:'justify'}}>The <b>Fecal sludge safely managed</b> chart represents the percentage of fecal sludge in rural areas that is covered and buried and the percentage of fecal sludge in urban areas that is taken from the onsite facilities and brought to treatment. </p>
                <p style={{textAlign:'justify'}}>The <b>Sewage treatment</b> chart represents the percentage of sewage from “Flush onsite (sewered)” that actually reaches the treatment plant.</p>
                <p style={{textAlign:'justify'}}>The <b>Pathogen reduction after treatment</b> chart represent the reduction (in percentage or the log removal) that the treatment in the subarea achieves. For pathogens high reductions (e.g. 3 log units or more) are required to sufficiently reduce the number of pathogens from the effluent. These reductions are rarely achieved, also in developed countries. </p>

                <SummaryTreatmentCharts context={map}/>
            </Tab>
            ]
        }
        {/* <Tab id="settings" header="Settings" icon={<BrushTwoToneIcon style={{marginTop: '10px'}}/>} anchor="bottom">
            <FormControl component="fieldset">
                <FormLabel component="legend">Map skin</FormLabel>
                <RadioGroup aria-label="skin" name="skin" defaultValue={context.skin} onChange={(e) => {e.preventDefault(null);}}>
                    {skins.map(skin => { 
                        return <FormControlLabel value={skin.name} control={<Radio />} label={skin.name} />
                    } )}
                </RadioGroup>
            </FormControl>
        </Tab> */}
    </Sidebar>
});

export default withLeaflet(GlobalSidebar);

