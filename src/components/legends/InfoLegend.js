import React from "react";
import { observer } from "mobx-react-lite";
import { withLeaflet } from "react-leaflet";
import DialogDefault from 'react-leaflet-dialog';

const Dialog = withLeaflet(DialogDefault);

const InfoLegend = observer(({ map }) => {

  const hideClose = () => {
    map.infoMapDialog.current.leafletElement.hideClose();
  }
  return <Dialog ref={map.infoMapDialog} id="infoDialog" anchor={[20,20]} size={[400,400]}>
  <div>
    <h4>Instructions</h4>
    <p>Default data are now loaded from JMP or earlier case studies. In the three tabs on the right ("Population", "Onsite sanitation" and "Sewage & fecal sludge management") you can check and amend these data for your selected area as a whole or for the subareas, so that they match your information. You can also use the default data as is. These data will be your baseline data, representing the original situation to compare scenarios to. </p>
    <p>You can now press "Run". You will be prompted to give your baseline a name and flag it as baseline and then run the model. The pathogen emissions are now calculated and shown on the map and you can click on the subareas to see the contribution of onsite sanitation facilities to the overall emissions. You can also click the results summary button at the bottom of the map to see a summary of the results.</p> 
    <p>
    Now you can add scenarios. These scenarios can be created by clicking the "+ New Scenario" button at the top right of the screen. The scenario is initially a copy of the baseline that you can amend to evaluate potential changes in your area, again using the three tabs in the right hand side of the screen. You can see the results in a new map and switch between the maps to see changes in the output. You can also see the differences in the results summary.
    </p>
  </div>
</Dialog>
});

export default withLeaflet(InfoLegend);

