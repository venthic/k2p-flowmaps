
import React from "react";
import { observer } from "mobx-react-lite";
import { withLeaflet } from "react-leaflet";
import DialogDefault from 'react-leaflet-dialog';

const Dialog = withLeaflet(DialogDefault);

const JMPLegend = observer(({ model }) => {

  const colors = ['#76ACB4','#6ADCC0','#fff'];
  const grades = ['Country-level data available', 'High resolution data available', 'No data available'];
  
  const hideClose = () => {
    model.JMPlegendRef.current.leafletElement.lock();
  }

  return <Dialog ref={model.JMPlegendRef} onOpened={()=>hideClose()} id="jmpInfo" anchor={[0,0]} size={[230, 110]}>
  <div style={{textAlign:'left'}}>
    {grades.map((grade, i) =>
      <span className="legendColorJMP"><i className="legendStop" style={{background: colors[i]}}></i>{grades[i]}</span>
    )}
  </div>
  </Dialog>
});

export default withLeaflet(JMPLegend);

