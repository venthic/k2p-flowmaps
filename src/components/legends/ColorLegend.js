
import React from "react";
import { observer } from "mobx-react-lite";
import { withLeaflet } from "react-leaflet";
import DialogDefault from 'react-leaflet-dialog';
import { Colorscale } from 'react-colorscales';


const Dialog = withLeaflet(DialogDefault);

const ColorLegend = observer(({ model, position, anchor }) => {

  const rainbow = ['#96005A', '#4B0091', '#0000C8', '#000DE4', '#0019FF', '#0059FF', '#0098FF', '#16CCCB', '#2CFF96', '#62FF4B', '#97FF00', '#CBF500', '#FFEA00', '#FFAD00', '#FF6F00', '#FF3800', '#FF0000'];
  // const [readMore, setReadMore] = useState(false);
  const hideClose = () => {
    model.colorLegendRef.current.leafletElement.hideClose();
    model.colorLegendRef.current.leafletElement.hideResize();
  }


  return <Dialog ref={model.colorLegendRef}  onOpened={()=>hideClose()} position={position} anchor={anchor} id="colorscale" size={[380, 120]}>
    <div>
    <p>Total emissions (log transformed viral particles / box / year):</p>
    {/* <a className="disclaimer" href="#" onClick={ ()=>model.doStuff() }>{(readMore) ? <span>read less...</span>:<span>read more...</span>}</a> */}
    
    {/* <div class='ruler'>
      { rainbow.map(( stop, key )=> {

        if (key%2 === 0) {
          return <div class='cm'>
            <div class='mm' style={{backgroundColor:stop}}>

            </div>
            { (key+1 < rainbow.length) && <div class='mm' style={{backgroundColor:rainbow[key+1]}}>
              
              </div>
            }
          </div>
        }
        return null;
      })}
      
    </div> */}


    <Colorscale
      style={{ zIndex: 3333 }}
      colorscale={rainbow}
      width={20}
    />
    {/* <div className="stops">{rainbow.slice(0, 9).map((color, i) => { return (i * 2 > 9) ? <span>{i * 2}</span> : <span>&nbsp;{i * 2}</span> })}</div> */}
</div>

  </Dialog>

});

export default withLeaflet(ColorLegend);

