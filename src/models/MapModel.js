import { observable, computed, action, toJS} from "mobx";
import codes from "../config/codes.json";
import ScenarioModel from "./ScenarioModel";
import L from "leaflet";
import React, { createRef } from 'react';
import DataHeaders from "../config/DataHeaders";
import ReportPDF from "../components/ReportPDF";
import shpLoader from '../async/shpLoader.js';
import WebWorker from '../async/workerSetup';
import Axios from 'axios';
import fileDownload from 'js-file-download';
import shp from 'shpjs';

class MapModel{

    @observable.ref mapRef;
    @observable.ref countriesRef;
    @observable.ref areasRef;
    @observable errorMessages;
    @observable selectedArea;
    @observable selectedAreasList;
    @observable selectedAreasDictionary;
    @observable scenarioMode;
    @observable geofencingMode;
    @observable dataViewEnabled;
    @observable activeScenario;
    @observable mapBounds;
    @observable selectedBounds;
    @observable viewBounds;
    @observable focusedCountry;
    @observable searchedCountry;
    @observable searchedCountryLongName;
    @observable resolution;
    @observable pathogen;
    @observable.ref availableResolutions;
    @observable.ref resolutionsData;
    @observable mapLoading;
    @observable multiCountryMode;
    @observable scenarios;
    @observable.ref resolutionDatasets;
    @observable resolutionDataLoading;
    @observable highResolutionDatasetId;
    @observable datasetRecords;
    @observable datasetResources;
    @observable metadataModalState;
    @observable activeScenarioDescription;
    @observable activeScenarioTitle;
    @observable servicedPopulation;
    @observable selectedBounds;
    @observable viewBounds;
    @observable loadingTiff;
    @observable popup;
    @observable.ref worldData;
    @observable.ref worldResources;
    @observable.ref worldTiff;
    @observable.ref worldOutput;
    @observable worldInputId;
    @observable.ref countryData;
    @observable printControl;
    @observable summaryMode;
    @observable baselineScenario;
    @observable animationRendering;
    @observable infoState;
    @observable disclaimerState;
    @observable infoMapDialogState;
    @observable infoMapDialog;
    @observable JMPlegendRef;
    @observable JMPDialogState;
    @observable sourceDataset;
    @observable worker;
    @observable session;
    @observable highResolutionGeodata;
    @observable reportData;
    @observable generateReportModalState;
    @observable generatePDFButton;
    @observable screenshotObject;
    @observable.ref sanitationLadder;
    

    constructor(...props) {
        this.mapRef = createRef();
        this.countriesRef = createRef();
        this.areasRef = createRef();
        this.errorMessages = [];
        this.selectedArea = null;
        this.selectedAreasList = [];
        this.selectedAreasDictionary = [];
        this.scenarioMode = false;
        this.geofencingMode = false;
        this.dataViewEnabled = false;
        this.activeScenario = 0;
        this.mapBounds = [[-90,-180], [90,180]];
        this.maxZoom = 10;
        this.selectedBounds = [];
        this.viewBounds = [];
        this.focusedCountry = '';
        this.searchedCountry = '';
        this.searchedCountryLongName = '';
        this.resolution = 0;
        this.availableResolutions = 0;
        this.resolutionsData = [];
        this.resolutionDataLoading = false;
        this.mapLoading = false;
        this.multiCountryMode = false;
        this.scenarios = [];
        this.resolutionDatasets = [];
        this.highResolutionDatasetId = 0;
        this.datasetResources = [];
        this.datasetRecords = [];
        this.metadataModalState = false;
        this.activeScenarioTitle = '';
        this.activeScenarioDescription = '';
        this.servicedPopulation = 0;
        this.selectedBounds='';
        this.viewBounds='';
        this.loadingTiff = false;
        this.pathogen = '';
        this.popup = {};
        this.worldData = [];
        this.worldResources = [];
        this.worldOutput = [];
        this.countryData = [];
        this.worldInputId;
        this.printControl = null;
        this.summaryMode = false;
        this.baselineScenario = -1;
        this.animationRendering = false;
        this.infoState = false;
        this.disclaimerState = false;
        this.infoMapDialog = createRef();
        this.infoMapDialogState = true;
        this.JMPlegendRef = createRef();
        this.JMPDialogState = false;
        this.colorLegendRef = createRef();
        this.colorLegendState = true;
        this.sourceDataset = '';
        this.worker = new WebWorker(shpLoader);
        this.session = '';
        this.highResolutionGeodata = [];
        this.generateReportModalState =false;
        this.generatePDFButton = true;
        this.screenshotObject = '';
        this.reportData = {
            authorName:'',
            organization: '',
            street: '',
            city: '',
            state: '',
            street: '',
            country: '',
            phone: '',
            email: '',
            name: ''
          };
        this.sanitationLadder = '';
    }
    
    // @computed get nodePropsVisible(){
    //     return this.selectedNode !== null;
    // }

    @action addPopup(e) {
        if (this.scenarioMode) {
            var area = (e.layer.feature.properties.hasOwnProperty("NAME_"+this.resolution)) ? e.layer.feature.properties["NAME_"+this.resolution] : e.layer.feature.properties.name;
            var data = this.scenarios[this.activeScenario].payload.outputRecords.filter((record) => record.subarea === area);
            if (data.length > 0) {
                this.popup = { 
                    key: 'popup',
                    position: e.latlng,
                    area: area,
                    data: data[0]
                };
            }
            
        }
    }

    @action getShpWorker(url) {
        this.worker.postMessage('shpLoad');

		this.worker.addEventListener('awesome', event => {
			console.log(event);
		});
    }

    @action
    setGenerateReportModalState(status) {
      this.generateReportModalState = status;
    }

    @action
    print() {
        ReportPDF(this, () => {this.setGeneratePDFButton(true)});
    };
  
    @action
    setGeneratePDFButton(status) {
      this.generatePDFButton = status;
    }

    @action
    setReportValue(field, value) {
      this.reportData[field] = value;
    }

    @action
    setScreenshotObject(value) {
        this.screenshotObject = value;
    }

    @action
    setSanitationLadder(uri) {
        this.sanitationLadder = uri;
    }

    @action
    prepareReport() {
        //this.setActiveScenario(null,this.baselineScenario);
        this.setGeneratePDFButton(false);
        this.setScreenshotObject('baselineLadder');
        setTimeout(() => {
            this.setScreenshotObject(''); 
            this.print();
        }, 3000);
    }
    
    @action
    setInfoState(status) {
        this.infoState = status;
    }

    @action
    setDisclaimerState(status) {
        this.disclaimerState = status;
    }

    @action setAnimationRendering(value) {
        this.animationRendering = value;
    }

    @action setBaselineScenario(scenario) {
        this.baselineScenario = scenario;
    }

    @action setPrintControl(ref) {
        this.printControl = ref;
    }


    @action addArea(area) {
        this.selectedAreasList.push(area);
    }

    @action addAreaToDictionary(id, area) {
        this.selectedAreasDictionary.push({"id":id,"area": area});
    }

    @action removeAreaByKey(areaKey) {
        this.selectedAreasList.splice(areaKey, 1);
    }

    @action toggleSummaryMode(){
        this.summaryMode = !this.summaryMode;
        this.setAnimationRendering(true);
        //this.setActiveScenario(null,this.baselineScenario);
        
        setTimeout( () => this.setAnimationRendering(false), 1000);
        if (!this.summaryMode) {
            this.scenarios[this.activeScenario].payload.setActiveTab(null,0);
        }
        
    }

    @action doStuff() {
        alert('stuff');
    }

    @action setSelectedArea(area){
        this.selectedArea = area;
    }

    @action clearSelectedAreas() {
        this.selectedAreasList = [];
        this.selectedAreasDictionary = [];
        this.toggleGeofencingMode(false);
        //countriesRef.current.leafletElement.resetStyle(mapRef.current.leafletElement._layers);
    }

    @action toggleScenarioMode() {
        this.scenarioMode = !this.scenarioMode;
    }

    @action toggleGeofencingMode(value) {
        this.geofencingMode = value;
    }

    @action setMultiCountryMode(value) {
        this.multiCountryMode = value;
    }

    @action isMapLoading(value) {
        this.mapLoading = value;
    }

    @action setMaxZoom(maxZoom) {
        this.maxZoom = maxZoom;
    }

    @action setMapBounds(bounds) {
        this.mapBounds = bounds;
    }

    @action setSearchedCountry(country) {
        this.searchedCountry = country;
    }

    @action setFocusedCountry(country) {
        this.focusedCountry = country;
        this.fetchHighResolutionData();
    }

    @action hasHighResolutionData(geo) {
       return this.countryData.filter((record) => record.tags.filter((tag) => tag.name==geo).length > 0).length > 0;
    }

    @action setWorldData(data) {
        this.worldData = data;
    }

    @action setWorldResources(resources) {
        this.worldResources = resources;
    }

    @action setWorldTiff(url) {
        this.worldTiff = url;
    }

    @action setWorldOutput(value) {
        this.worldOutput = value;
    }

    @action setCountryData(data) {
        this.countryData = data;
    }

    @computed get focusedCountryLongName() {
        if (this.focusedCountry != '') {
            return codes.filter((code) => code["alpha-3"]==this.focusedCountry)[0].name;
        }
        return '';
    }

    @action addToResolutionDatasets(dataset) {
        this.resolutionDatasets.push(dataset);
    }

    @action setWorldInputId(id) {
        this.worldInputId = id;
    }

    @action setHighResolutionGeodata(data) {
        this.highResolutionGeodata = data;
    }

    @action fetchHighResolutionGeodata(countryCode) {
        fetch("https://data.waterpathogens.org/api/3/action/group_package_show?id=countries-geodata")
            .then(res => res.json())
            .then(
            (res) => {
                for (var i in res.result) {
                    var tagFilteredData = res.result[i].tags.filter((tag) => tag.name===countryCode);
                    if (tagFilteredData.length > 0) {
                        this.setHighResolutionGeodata(res.result[0].resources);
                    }
                }
            },
            (error) => {
                console.log(error);
            });
    }

    @action fetchWorldData() {
        this.resolutionDatasets = [];
        this.setResolutionDataLoading(true);
        fetch("https://data.waterpathogens.org/api/3/action/group_package_show?id=pfm-configuration-data")
            .then(res => res.json())
            .then(
            (res) => {
                this.setCountryData(res.result);
                for (var i in res.result) {
                    var tagFilteredData = res.result[i].tags.filter((tag) => tag.name=='World');
                    if (tagFilteredData.length > 0) {
                        this.addToResolutionDatasets(res.result[i]);
                        this.setWorldInputId(res.result[i].id);
                        this.setWorldResources(res.result[i].resources);
                        this.setWorldTiff(res.result[i].resources.filter(resource => resource.mimetype === "image/tiff" && resource.name.toLowerCase().includes("output"))[0].url);
                        var output_id = res.result[i].resources.filter(resource => resource.mimetype==="text/csv" && resource.name.toLowerCase().includes("output"))[0].id
                        fetch("https://data.waterpathogens.org/api/3/action/datastore_search?resource_id="+output_id+"&limit=1000")
                            .then(res => res.json())
                            .then(
                            (res) => {
                                this.setWorldOutput( res.result.records );
                            },
                            (error) => {
                                console.log(error);
                            }
                        )
                        var input_id = res.result[i].resources.filter(resource => resource.mimetype==="text/csv" && resource.name.toLowerCase().includes("input"))[0].id
                        fetch("https://data.waterpathogens.org/api/3/action/datastore_search?resource_id="+input_id+"&limit=1000")
                            .then(res => res.json())
                            .then(
                            (res) => {
                                this.setWorldData( res.result.records);
                            },
                            (error) => {
                                console.log(error);
                            }
                        )
                    
                    }
                }
                this.setResolutionDataLoading(false);
            },
            (error) => {
                console.log(error);
            }
        )   
    }

    @action fetchHighResolutionData() {
        this.resolutionDatasets = [];
        this.setResolutionDataLoading(true);
        for (var i in this.countryData) {
            var tagFilteredData = this.countryData[i].tags.filter((tag) => tag.name===this.focusedCountry || tag.name==="ADM0");
            if (tagFilteredData.length > 0) {
                if (this.resolutionDatasets.filter((dataset) => { return dataset.id === tagFilteredData[0].id}).length === 0) {
                    this.addToResolutionDatasets(this.countryData[i]);
                }
            }
        }
        this.setResolutionDataLoading(false);
    }

    @action setResolutionDataLoading(status) {
        this.resolutionDataLoading = status;
    }

    @action toggleInfoMapDialogState (state) {
        this.infoMapDialogState = state;
        if (this.infoMapDialogState) {
            this.infoMapDialog.current.leafletElement.open();
        }
        else {
            this.infoMapDialog.current.leafletElement.close();
        }
    } 

    @action toggleJMPDialogState (state) {
        this.JMPDialogState = state;
        if (this.JMPDialogState) {
            this.JMPlegendRef.current.leafletElement.open();
        }
        else {
            this.JMPlegendRef.current.leafletElement.close();
        }
    } 

    @action toggleColorLegendState (state) {
        this.colorLegendState = state;
        if (this.colorLegendState) {
            this.colorLegendRef.current.leafletElement.open();
        }
        else {
            this.colorLegendRef.current.leafletElement.close();
        }
    } 

    @action setServicedPopulation(population) {
        this.servicedPopulation = population;
    }

    @action setSearchedCountryLongName(country) {
        this.searchedCountryLongName = country;
    }


    @action setActiveScenario(event,scenario) {
        this.activeScenario = scenario;
        this.activeScenarioTitle = this.scenarios[scenario].title;
        this.activeScenarioDescription = this.scenarios[scenario].description;
    }

    @action toggleDataView() {
        this.dataViewEnabled = !this.dataViewEnabled;
    }

    @action setSelectedBounds(bounds) {
        this.selectedBounds = bounds;
    }

    @action setViewBounds(bounds) {
        this.viewBounds = bounds;
    }

    @action setPathogen(pathogen) {
        this.pathogen = pathogen;
    }

    @action setResolution(resolution) {
        this.selectedAreasList = [];
        var map = this;

        var base = '';
        if (resolution === 0) {
            var base = this.highResolutionGeodata.filter((resource) => resource.name === 'ADM0')[0].url;
        }
        else {
            var base = this.highResolutionGeodata.filter((resource) => resource.name.replace('ADM', '') === resolution+'')[0].url;
        }
        this.setResolutionDataLoading(true);
        shp(base).then( ( data ) => {
            map.setResolutionsData( data );
            var polygon = L.geoJson(data);
            var bounds = polygon.getBounds();
            map.mapRef.current.leafletElement.fitBounds(bounds);
            var dataset = map.resolutionDatasets.filter((dataset) => dataset.tags.filter(tag => tag.name === 'ADM'+map.resolution).length > 0 )[0];
            map.setHighResolutionDatasetId(dataset);
            map.setResolutionDataLoading(false);
            
        });
        
        this.resolution = resolution;
    }

    @action setHighResolutionDatasetId (dataset) {
        if (dataset !== null ) {
            this.highResolutionDatasetId = dataset.id;
            this.datasetResources = dataset.resources;
            if (dataset.id !== this.worldInputId) {
                var input_id = this.datasetResources.filter(resource => resource.mimetype=="text/csv" && resource.name.toLowerCase().includes("input"))[0].id
                fetch("https://data.waterpathogens.org/api/3/action/datastore_search?resource_id="+input_id)
                    .then(res => res.json())
                    .then(
                    (res) => {
                        for (var j in res.result.records) {
                            var record = res.result.records[j];
                            for (var i in this.areasRef.current.props.data.features) {
                                
                                var feature = this.areasRef.current.props.data.features[i];
                                if (feature.properties["GID_"+this.resolution] === record.gid) {
                                    this.addArea(record.gid);
                                    this.addAreaToDictionary(record.gid, record.subarea);
                                }
                            }
                        }
                    
                        this.setdatasetRecords(res.result.records);
                        this.areasRef.current.leafletElement.resetStyle();
                        
                    },
                    (error) => {
                        console.log(error);
                    }
                
                )
            }
            else {
                this.addArea(this.focusedCountry);
                var worldDataMatch = this.worldData.filter((country) => country.gid === this.focusedCountry);
                this.addAreaToDictionary(this.focusedCountry, worldDataMatch[0].subarea);
            
                this.setdatasetRecords(worldDataMatch);
            }
        }
        else {
            this.highResolutionDatasetId = this.worldInputId;
            this.addArea(this.focusedCountry);
            var worldDataMatch = this.worldData.filter((country) => country.gid === this.focusedCountry);
            this.addAreaToDictionary(this.focusedCountry, worldDataMatch[0].subarea);
        
            this.setdatasetRecords(worldDataMatch);
            
            this.colorLegendState = true;
        }
        
        this.setSourceDataset('https://data.waterpathogens.org/dataset/'+this.highResolutionDatasetId);
    }

    @action setSourceDataset(dataset) {
        this.sourceDataset = dataset;
    } 

    @action setdatasetRecords(records) {
        this.datasetRecords = records;
    }

    @action addErrorMessage(msg) {
        this.errorMessages.push(msg);
    }

    @action clearErrorMessages(msg) {
        this.errorMessages = [];
    }

    @action setAvailableResolutions(array) {
        this.availableResolutions = array;
    }

    @action setResolutionsData(data) {
        this.resolutionsData = data;
    }

    @action setActiveScenarioTitle(title) {
        this.scenarios[this.activeScenario].title = title;
    }

    @action setActiveScenarioDescription(desc) {
        this.scenarios[this.activeScenario].description = desc;
    }

    @action setMetadataModalState(state) {
        this.metadataModalState = state;
    }

    @action updateActiveScenarioMetadata() {
        this.scenarios[this.activeScenario].title = this.activeScenarioTitle;
        this.scenarios[this.activeScenario].description = this.activeScenarioDescription;
    }

    // difference(column,) {
    // return computed(()=> { 
        
    //     return 0;
    //     }).get();
    // }

    @action
    applyDataFilter(records) {
        const headers = new DataHeaders();
        const columnDefs = headers.analysisEditorfields;
        var pathogenDataThere = false;
        for (var i=0; i<columnDefs.length; i++) {
            if (columnDefs[i].pathogen) {
                if (records[0][columnDefs[i].field+"_"+this.pathogen.toLowerCase()]) {
                    pathogenDataThere = true;
                }
            }
            
        }
        if (pathogenDataThere) {
            records = JSON.parse(JSON.stringify(records).split('_'+this.pathogen.toLowerCase()).join(''));
            for (var i=0; i<records.length; i++) {
                for (var j=0; j<columnDefs.length; j++) {
                    if (columnDefs[j].pathogen) {
                        // records[i][columnDefs[j].field] = records[i][columnDefs[j].field+"_"+this.pathogen];
                        delete records[i][columnDefs[j].field+"_virus"];
                        delete records[i][columnDefs[j].field+"_protozoa"];
                    }
                    if (columnDefs[j].numType) {
                        if (columnDefs[j].numType === "float") {
                            records[i][columnDefs[j].field] = isNaN(parseFloat(records[i][columnDefs[j].field])) ? 0.00001 : parseFloat(records[i][columnDefs[j].field]);
                        }
                        else {
                            records[i][columnDefs[j].field] = isNaN(parseInt(records[i][columnDefs[j].field])) ? 0 : parseInt(records[i][columnDefs[j].field]);
                        }
                    }
                }
            }
        }
        else {
            this.addErrorMessage('Pathogen group selected is not supported by this dataset.');
            return [];
        }
        return records;
    } 

    @action addScenario(scenario) {
        var payload = new ScenarioModel();
        if (scenario == null) {
            if (this.multiCountryMode) {
                this.setdatasetRecords(this.worldData.filter((row) => this.selectedAreasList.includes(row.gid)));
            }
            var records = this.applyDataFilter(toJS(this.datasetRecords));
            if (records.length > 0 ) {
                payload.newRecords(records);
                payload.copyRecords();
                this.scenarios.push( {
                    id: 0,
                    title: 'Default',
                    description: 'This is a scenario populated with default data values.',
                    payload: payload
                })
                //this.run();
            }
        }
        else {
            payload.newRecords(toJS(scenario.payload.records));
            payload.copyRecords();
            this.scenarios.push( {
              id: this.scenarios.length,
              title: 'Scenario #'+this.scenarios.length,
              description: '',
              payload: payload  
            });
            this.setActiveScenario( null, this.scenarios.length - 1 );
            this.setMetadataModalState(true);
        }
        payload.initGridOptions();
    }

    @action eraseScenarios() {
        this.scenarios = [] ;
    }

    @action deleteScenario() {
        this.scenarios.splice(this.activeScenario, 1);
        for (var i = 0; i < this.scenarios.length; i++) {
            this.scenarios[i].id = i;
        }
        this.setActiveScenario(null, 0);
    }

    @action flyToBounds() {
        if (this.mapRef.current !== null) {
            // console.log(this.mapRef.current);
            this.mapRef.current.leafletElement.fitBounds(this.viewBounds);
        }
    }

    @action flyToPolygons() {
        var ref = null;
        if (this.countriesRef.current !== null) {
            ref = this.countriesRef;
        }
        else {
            ref = this.areasRef;
        }
        var selectedFeatures = ref.current.props.data.features
                                .filter(feature => (this.selectedAreasList.includes(feature.id) ||
                                this.selectedAreasList.includes(feature.properties["GID_"+this.resolution])
                                ));
        // countriesRef.current.leafletElement.resetStyle(countriesRef.current.leafletElement._layers);
        var polygon = L.geoJson(selectedFeatures);
        var bounds = polygon.getBounds();
        if ( bounds.getNorthEast().lng === 180 && bounds.getSouthWest().lng === -180) {
            
            var topRight = L.latLng([50.85360287457749, 186.81229925270603]);
            var bottomLeft = L.latLng([32.28195040994254, 46.99349796160391]);
            this.mapRef.current.leafletElement.fitBounds([topRight, bottomLeft]);
        }
        else {
            this.mapRef.current.leafletElement.fitBounds(bounds);
        }
        this.viewBounds = bounds;
        this.selectedBounds = this.toWKT(bounds);
        
        
    }

    @action setSession(session) {
        this.session = session;
    }

    @action
    toWKT (bounds) {
            var neLat = bounds.getNorthEast().lat;
            var neLng = bounds.getNorthEast().lng;
            var swLat = bounds.getSouthWest().lat;
            var swLng = bounds.getSouthWest().lng; 
            return "POLYGON((" +
                    swLng + " " + swLat + "," +
                    swLng + " " + neLat + "," +
                    neLng + " " + neLat + "," +
                    neLng + " " + swLat + "," +
                    swLng + " " + swLat +
                    "))";
        
        };

        @action run() {
            this.setLoadingTiff(true);
            this.scenarios[this.activeScenario].payload.setGeoTiff('');
            var scenario = this.activeScenario;
            var resources = ( this.datasetResources.length > 0 ) ? this.datasetResources : this.worldResources;
            var isoraster = resources.filter((resource) => resource.mimetype==="image/tiff" && resource.name.toLowerCase().includes("isoraster"));
            var popurban = resources.filter((resource) => ((resource.mimetype==="image/tiff" || resource.mimetype==="text/plain") && resource.name.toLowerCase().includes("urban")));
            var poprural = resources.filter((resource) => ((resource.mimetype==="image/tiff" || resource.mimetype==="text/plain") && resource.name.toLowerCase().includes("rural")));
            var session = (this.session !== '') ? this.session : null;
            isoraster = (isoraster.length > 0) ? isoraster[0].url : '';
            popurban = (popurban.length > 0) ? popurban[0].url : '';
            poprural = (poprural.length > 0) ? poprural[0].url : '';
            
            var wwtp_available = 2;
            var wwtp = '';

            if ((resources.filter((resource) => resource.mimetype=="text/csv" && resource.name.toLowerCase().includes("wwtp"))).length > 0) {
                wwtp_available = 3;
                wwtp = resources.filter((resource) => resource.mimetype=="text/csv" && resource.name.toLowerCase().includes("wwtp"))[0].url 
            }
            
            fetch("https://server.waterpathogens.org/flowmaps/run", {
              body: "human_data="+this.toCSV(this.scenarios[this.activeScenario].payload.records)+"&isoraster="+isoraster+"&popurban="+popurban+"&poprural="+poprural+"&wwtp="+wwtp +"&level="+this.resolution+"&wkt_extent="+this.selectedBounds+"&id="+this.scenarios[this.activeScenario].id+"&wwtp_available="+wwtp_available+"&pathogen_type="+this.pathogen+'&sessionId='+session,
              headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                "Cache-control": "no cache"
              },
              method: "POST"
            }).then(res => res.json())
            .then(
                (res) => {
                    this.setLoadingTiff(false);
                    if (!res["error"]) {
                        console.log(res);
                        this.scenarios[scenario].payload.setPng('https://tools.waterpathogens.org/data/'+ res.results.figs[0]);
                        if (this.resolution < 4) {
                            this.scenarios[scenario].payload.setGeoTiff('https://tools.waterpathogens.org/data/'+ res.results.grid.file[0]);
                            this.scenarios[scenario].payload.setMinValue(res.results.grid.min[0]);
                            this.scenarios[scenario].payload.setMaxValue(res.results.grid.max[0]);
                        }
                        this.scenarios[scenario].payload.setOutputRecords(res.results.emissions);
                        this.setSession(res.results.session);
                        this.toggleColorLegendState(true);
                    }
                    else {
                        this.addErrorMessage('There was an error while executing the model. Please check your input data.');
                    }
                },
                (error) => {
                    console.log(error);
                    this.addErrorMessage('Model server is unreachable. Please contact the tool administrator.');
                });
        }

        @action
        setLoadingTiff(value) {
            this.loadingTiff = value;
        }

        @action
        toCSV(json) {
            var fields = Object.keys(json[0])
            var replacer = function(key, value) { return value === null ? '' : value } 
            var csv = json.map(function(row){
            return fields.map(function(fieldName){
                return JSON.stringify(row[fieldName], replacer)
            }).join(',')
            })
            csv.unshift(fields.join(',')) // add header column
            csv = csv.join('\r\n');
            return encodeURIComponent(csv);
        }

        @action downloadFromUrl( url, filename) {
            Axios.get(url, {
                responseType: 'blob',
            }).then(res => {
            fileDownload(res.data, filename);
            });
        }

        @action downloadFromServer() {
            const data = {
                extent: this.scenarios.length,
                sessionId: (this.session !== '') ? this.session : null
            };
            fetch("https://server.waterpathogens.org/flowmaps/downloadAll", {
              body: JSON.stringify(data),
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
              method: "POST"
            }).then((res)=>res.json()).then((data)=>{
                this.downloadFromUrl('https://tools.waterpathogens.org/data/'+data.file, 'GWPP-PFMT_bundle.zip');
            });
        }
    
}
export default MapModel;
