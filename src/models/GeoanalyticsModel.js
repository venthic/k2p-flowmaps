import { observable, action, toJS } from "mobx";
import { createRef } from 'react';
import ScenarioModel from '../models/ScenarioModel';

class GeoanalyticsModel{
    
    @observable geoTiff;
    @observable scale;
    @observable skin;
    @observable geometry;
    @observable stats;
    @observable countryDialogState;
    @observable countryDialog;
    @observable country;
    @observable sidebarCollapsed;
    @observable sidebarSelected;
    @observable fetchingStatsState;
    @observable scenarioHandler;

    constructor(...props) {
        this.geoTiff = null;
        this.geometry = null;
        this.skin = 'CartoDB.Positron';
        this.stats = [];
        this.countryDialogState = false; 
        this.countryDialog = createRef();
        this.country = null;
        this.sidebarCollapsed = false;
        this.sidebarSelected = 'home';
        this.fetchingStatsState = false;
        this.scenarioHandler = new ScenarioModel();
    }

    
    @action 
    setSkin(value) {
        this.skin = value;
    }

    @action
    initGeoTiff(url) {
        this.geoTiff = url;
    }

    @action
    setGeometry(geometry) {
        this.geometry = geometry;
    }

    @action
    setCountry(country) {
        this.country = country;
    }

    @action
    toggleCountryDialogState(status) {
        this.countryDialogState = status;
        if (this.countryDialogState) {
            this.countryDialog.current.leafletElement.open();
        }
        else {
            this.countryDialog.current.leafletElement.close();
        }
    }

    @action
    setSidebarSelected(id) {
        this.sidebarSelected = id;
    }

    @action
    setSidebarCollapsed(value) {
        this.sidebarCollapsed = value;
    }

    @action
    setStats(value) {
        this.stats = value;
    }

    @action 
    setFetchingStatsState(value) {
        this.fetchingStatsState = value;
    }

    @action
    fetchStats() {
        this.setStats([]);
        this.setFetchingStatsState(true);
        const geometry = JSON.stringify(toJS(this.geometry));
        fetch("https://server.waterpathogens.org/geoanalytics/geoanalytics", {
              body: "geotiff="+this.geoTiff+"&action=stats&geometry="+geometry,
              headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                "Cache-control": "no cache"
              },
              method: "POST"
            }).then(res => res.json())
            .then(
                (res) => {
                    console.log(res);
                    this.setStats(res.stats);
                    this.setFetchingStatsState(false);
                },
                (error) => {
                    console.log(error);
                    this.setFetchingStatsState(false);
            });
    }

    @action
    initScenarioOptions() {
        this.scenarioHandler.newRecords(toJS(this.country));
    } 
}
export default GeoanalyticsModel;
