import { observable, computed, action, toJS } from "mobx";
import DataHeaders from "../config/DataHeaders";
import toiletColumns from "../config/toiletColumns";
import toiletHandles from "../config/toiletColumnsChanges";
import functions from "../config/functions";
import { createRef } from 'react';

class ScenarioModel{
    
    @observable activeTab;
    @observable records;
    @observable percentageAmount;
    @observable minValue;
    @observable maxValue;
    @observable recordsCopy;
    @observable gridOptions;
    @observable toiletFractions;
    @observable toiletFractionsPrev;
    @observable toiletFractionHandles;
    @observable treatmentFractions;
    @observable activeToiletTab;
    @observable dataFunctions;
    @observable geoTiff;
    @observable png;
    @observable selectedAreasDialogTab;
    @observable selectedAreasDialogValues;
    @observable.ref gridApi;
    @observable outputRecords;
    @observable selectedContext;
    @observable fromToilet;
    @observable toToilet;
    @observable outputChartValues;
    @observable maxEmissionsPerCapita;
    @observable.ref outputLadder;

    constructor(...props) {
        this.records  = [];
        this.recordsCopy = [];
        this.percentageAmount = 0.1;
        this.activeTab = 0;
        this.activeToiletTab = 0;
        this.toiletFractions = [20, 20, 20, 20, 20];
        this.toiletFractionHandles = [];
        this.toiletFractionsPrev = [];
        this.treatmentFractions = [20, 20, 20, 20, 20];
        this.dataFunctions = [];
        this.geoTiff = '';
        this.png = '';
        this.gridOptions = {};
        this.selectedAreasDialogTab = -1;
        this.selectedAreasDialogValues = [[],[],[]];
        this.gridApi = null;
        this.outputRecords = [];
        this.selectedContext = 'both';
        this.fromToilet = -1;
        this.toToilet = -1;
        this.outputChartValues = [];
        this.maxEmissionsPerCapita = 0;
        this.outputLadder = '';
    }

    @computed get initialToiletFractions() {
        var initialArray = [];
        for (var i=0; i < toiletColumns.length-1; i++) {
            var sum = 0;
            for (var j=0; j <= i; j++ ) {
                sum += this.populationWeightedAvg(toiletColumns[j].columns, this.selectedContext, true, []);
            }
            initialArray.push(sum);
        }
        return initialArray;
    }

    @action setDesiredPercentagePoints(handles) {

        var target = [];
        var source = [];
        var difference = 0;
        for (var i=0; i<handles.length; i++) {
            if (handles[i] > this.toiletFractionHandles[i]) {
                source = toiletHandles[i].increaseSourceColumns;
                target = toiletHandles[i].increaseTargetColumns;
                difference = Math.abs(handles[i] - this.toiletFractionHandles[i])/100;
            }
            else if (handles[i] < this.toiletFractionHandles[i]) {
                source = toiletHandles[i].decreaseSourceColumns;
                target = toiletHandles[i].decreaseTargetColumns;
                difference = Math.abs(handles[i] - this.toiletFractionHandles[i])/100;
            }
        }
        if (this.toiletFractions[toiletColumns.filter(c => c.columns.includes(source[0]))[0].id] == 0) {
            this.movePercentagePoints(source, target, 'urb', 0.0001, -1);
            this.movePercentagePoints(source, target, 'rur', 0.0001, -1);
        }
        else {
            this.movePercentagePoints(source, target, 'urb', 0.0001, difference);
            this.movePercentagePoints(source, target, 'rur', 0.0001, difference);
        }
    }

    
    @action
    setOutputLadder(uri) {
        this.outputLadder = uri;
    }

@computed get getMaximumPerCapita() {
    var values = this.outputRecords.map((obj) => {
        var pop = this.records.filter(record => record.subarea === obj.subarea)[0]['population'];
        return (obj['total_openDefecation_out']+obj['total_other_out']+obj['total_hangingToilet_out'])/pop +
        + obj['total_compostingToilet_out']/pop + (obj['total_flushSeptic_out']+obj['total_flushPit_out']+obj['total_flushOpen_out']+obj['total_flushUnknown_out'])/pop +
        + (obj['total_pitSlab_out']+obj['total_pitNoSlab_out']+obj['total_bucketLatrine_out']+obj['total_containerBased_out'])/pop
    + obj['total_flushSewer_out']/pop});
    var maxValues = values.sort((function(a, b) {
        return b - a;
      }));
      return maxValues[0]; 
    
    
}

    @action calculateOutputChartValues() {
        var data = [{
            name: 'No Facility',
            data: this.outputRecords.map((obj) => { 
              var pop =this.records.filter(record => record.subarea === obj.subarea)[0]['population']; 
              
              var value = (obj['total_openDefecation_out']+obj['total_other_out']+obj['total_hangingToilet_out'])/pop;
              this.incrementMaxEmissionsPerCapita(value);
              return value}) 
          }, {
            name: 'Dry Toilets',
            data: this.outputRecords.map((obj) => {
              var pop = this.records.filter(record => record.subarea === obj.subarea)[0]['population'];
              var value = (obj['total_pitSlab_out']+obj['total_pitNoSlab_out']+obj['total_bucketLatrine_out']+obj['total_containerBased_out'])/pop;
              this.incrementMaxEmissionsPerCapita(value);
              return value})
          }, {
            name: 'Composting Toilets',
            data: this.outputRecords.map((obj) => {
              var pop = this.records.filter(record => record.subarea === obj.subarea)[0]['population'];
              var value = obj['total_compostingToilet_out']/pop;
              this.incrementMaxEmissionsPerCapita(value);
              return value})
          }, {
            name: 'Flush toilets (onsite)',
            data: this.outputRecords.map((obj) => {
              var pop = this.records.filter(record => record.subarea === obj.subarea)[0]['population'];
              var value = (obj['total_flushSeptic_out']+obj['total_flushPit_out']+obj['total_flushOpen_out']+obj['total_flushUnknown_out'])/pop;
              this.incrementMaxEmissionsPerCapita(value);
              return value})
          }, {
            name: 'Flush toilets (sewered)',
            data: this.outputRecords.map((obj) => {
              var pop = this.records.filter(record => record.subarea === obj.subarea)[0]['population'];
              var value = obj['total_flushSewer_out']/pop;
              this.incrementMaxEmissionsPerCapita(value);
              return value})
          }]
    }

    @action
    incrementMaxEmissionsPerCapita(value) {
        this.maxEmissionsPerCapita += value;
    }

    @action
    movePercentagePoints(fromColumns, toColumns, context, precision, difference) {
        //source category bar moving to the right (increase)
        
        var targetContextTotal = 0;
        var contextTotal = 0;
        var regionAvailablePoints = [];
        var regionAllocatedPoints = [];
        var sourceSplice = [];
        var targetSplice = [];

        for (var i=0; i<this.records.length; i++) { 
            for (var j in toColumns) {
                var contextPop = (context === 'urb') ? this.records[i]['fraction_urban_pop'] : (1-this.records[i]['fraction_urban_pop']); 
                contextTotal += contextPop*this.records[i].population;
                targetContextTotal += this.records[i][toColumns[j]+"_"+context]*this.records[i].population*contextPop;
            }
            
            var availablePoints = 0;
            for (var k in fromColumns) {
                availablePoints += this.records[i][fromColumns[k]+"_"+context];
            }
            regionAvailablePoints.push(availablePoints);

            if (availablePoints === 0) {
                availablePoints = 0.00001;
            }
            
            sourceSplice[i] = [];
            for (var l in fromColumns) {
                sourceSplice[i].push(this.records[i][fromColumns[l]+"_"+context]/availablePoints);
            }

            var allocatedPoints = 0;
            for (var m in toColumns) {
                allocatedPoints += this.records[i][toColumns[m]+"_"+context];
            }
            regionAllocatedPoints.push(allocatedPoints);
            
            if (allocatedPoints === 0) {
                allocatedPoints = 0.00001;
            }

            targetSplice[i] = [];
            for (var n in toColumns) {
                
                targetSplice[i].push(this.records[i][toColumns[n]+"_"+context]/allocatedPoints);
            }
        }
        var current = targetContextTotal/contextTotal;
        //if (this.toiletFractions[toiletColumns.filter(c => c.columns.includes(fromColumns[0]))[0].id] == 0) {
        if (difference == -1) {    
            //console.log('Full point allocation in '+context+' context');
            for (var i=0; i<this.records.length; i++) { 
                
                for (var j in toColumns) {
                    this.records[i][toColumns[j]+"_"+context] += regionAvailablePoints[i]*targetSplice[i][j];
                }
                for (var k in fromColumns) {
                    this.records[i][fromColumns[k]+"_"+context] = 0.0001;
                }
            }
        }
        else {
            
            var goal = current + difference;
            
            
            // console.log('Incremental point allocation in '+context+' context');
            // console.log(goal);
            // console.log(current);
            //var o = 0;
            while (current < goal) {
                var ofNeed = 0;
                var min = 999;
                //o++; 
                sourceSplice = [];
                targetSplice = [];
                for (var i=0; i<this.records.length; i++) { 
                    
                    var availablePoints = 0;
                    for (var k in fromColumns) {
                        availablePoints += this.records[i][fromColumns[k]+"_"+context];
                    }

                    if (availablePoints === 0) {
                        availablePoints = 0.0001;
                    }
                    //regionAvailablePoints.push(availablePoints);

                    var allocatedPoints = 0;
                    for (var m in toColumns) {
                        allocatedPoints += this.records[i][toColumns[m]+"_"+context];
                    }

                    if (allocatedPoints === 0) {
                        allocatedPoints = 0.0001;
                    }
                    //finding region of need
                    if (allocatedPoints < min && availablePoints !== 0.0001 ) {
                        min = allocatedPoints;
                        ofNeed = i;
                    }
                    sourceSplice[i] = [];
                    for (var l in fromColumns) {
                        sourceSplice[i].push(this.records[i][fromColumns[l]+"_"+context]/availablePoints);
                    }
                    targetSplice[i] = [];
                    for (var n in toColumns) {
                        targetSplice[i].push(this.records[i][toColumns[n]+"_"+context]/allocatedPoints);
                    }
                    // console.log("Region: "+i)
                    // console.log(availablePoints);
                    // console.log(allocatedPoints);
                    // console.log(sourceSplice);
                    // console.log(targetSplice);    
                }
                var totalSourceSpliceOfNeed = 0;
                for (var s in fromColumns) {
                    totalSourceSpliceOfNeed += this.records[ofNeed][fromColumns[s]+"_"+context];
                }
                
                if (fromColumns.filter((col, f) => { 
                        return (this.records[ofNeed][fromColumns[f]+"_"+context] - sourceSplice[ofNeed][f]*precision) < 0
                    }).length > 0) {
                    // data[ofNeed,toColumns]<-data[ofNeed,toColumns]+sum(data[ofNeed,fromColumns])*sT
                    // data[ofNeed,fromColumns]<-0
                    for (var j in toColumns) {
                        this.records[ofNeed][toColumns[j]+"_"+context] += totalSourceSpliceOfNeed*targetSplice[ofNeed][j];
                    }
                    for (var s in fromColumns) {
                        this.records[ofNeed][fromColumns[s]+"_"+context] = 0.0001;
                    }
                }
                else {
                    // data[ofNeed,toColumns]<-data[ofNeed,toColumns]+precision*sT 
                    // data[ofNeed,fromColumns]<-data[ofNeed,fromColumns]-precision*sF
                    for (var j in toColumns) {
                        this.records[ofNeed][toColumns[j]+"_"+context] += precision*targetSplice[ofNeed][j] ;
                    }
                    for (var k in fromColumns) {
                        this.records[ofNeed][fromColumns[k]+"_"+context] -= precision*sourceSplice[ofNeed][k] ;
                    }
                }
                var targetContextTotal = 0; 
                var contextTotal = 0;
                var contextPop = 0;

                for (var i=0; i<this.records.length; i++) { 
                    var contextPop = (context == 'urb') ? this.records[i]['fraction_urban_pop'] : (1-this.records[i]['fraction_urban_pop']); 
                    contextTotal += contextPop*this.records[i].population;
                    for (j in toColumns) {
                        targetContextTotal += this.records[i][toColumns[j]+"_"+context]*this.records[i].population*contextPop;
                    }
                }
                current = targetContextTotal/contextTotal;
           }
           //console.log(o);
        }
    }

    @action
    moveAllPoints(fromColumns, toColumns, context, filter) {
        
        var targetContextTotal = 0;
        var contextTotal = 0;
        var regionAvailablePoints = [];
        var regionAllocatedPoints = [];
        var sourceSplice = [];
        var targetSplice = [];

        var records = [];
        if (filter) {
            records = (this.selectedAreasDialogValues[this.activeTab].length === 0 || this.selectedAreasDialogValues[this.activeTab].length === this.records.length) ? this.records : this.records.filter( record => this.selectedAreasDialogValues[this.activeTab].includes(record.subarea)); 
          }
          else {
                records = this.records;
        }

        for (var i=0; i<records.length; i++) { 
            
            var availablePoints = 0;
            for (var k in fromColumns) {
                availablePoints += records[i][fromColumns[k]+"_"+context];
            }
            

            if (availablePoints === 0) {
                availablePoints = 0.00001;
            }
            regionAvailablePoints.push(availablePoints);

            var allocatedPoints = 0;
            for (var m in toColumns) {
                allocatedPoints += records[i][toColumns[m]+"_"+context];
            }
            
            if (allocatedPoints === 0) {
                //allocatedPoints = 0.00001;
            }

            regionAllocatedPoints.push(allocatedPoints);
            // targetSplice[i] = [];
            // for (var n in toColumns) {
            //     targetSplice[i].push(records[i][toColumns[n]+"_"+context]/allocatedPoints);
            // }
            for (var n in toColumns) {
                if (allocatedPoints === 0) {
                    records[i][toColumns[n]+"_"+context] = availablePoints/toColumns.length;
                }
                else {
                    records[i][toColumns[n]+"_"+context] += availablePoints*(records[i][toColumns[n]+"_"+context]/allocatedPoints);
                }
            } 
            for (var k in fromColumns) {
                records[i][fromColumns[k]+"_"+context] = 0;
            }
        }

        // for (var i=0; i<records.length; i++) { 
            
        //     for (var j in toColumns) {
        //         records[i][toColumns[j]+"_"+context] += regionAvailablePoints[i]*targetSplice[i][j];
        //     }
        //     for (var k in fromColumns) {
        //         records[i][fromColumns[k]+"_"+context] = 0;
        //     }
        // }
        
    }

    sum(column,decimalRound) {
        return computed(()=> { 
          var total = 0;
          if (this.records != null) {
              for (var i=0; i<this.records.length; i++) {
                  total +=this.records[i][column];
              }
              if (decimalRound) {
                return Math.ceil(total);
              }
              else {
                  return total;
              }
          } 
          return 0;
          }).get();
    }

    selectedSum(column,decimalRound) {
        return computed(()=> { 
          var total = 0;
          var records = (this.selectedAreasDialogValues[this.activeTab].length === 0 || this.selectedAreasDialogValues[this.activeTab].length === this.records.length) ? this.records : this.records.filter( record => this.selectedAreasDialogValues[this.activeTab].includes(record.subarea)); 
          if (records.length > 0) {
              for (var i=0; i<records.length; i++) {
                  total += records[i][column];
              }
              if (decimalRound) {
                return Math.ceil(total);
              }
              else {
                  return total;
              }
          } 
          return 0;
          }).get();
    }

    rowAvg(row, columns, context) {
        var total = 0;    
        var columnAvg = 0;
        for (var j=0; j<columns.length; j++) {
            total += row[columns[j]+"_"+context];
        }

        if (total >= columns.length) {
            columnAvg = 1;
        }
        else {
            columnAvg += total/columns.length;
        }

        return columnAvg;
        
    }

    avg(columns, filter) {
      return computed(()=> { 
        var records = [];
        var sum=0;
        if (filter) {
            records = (this.selectedAreasDialogValues[this.activeTab].length === 0 || this.selectedAreasDialogValues[this.activeTab].length === this.records.length) ? this.records : this.records.filter( record => this.selectedAreasDialogValues[this.activeTab].includes(record.subarea)); 
          }
          else {
                records = this.records;
        }
        if (records != null) {
            
            for (var i=0; i<records.length; i++) {
                var total = 0;    
                var columnAvg = 0;
                for (var j=0; j<columns.length; j++) {
                    total +=records[i][columns[j]];
                }
                if (total >= columns.length) {
                    columnAvg = 1;
                }
                else {
                    columnAvg += total/columns.length;
                }
                
                sum += columnAvg;
            }
            if (sum >= records.length) {
                return 1;
            }
            else {
                return sum/records.length;
            }
        } 
        return 0;
        }).get();
    }

    selectedAvg(column) {
        return computed(()=> { var total = 0;
            if (this.selectedAreasDialogValues[this.activeTab].length > 0) {
              for (var i=0; i<this.selectedAreasDialogValues[this.activeTab].length; i++) {
                  total +=this.records.filter(record => record.subarea === this.selectedAreasDialogValues[this.activeTab][i])[0][column];
              }
              if (total >= this.records.length) {
                  return 1;
              }
              else {
                  return total/this.selectedAreasDialogValues[this.activeTab].length;
              }
          } 
          return 0;
          }).get();
      }

    @action
    increaseSum(column) {
        var records = (this.selectedAreasDialogValues[this.activeTab].length === 0 || this.selectedAreasDialogValues[this.activeTab].length === this.records.length) ? this.records : this.records.filter( record => this.selectedAreasDialogValues[this.activeTab].includes(record.subarea)); 

        for (var i = 0; i < records.length; i++) {
            records[i][column] = Math.round((records[i][column]*((this.percentageAmount*100)+100))/100);
        }
    }

    @action
    increaseAvg(column) {
        if (this.selectedAreasDialogValues[this.activeTab].length === 0 || this.selectedAreasDialogValues[this.activeTab].length === this.records.length) {
            var oldAvg = this.avg([column], false);
            for (var i in this.records) {
                //this.records[i][column] = ((oldAvg + (this.percentageAmount*oldAvg))/oldAvg)*this.records[i][column];
                this.records[i][column] = oldAvg + this.percentageAmount;
            }     
        }
        else {
            for (var i=0; i<this.selectedAreasDialogValues[this.activeTab].length; i++) {
                var oldAvg = this.avg([column], true);
                var record = this.records.filter(record => record.subarea === this.selectedAreasDialogValues[this.activeTab][i])[0];
                //this.records[i][column] = ((oldAvg + (this.percentageAmount*oldAvg))/oldAvg)*this.records[i][column];
                this.records[i][column] = oldAvg + this.percentageAmount;
            }
        }
        
    }

    @action
    decreaseAvg(column) {
        if (this.selectedAreasDialogValues[this.activeTab].length == 0 || this.selectedAreasDialogValues[this.activeTab].length == this.records.length) {
            var oldAvg = this.avg([column], false);
            for (var i in this.records) {
                //this.records[i][column] = ((oldAvg - (this.percentageAmount*oldAvg))/oldAvg)*this.records[i][column];
                this.records[i][column] = oldAvg - this.percentageAmount;
            }     
        }
        else {
            for (var i=0; i<this.selectedAreasDialogValues[this.activeTab].length; i++) {
                var oldAvg = this.avg([column], true);
                var record = this.records.filter(record => record.subarea == this.selectedAreasDialogValues[this.activeTab][i])[0];
                //this.records[i][column] = ((oldAvg - (this.percentageAmount*oldAvg))/oldAvg)*this.records[i][column];
                this.records[i][column] = oldAvg - this.percentageAmount;
            }
        }
    }

    @action
    decreaseSum(column) {
        var records = (this.selectedAreasDialogValues[this.activeTab].length === 0 || this.selectedAreasDialogValues[this.activeTab].length === this.records.length) ? this.records : this.records.filter( record => this.selectedAreasDialogValues[this.activeTab].includes(record.subarea)); 

        for (var i = 0; i < records.length; i++) {
            records[i][column] = Math.round((records[i][column]*(100-(this.percentageAmount*100)))/100);
        }
    }

    @action
    decreaseToiletCategory(category, filter, contextFilter) {
        
        var increment = 0.01;
        var context = contextFilter;
        var records = [];

        if (filter) {
            records = (this.selectedAreasDialogValues[this.activeTab].length === 0 || this.selectedAreasDialogValues[this.activeTab].length === this.records.length) ? this.records : this.records.filter( record => this.selectedAreasDialogValues[this.activeTab].includes(record.subarea)); 
        }
        else {
            records = this.records;
        }
        
        for (var i in records) {
            var increment_urb = 0.01;//increment * records[i]['fraction_urban_pop']*records[i]['population']/records[i]['population'];
            var increment_rur = 0.01;//increment * (1- records[i]['fraction_urban_pop'])*records[i]['population']/records[i]['population'];
            
            var selectedCategory = category;
            var decreasedColumns = toiletColumns[category].decreaseSourceColumns;

            var otherColumns = toiletColumns[selectedCategory].columns.filter( col => !decreasedColumns.includes(col));
            var otherSum_urb = 0;
            var otherSum_rur = 0;
            var value_urb = 0;
            var value_rur = 0;
            
            for (var o=0; o < otherColumns.length; o++) {
                if (context === 'both' || context === 'urb') {
                    value_urb = parseFloat(records[i][otherColumns[o]+'_urb'].toFixed(2));
                    records[i][otherColumns[o]+'_urb'] = value_urb;
                    otherSum_urb += value_urb;
                }
                if (context === 'both' || context === 'rur') {
                    value_rur = parseFloat(records[i][otherColumns[o]+'_rur'].toFixed(2));
                    records[i][otherColumns[o]+'_rur'] = value_rur;
                    otherSum_rur += value_rur;
                }
            }
            var timesMod_urb = decreasedColumns.length; 
            var timesMod_rur = decreasedColumns.length; 
            for (var j=0; j < decreasedColumns.length; j++) {
                var column = decreasedColumns[j];
                if (context === 'both' || context === 'urb') {
                // if ((records[i][column+'_urb'] + otherSum_urb)* records[i]['fraction_urban_pop']*records[i]['population']/records[i]['population'] + increment <= 1) {
                    if (records[i][column+'_urb'] + otherSum_urb + increment <= 1) {
                        var increasedColumns_urb = this.findIncreasedColumnGroup(category, records[i], 'urb', filter, increment_urb);
                        var increasedColumn_urb = (increasedColumns_urb !== -1) ? this.findIncreasedColumn(increasedColumns_urb, records[i], 'urb', increment_urb) : null;

                        if (increasedColumn_urb !== null) {
                            timesMod_urb++;
                            // console.log("Urban "+increasedColumn_urb+": "+records[i][increasedColumn_urb+'_urb'] +', increment: '+increment_urb );
                            this.setRecordColumnValue(records[i], increasedColumn_urb, '_urb', records[i][increasedColumn_urb+'_urb'] + (increment_urb/decreasedColumns.length));
                            this.setRecordColumnValue(records[i], column, '_urb', records[i][column+'_urb'] - increment_urb);
                        }
                    }
                }
                if (context === 'both' || context === 'rur') {
                    // if ((records[i][column+'_rur'] + otherSum_rur)* (1- records[i]['fraction_urban_pop'])*records[i]['population']/records[i]['population'] + increment <= 1) {
                    if (records[i][column+'_rur'] + otherSum_rur + increment <= 1) {
                        var increasedColumns_rur = this.findIncreasedColumnGroup(category, records[i], 'rur', filter, increment_rur);

                        var increasedColumn_rur = (increasedColumns_rur !== -1) ? this.findIncreasedColumn(increasedColumns_rur, records[i], 'rur', increment_rur) : null;
                
                        if (increasedColumn_rur !== null) {
                            // console.log("Rural "+increasedColumn_rur+": "+records[i][increasedColumn_rur+'_urb'] +', increment: '+increment_rur );
                            this.setRecordColumnValue(records[i], increasedColumn_rur, '_rur', records[i][increasedColumn_rur+'_rur'] + (increment_rur/decreasedColumns.length));
                            this.setRecordColumnValue(records[i], column, '_rur', records[i][column+'_rur'] - increment_rur);

                        }
                    } 
                }
            }
        }     
    }

    @action
    increaseToiletCategory(category, filter, contextFilter) {
        
        var increment = 0.01;
        var context = contextFilter;
        var records = [];

        if (filter) {
            records = (this.selectedAreasDialogValues[this.activeTab].length === 0 || this.selectedAreasDialogValues[this.activeTab].length === this.records.length) ? this.records : this.records.filter( record => this.selectedAreasDialogValues[this.activeTab].includes(record.subarea)); 
        }
        else {
            records = this.records;
        }
        
        for (var i in records) {
            var increment_urb = 0.01;//increment * records[i]['fraction_urban_pop']*records[i]['population']/records[i]['population'];
            var increment_rur = 0.01;//increment * (1- records[i]['fraction_urban_pop'])*records[i]['population']/records[i]['population'];
            
            var selectedCategory = category;
            var increasedColumns = toiletColumns[category].increaseTargetColumns;

            var otherColumns = toiletColumns[selectedCategory].columns.filter( col => !increasedColumns.includes(col));
            var otherSum_urb = 0;
            var otherSum_rur = 0;
            var value_urb = 0;
            var value_rur = 0;
            
            for (var o=0; o < otherColumns.length; o++) {
                if (context === 'both' || context === 'urb') {
                    value_urb = parseFloat(records[i][otherColumns[o]+'_urb'].toFixed(2));
                    records[i][otherColumns[o]+'_urb'] = value_urb;
                    otherSum_urb += value_urb;
                }
                if (context === 'both' || context === 'rur') {
                    value_rur = parseFloat(records[i][otherColumns[o]+'_rur'].toFixed(2));
                    records[i][otherColumns[o]+'_rur'] = value_rur;
                    otherSum_rur += value_rur;
                }
            }

            for (var j=0; j < increasedColumns.length; j++) {
                var column = increasedColumns[j];
                if (context === 'both' || context === 'urb') {
                // if ((records[i][column+'_urb'] + otherSum_urb)* records[i]['fraction_urban_pop']*records[i]['population']/records[i]['population'] + increment <= 1) {
                    if (records[i][column+'_urb'] + otherSum_urb + increment <= 1) {
                        var decreasedColumns_urb = this.findDecreasedColumnGroup(category, records[i], 'urb', filter, increment_urb);
                        var decreasedColumn_urb = (decreasedColumns_urb !== -1) ? this.findDecreasedColumn(decreasedColumns_urb, records[i], 'urb', increment_urb) : null;

                        if (decreasedColumn_urb !== null) {
                            // console.log("Urban "+decreasedColumn_urb+": "+records[i][decreasedColumn_urb+'_urb'] +', increment: '+increment_urb );
                            this.setRecordColumnValue(records[i], decreasedColumn_urb, '_urb', records[i][decreasedColumn_urb+'_urb'] - increment_urb);
                            this.setRecordColumnValue(records[i], column, '_urb', records[i][column+'_urb'] + increment_urb);
                        }
                    }
                }
                if (context === 'both' || context === 'rur') {
                    // if ((records[i][column+'_rur'] + otherSum_rur)* (1- records[i]['fraction_urban_pop'])*records[i]['population']/records[i]['population'] + increment <= 1) {
                    if (records[i][column+'_rur'] + otherSum_rur + increment <= 1) {
                        var decreasedColumns_rur = this.findDecreasedColumnGroup(category, records[i], 'rur', filter, increment_rur);

                        var decreasedColumn_rur = (decreasedColumns_rur !== -1) ? this.findDecreasedColumn(decreasedColumns_rur, records[i], 'rur', increment_rur) : null;
                
                        if (decreasedColumn_rur !== null) {
                            // console.log("Rural "+decreasedColumn_rur+": "+records[i][decreasedColumn_rur+'_rur']+', increment: '+increment_rur);
                            this.setRecordColumnValue(records[i], decreasedColumn_rur, '_rur', records[i][decreasedColumn_rur+'_rur'] - increment_rur);
                            this.setRecordColumnValue(records[i], column, '_rur', records[i][column+'_rur'] + increment_rur);

                        }
                    } 
                }
            }
        }     
    }

    @action
    movePoints(from, to, filter, contextFilter) {
        
        var increment = 0.01;
        var context = contextFilter;
        var records = [];

        if (filter) {
            records = (this.selectedAreasDialogValues[this.activeTab].length === 0 || this.selectedAreasDialogValues[this.activeTab].length === this.records.length) ? this.records : this.records.filter( record => this.selectedAreasDialogValues[this.activeTab].includes(record.subarea)); 
        }
        else {
            records = this.records;
        }
        
        for (var i in records) {
            var increment_urb = increment;//increment * records[i]['fraction_urban_pop']*records[i]['population']/records[i]['population'];
            var increment_rur = increment;//increment * (1- records[i]['fraction_urban_pop'])*records[i]['population']/records[i]['population'];
            
            
            var increasedColumns = toiletColumns[to].increaseTargetColumns;
            var decreasedColumns = toiletColumns[from].decreaseSourceColumns;
            var otherColumns = toiletColumns[to].columns.filter( col => !increasedColumns.includes(col));
            var otherSum_urb = 0;
            var otherSum_rur = 0;
            var value_urb = 0;
            var value_rur = 0;
            
            for (var o=0; o < otherColumns.length; o++) {
                if (context === 'both' || context === 'urb') {
                    value_urb = parseFloat(records[i][otherColumns[o]+'_urb'].toFixed(2));
                    records[i][otherColumns[o]+'_urb'] = value_urb;
                    otherSum_urb += value_urb;
                }
                if (context === 'both' || context === 'rur') {
                    value_rur = parseFloat(records[i][otherColumns[o]+'_rur'].toFixed(2));
                    records[i][otherColumns[o]+'_rur'] = value_rur;
                    otherSum_rur += value_rur;
                }
            }
            
            for (var j=0; j < increasedColumns.length; j++) {
                var column = increasedColumns[j];
                
                if (context === 'both' || context === 'urb') {
                    
                    //check with other columns in the increased group, not just others
                    if (records[i][column+'_urb'] + otherSum_urb + increment <= 1) {
                        //var decreasedColumns_urb = this.findDecreasedColumnGroup(category, records[i], 'urb', filter, increment_urb);
                        
                        var decreasedColumn_urb = this.findDecreasedColumn(decreasedColumns, records[i], 'urb', increment_urb);
                        //console.log(decreasedColumn_urb);

                        if (decreasedColumn_urb !== null && records[i][decreasedColumn_urb+'_urb'] > 0) {
                            // console.log("Urban "+decreasedColumn_urb+": "+records[i][decreasedColumn_urb+'_urb'] +', increment: '+increment_urb );
                            this.setRecordColumnValue(records[i], decreasedColumn_urb, '_urb', records[i][decreasedColumn_urb+'_urb'] - increment_urb);
                            this.setRecordColumnValue(records[i], column, '_urb', records[i][column+'_urb'] + increment_urb);
                        }
                        else {
                            var otherDecreased = toiletColumns[from].columns.filter( col => {return !decreasedColumns.includes(col)}); 
                            if (this.rowAvg(records[i], otherDecreased, 'urb') >= 0) {
                                for (var k in otherDecreased) {
                                    if ( records[i][column+'_urb'] + records[i][otherDecreased+'_urb'] <= 1) {
                                        this.setRecordColumnValue(records[i], otherDecreased[k], '_urb', 0);
                                        this.setRecordColumnValue(records[i], column, '_urb', records[i][column+'_urb'] + records[i][otherDecreased+'_urb']);
                                    }     
                                }
                            }
                        }
                    }
                }
                
                if (context === 'both' || context === 'rur') {
                    // if ((records[i][column+'_rur'] + otherSum_rur)* (1- records[i]['fraction_urban_pop'])*records[i]['population']/records[i]['population'] + increment <= 1) {
                    if (Math.round((records[i][column+'_rur'] + otherSum_rur) * 100) / 100 + increment <= 1) {
                        //var decreasedColumns_rur = this.findDecreasedColumnGroup(category, records[i], 'rur', filter, increment_rur);

                        var decreasedColumn_rur = this.findDecreasedColumn(decreasedColumns, records[i], 'rur', increment_rur);
                        //console.log(decreasedColumn_rur);

                
                        if (decreasedColumn_rur !== null && records[i][decreasedColumn_rur+'_rur'] > 0) {
                            // console.log("Rural "+decreasedColumn_rur+": "+records[i][decreasedColumn_rur+'_rur']+', increment: '+increment_rur);
                            this.setRecordColumnValue(records[i], decreasedColumn_rur, '_rur', records[i][decreasedColumn_rur+'_rur'] - increment_rur);
                            this.setRecordColumnValue(records[i], column, '_rur', records[i][column+'_rur'] + increment_rur);
                        }
                        else {
                            var otherDecreased = toiletColumns[from].columns.filter( col => {return !decreasedColumns.includes(col)}); 
                            
                            if (this.rowAvg(records[i], otherDecreased, 'rur') >= 0) {
                                for (var k in otherDecreased) {
                                    if ( records[i][column+'_rur'] + records[i][otherDecreased+'_rur'] <= 1) {
                                        this.setRecordColumnValue(records[i], otherDecreased[k], '_rur', 0);
                                        this.setRecordColumnValue(records[i], column, '_rur', records[i][column+'_rur'] + records[i][otherDecreased+'_rur']);
                                    }
                                }
                            }
                        }
                    } 
                }
            }
        }     
    }

    

    @action setRecordColumnValue( record, column, context, value) {
        record[column+context] = (value < 0) ? 0.0 : value ;
        //record[column+context] = value;
    }

    @action findIncreasedColumnGroup(selectedCategory, record, context, filter, threshold) {
        var increasedColumns = [];
        var categoryFoundBackward = false;
        var categoryFoundForward = false;
        var initialCategory = selectedCategory;
        while (selectedCategory < toiletColumns.length-1 && !categoryFoundForward) {
            selectedCategory++;
            increasedColumns = toiletColumns[selectedCategory].increaseTargetColumns;

            // if (this.populationWeightedAvg(decreasedColumns, context, filter, record)/100 >= threshold/decreasedColumns.length) {

            if (this.rowAvg(record, increasedColumns, context) <= 1-threshold) {
                categoryFoundForward = true;
            }
        }
        if (!categoryFoundForward) {
            selectedCategory = initialCategory;
            while (selectedCategory > 0 && !categoryFoundBackward) {
                selectedCategory--;
                increasedColumns = toiletColumns[selectedCategory].increaseTargetColumns;
                // if (this.populationWeightedAvg(decreasedColumns, context, filter, record)/100 >= threshold/decreasedColumns.length) {
                if (this.rowAvg(record, increasedColumns, context) <= 1-threshold) {
                    categoryFoundBackward = true;
                }
            }
            
        }
        return (categoryFoundForward || categoryFoundBackward) ? increasedColumns : -1;
    }

    @action findIncreasedColumn (increasedColumns, record, context, threshold) {
        var i = 0;
        var increasedColumn = increasedColumns[i];
        while ( i < increasedColumns.length && record[increasedColumn+'_'+context] < 1-threshold) {
            increasedColumn = increasedColumns[i];
            i++;
        }
        //console.log(context+': '+decreasedColumn);
        return increasedColumn;
    }

    @action findDecreasedColumnGroup(selectedCategory, record, context, filter, threshold) {
        var decreasedColumns = [];
        var categoryFoundBackward = false;
        var categoryFoundForward = false;
        var initialCategory = selectedCategory;
        while (selectedCategory > 0 && !categoryFoundBackward) {
            selectedCategory--;
            decreasedColumns = toiletColumns[selectedCategory].decreaseSourceColumns;
            // if (this.populationWeightedAvg(decreasedColumns, context, filter, record)/100 >= threshold/decreasedColumns.length) {
            if (this.rowAvg(record, decreasedColumns, context) >= threshold/decreasedColumns.length) {
                categoryFoundBackward = true;
            }
        }
        if (!categoryFoundBackward) {
            selectedCategory = initialCategory;
            while (selectedCategory < toiletColumns.length-1 && !categoryFoundForward) {
                selectedCategory++;
                decreasedColumns = toiletColumns[selectedCategory].decreaseSourceColumns;

                // if (this.populationWeightedAvg(decreasedColumns, context, filter, record)/100 >= threshold/decreasedColumns.length) {

                if (this.rowAvg(record, decreasedColumns, context) >= threshold/decreasedColumns.length) {
                    categoryFoundForward = true;
                }
            }
        }
        return (categoryFoundForward || categoryFoundBackward) ? decreasedColumns : -1;
    }

    @action findDecreasedColumn (decreasedColumns, record, context, threshold) {
        var i = decreasedColumns.length;
        var decreasedColumn = decreasedColumns[i-1];
        //while ( i > 0 && this.populationWeightedAvg([decreasedColumn], context, filter, record)/100 <= threshold) {
        while ( i > 0 && record[decreasedColumn+'_'+context] < threshold ) {
            i--;
            decreasedColumn = decreasedColumns[i];
        }
        //console.log(context+': '+decreasedColumn);
        return decreasedColumn;
    }

    populationWeightedAvg(columns, context, filter, regionFilter) {
        return computed(()=> {
        
          var records = [];
          if (filter) {
            records = (this.selectedAreasDialogValues[this.activeTab].length === 0 || this.selectedAreasDialogValues[this.activeTab].length === this.records.length) ? this.records : this.records.filter( record => this.selectedAreasDialogValues[this.activeTab].includes(record.subarea)); 
          }
          else {
            if (regionFilter.length > 0) {
                records = regionFilter;
            }
            else {
                records = this.records;
            }
          }
          if (records != null) {
              var total = 0;
              for (var i=0; i < records.length; i++) {
                  var urbanTotal = 0;
                  var ruralTotal = 0;
                  for (var j in columns) {
                    if (context == "urb" || context == "both") {
                        urbanTotal += records[i][columns[j]+"_urb"];
                    }
                    if (context =="rur" || context == "both") {
                        ruralTotal += records[i][columns[j]+"_rur"];
                    }
                    if (context == "none") {
                        urbanTotal += records[i][columns[j]];
                    }
                  }
                  if (context=="both") {
                    total = total + (urbanTotal*records[i]['population']*records[i]['fraction_urban_pop'])+(ruralTotal*records[i]['population']*(1-records[i]['fraction_urban_pop']));
                  }
                  else if (context == "urb") {
                    total = total + (urbanTotal*records[i]['population']*records[i]['fraction_urban_pop']);
                  }
                  else if (context == "rur") {
                    total = total + (ruralTotal*records[i]['population']*(1-records[i]['fraction_urban_pop']));
                  }
                  else {
                    total = total + (urbanTotal*records[i]['population']);
                  }

                }
              return (filter) ? Math.round(total*100)/this.selectedSum("population", false):Math.round(total*100)/this.sum("population");
          } 
          return 0;
          }).get();
      }

    //   difference(column) {
    //     return computed(()=> { 
          
    //       return 0;
    //       }).get();
    // }

      @action setSewageSlider(e) {
        var records = (this.selectedAreasDialogValues[this.activeTab].length === 0 || this.selectedAreasDialogValues[this.activeTab].length === this.records.length) ? this.records : this.records.filter( record => this.selectedAreasDialogValues[this.activeTab].includes(record.subarea)); 

        for (var i=0; i<records.length; i++) {
            records[i]["sewageTreated_urb"] = e/100;
            records[i]["sewageTreated_rur"] = e/100;
        }
      }

    @action setFaecalSlider(e) {
        var valueToBe = e/100;
        var records = (this.selectedAreasDialogValues[this.activeTab].length === 0 || this.selectedAreasDialogValues[this.activeTab].length === this.records.length) ? this.records : this.records.filter( record => this.selectedAreasDialogValues[this.activeTab].includes(record.subarea)); 

        for (var i=0; i<records.length; i++) {   
            //if (valueToBe <= 1 - records[i]["fecalSludgeTreated_rur"] && valueToBe <= 1 - records[i]["coverBury_urb"]) { 
            records[i]["fecalSludgeTreated_urb"] = valueToBe ;
            records[i]["coverBury_rur"] = valueToBe ;  
            records[i]["fecalSludgeTreated_rur"] = 1 - valueToBe ;
            records[i]["coverBury_urb"] = 1 - valueToBe; 
            //}
        }   
    }

    // @computed get
    // getFaecalColumnsPercentage() {
    //     return this.avg(["fecalSludgeTreated_urb"]) + this.avg(["coverBury_rur"]);
    // }

    @action logChange(value, pathogen) {
        var records = (this.selectedAreasDialogValues[this.activeTab].length === 0 || this.selectedAreasDialogValues[this.activeTab].length === this.records.length) ? this.records : this.records.filter( record => this.selectedAreasDialogValues[this.activeTab].includes(record.subarea)); 

        // var oldValue = this.avg(['fRemoval_treatment']) === 0 ? 0.0001: this.avg(['fRemoval_treatment']);
        var newValue = 1 - Math.pow(10, - value);
        for (var i=0; i<records.length; i++) {   
            // records[i]['fRemoval_treatment'] = this.records[i]['fRemoval_treatment']*newValue/oldValue;
            records[i]['fRemoval_treatment'] = newValue;
            var fLiquid = 0;
            if (pathogen === "Virus") {
                if (records[i]['fRemoval_treatment'] < 0.85) {
                    fLiquid = 0.97;
                }
                else if (records[i]['fRemoval_treatment'] < 0.97){
                    fLiquid = 0.5;
                }
                else {
                    fLiquid = 0.4;
                }
            }
            else if (pathogen === "Protozoa") {
                if (records[i]['fRemoval_treatment'] < 0.7) {
                    fLiquid = 0.85;
                }
                else {
                    fLiquid = 0.25;
                }
            }
            records[i]['fEmitted_inEffluent_after_treatment'] = fLiquid - (fLiquid*records[i]['fRemoval_treatment']);
        }
        
    }

    @action setActiveTab(event,tab) {
        this.activeTab = tab;
    }

    @action setGeoTiff(url) {
        this.geoTiff = url;
    }

    @action setPng(url) {
        this.png = url;
    }

    @action setPercentageAmount(value) {
        this.percentageAmount = value;
    }

    @action setMinValue(value) {
        this.minValue = value;
    }

    @action setMaxValue(value) {
        this.maxValue = value;
    }

    @action setSelectedAreasDialogTab(tab) {
        this.selectedAreasDialogTab = tab;
    }

    @action setSelectedAreasDialogValuesAll() {
        this.selectedAreasDialogValues[this.activeTab]=[];
        for (var i in this.records) {
                this.selectedAreasDialogValues[this.activeTab].push(this.records[i].subarea);
        }
    }

    @action handleSelectedAreasDialogValue(subarea, checked) {
        if (checked) {
            this.selectedAreasDialogValues[this.activeTab].push(subarea);
        }
        else {
            if (this.selectedAreasDialogValues[this.activeTab].length == 0) {
                for (var i in this.records) {
                    if (this.records[i].subarea != subarea) {
                        this.selectedAreasDialogValues[this.activeTab].push(this.records[i].subarea);
                    }
                }
            }
            else {
                const index = this.selectedAreasDialogValues[this.activeTab].indexOf(subarea);
                if (index > -1) {
                    this.selectedAreasDialogValues[this.activeTab].splice(index, 1);
                }
            }
        }
    }

    @action setActiveToiletTab(event,tab) {
        this.activeToiletTab = tab;
    }

    @action setPreviousToiletFractions(fractionsArray) {
        this.toiletFractionHandles = fractionsArray;
        this.toiletFractionsPrev = [ 
            fractionsArray[0], 
            fractionsArray[1]-fractionsArray[0],
            fractionsArray[2]-fractionsArray[1],
            fractionsArray[3]-fractionsArray[2],
            100-fractionsArray[3]
        ];
    }

    @action setToiletFractions(fractionsArray) {
        var newFractions = [ 
            fractionsArray[0], 
            fractionsArray[1]-fractionsArray[0],
            fractionsArray[2]-fractionsArray[1],
            fractionsArray[3]-fractionsArray[2],
            100-fractionsArray[3]
        ];
        var increased = -1;
        var decreased = -1;
        var difference = 0;

        var records = (this.selectedAreasDialogValues[this.activeTab].length === 0 || this.selectedAreasDialogValues[this.activeTab].length === this.records.length) ? this.records : this.records.filter( record => this.selectedAreasDialogValues[this.activeTab].includes(record.subarea)); 

        for (var i = 0; i < newFractions.length; i++) {
            if (newFractions[i] > this.toiletFractionsPrev[i]) {
                increased = i;
                difference = newFractions[i] - this.toiletFractionsPrev[i];
            }
            if (newFractions[i] < this.toiletFractionsPrev[i]) {
                decreased = i;
            } 
        }

        this.adjustPoints(increased, decreased, newFractions[increased]/100, newFractions[decreased]/100, records);
        
        // for (var i in fractionsArray) {
        //     // if (fractionsArray[i] > this.toiletFractionsPrev[i]) {
        //     //     this.movePercentagePoints(toiletColumns[i].category, "urb", 0.01);
        //     // }
        //     var columns = toiletColumns[i].columns;
        //     var newValue = (fractionsArray[i] === 0) ? 0.00001 : fractionsArray[i];
        //     var multiplicationFactor = newValue/oldValue;
            
        //     this.toiletFractions[i] = Math.round(fractionsArray[i]);
        // }
        this.toiletFractions = newFractions;
    }

    @action adjustPoints(increased, decreased, increasedValue, decreasedValue, records) {
        if (increased !== -1) {
            for ( var i = 0; i < records.length; i++ ) {
                console.log(records[i].subarea);
                console.log(toiletColumns[increased].title);
                console.log(toiletColumns[decreased].title);
                console.log(increasedValue);
                console.log(decreasedValue);

                

                    var increased_target_avg = 0;
                    var increased_other_avg = 0;
                    var decreased_target_avg = 0;
                    var decreased_other_avg = 0;
                    
                    do {
                        for (var j = 0; j < toiletColumns[increased].increaseTargetColumns.length; j++) {
                            var column = toiletColumns[increased].increaseTargetColumns[j];
                            if (parseFloat(records[i][column+'_urb']) + 0.001 <= 1 ) {
                                records[i][column+'_urb'] = parseFloat(records[i][column+'_urb']) + 0.001;
                            }
                            
                            if (parseFloat(records[i][column+'_rur']) + 0.001 <= 1 ) {
                                records[i][column+'_rur'] = parseFloat(records[i][column+'_rur']) + 0.001;
                            }
                        }
                        increased_target_avg = this.populationWeightedAvg(toiletColumns[increased].increaseTargetColumns, "both", false, records[i])/100;
                        increased_other_avg = this.populationWeightedAvg(toiletColumns[increased].columns.filter(col => !toiletColumns[increased].increaseTargetColumns.includes(col)), "both", false, records[i])/100;
                        
                    } while (increased_target_avg + increased_other_avg < increasedValue);
                    
                    var decreasedColumn_urb = toiletColumns[decreased].decreaseSourceColumns.length - 1;
                    var decreasedColumn_rur = toiletColumns[decreased].decreaseSourceColumns.length - 1;
                    do {
                       
                        if (decreasedColumn_urb > -1) {
                            var column_urb = toiletColumns[decreased].decreaseSourceColumns[decreasedColumn_urb];
                            if (parseFloat(records[i][column_urb+'_urb']) - 0.001 >= 0.000) {
                                records[i][column_urb+'_urb'] = parseFloat(records[i][column_urb+'_urb']) - 0.001;
                            }
                            else {
                                decreasedColumn_urb--;
                            }
                        }
                        if (decreasedColumn_rur > -1) {
                            var column_rur = toiletColumns[decreased].decreaseSourceColumns[decreasedColumn_rur];
                            if (parseFloat(records[i][column_rur+'_rur']) - 0.001 >= 0.000) {
                                records[i][column_rur+'_rur'] = parseFloat(records[i][column_rur+'_rur']) - 0.001;
                            }
                            else {
                                decreasedColumn_rur--;
                            }
                        }
                        decreased_target_avg = this.populationWeightedAvg(toiletColumns[decreased].decreaseSourceColumns, "both", false, records[i])/100;
                        decreased_other_avg = this.populationWeightedAvg(toiletColumns[decreased].columns.filter(col => !toiletColumns[decreased].decreaseSourceColumns.includes(col)), "both", false, records[i])/100;
                    
                    } while (decreased_target_avg + decreased_other_avg > decreasedValue);
                    // && (decreasedColumn_rur !== -1 && decreasedColumn_urb !== -1)
                    
                    // if (decreased_target_avg + decreased_other_avg > decreasedValue) {
                    //     decreasedValue = (decreased_target_avg - decreased_other_avg) - decreasedValue;
                    // // for ( var j = 0; j < toiletColumns[increased].columns.length; j++ ) {
                    // //     var column = toiletColumns[increased].columns[j];
                    // //     if ( toiletColumns[increased].increaseTargetColumns.includes(column)) {
                    // //         mod = ( j%2===0 ) ? mod: -mod;
                    //     if (decreasedColumn_rur === -1) {
                    //        var points = decreasedValue / toiletColumns[decreased].decreaseSourceColumns.length;
                    //        var mod = ((points*100) % 1)/100;
                    //        for (var x = 0; x < toiletColumns[decreased].decreaseSourceColumns.length; x++ ) {
                    //             var column = toiletColumns[decreased].decreaseSourceColumns[x];
                    //             records[i][column+'_urb'] = parseFloat((points+mod).toFixed(2));
                    //             mod = ( x%2===0 ) ? mod: -mod;
                    //        }
                            
                    //     }
                    //     else {
                    //         // alert('Urb depleted');
                    //         var points = decreasedValue / toiletColumns[decreased].decreaseSourceColumns.length;
                    //        var mod = ((points*100) % 1)/100;
                    //        for (var y = 0; y < toiletColumns[decreased].decreaseSourceColumns.length; y++ ) {
                    //             var column = toiletColumns[decreased].decreaseSourceColumns[y];
                    //             records[i][column+'_rur'] = parseFloat((points+mod).toFixed(2));
                    //             mod = ( y%2===0 ) ? mod: -mod;
                    //        }
                    //     }
                    //}
            }
        }
    }

    @action setTreatmentFractions(fractionsArray) {
        this.treatmentFractions = fractionsArray;
    }
    
    @action newRecords(records) {
        this.records = records;
        if (this.gridApi !== null) {
            this.gridApi.refreshCells();
        }
    }

    @action copyRecords() {
        this.recordsCopy = toJS(this.records);
    }

    @action revert() {
        this.records = toJS(this.recordsCopy);
        this.initGridOptions();
        this.setDataFunctions([]);
        //this.setToiletFractions(this.initialToiletFractions);
        this.selectedAreasDialogValues[0] = [];
        this.selectedAreasDialogValues[1] = [];
        this.selectedAreasDialogValues[2] = [];
        if (this.gridApi !== null) {
            this.gridApi.refreshCells();
        }
    }

    @action pushRecords(newRecords) {
        this.records.push(newRecords)
    }  

    @action setSelectedContext(value) {
        this.selectedContext = value;
        
    } 

    @action setFromToilet(category) {
        this.fromToilet = category;
    }

    @action setToToilet(category) {
        this.toToilet = category;
    }

    @action setDataFunctions(options) {
        
        this.dataFunctions = options;
        
        // for (var f in this.dataFunctions) {
        //     var fun = functions.filter(fu => fu.id==this.dataFunctions[f])[0];
        //     if (fun.value)
        // }
    }

    @action applyFunction(fun, checked) {
        var records = (this.selectedAreasDialogValues[this.activeTab].length === 0 || this.selectedAreasDialogValues[this.activeTab].length === this.records.length) ? this.records : this.records.filter( record => this.selectedAreasDialogValues[this.activeTab].includes(record.subarea)); 
        if (checked) {
            if (fun.valueChecked !== null) {
                for (var i in records) {
                    for (var j in fun.columns) {
                        records[i][fun.columns[j]] = fun.valueChecked;
                    }
                }
            }
            else {
                if (fun["fromColumns"]) {
                    this.movePercentagePoints(fun["fromColumns"], fun["toColumns"], "urb", 0.0001, -1);
                    this.movePercentagePoints(fun["fromColumns"], fun["toColumns"], "rur", 0.0001, -1);
                }
            }
        }
        else {
            if (fun.valueUnchecked !== null) {
                for (var i in records) {
                    for (var j in fun.columns) {
                        records[i][fun.columns[j]] = fun.valueUnchecked;
                    }
                }
            }
            else {
                if (fun["fromColumns"]) {
                    this.movePercentagePoints(fun["toColumns"], fun["fromColumns"], "urb", 0.0001, -1);
                    this.movePercentagePoints(fun["toColumns"], fun["fromColumns"], "rur", 0.0001, -1);
                }
            }
        }
    }

    @action initGridOptions() {
        var scenario = this;
        //Columns Object
        var headers = new DataHeaders();
        const columnDefs = headers.analysisEditorfields;
        //Rows Obj
        scenario.gridOptions = {
        columnDefs: columnDefs,
        defaultColDef: {
            editable: false
        },
        rowData: scenario.records,
            onCellEditingStopped: function(event) {
                // console.log(toJS(event.data));
                scenario.newRecords(event.data);
                // console.log(toJS(scenario.records));
            }
        }
    }

    @action onGridReady (params)  {
        this.gridApi = params.api;
    };

    @action setOutputRecords (res)  {
        this.outputRecords = res;
    };

    @action downloadAsCSV() {
        const params = {
            suppressQuotes: false,
            columnSeparator: ";"
        }
        if (this.gridApi !== null) {
            this.gridApi.exportDataAsCsv(params);
        }
    }

    
}
export default ScenarioModel;
