import React, { ReactDOMServer } from 'react';
import { Map, GeoJSON, TileLayer, withLeaflet } from 'react-leaflet';
import { PlottyGeotiffLayer } from "../components/GeoTiff";
import countries from "../config/countries.json";
import Grid from '@material-ui/core/Grid';
import AreaPopup from "../components/legends/AreaPopup";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import HeaderBar from "../components/HeaderBar";
import { observer } from "mobx-react-lite";
import theme from "../config/Theme.js";
import { ThemeProvider, makeStyles } from "@material-ui/core/styles";
import FullscreenControl from 'react-leaflet-fullscreen';
import 'react-leaflet-fullscreen/dist/styles.css';
import ColorLegend from "../components/legends/ColorLegend";
import DisclaimerModal from '../components/dialogs/DisclaimerModal';
import {BounceLoader} from 'react-spinners';
import "leaflet-geotiff";
import "leaflet-geotiff/leaflet-geotiff-plotty";
import PrintControlDefault from 'react-leaflet-easyprint';
import { GeoJSONFillable } from 'react-leaflet-geojson-patterns';
import GeoanalyticsModel from '../models/GeoanalyticsModel';
import CountryLegend from '../components/legends/CountryLegend';
import InfoLegend from '../components/legends/InfoLegend';
import GlobalSidebar from '../components/legends/GlobalSidebar';
import InfoModal from '../components/dialogs/InfoModal';
import skins from '../config/skins.json'

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}


const PrintControl = withLeaflet(PrintControlDefault);

const GlobalView = observer(({ context }) => {

  const mapModel = context;
  const geoanalyticsModel = new GeoanalyticsModel();
  

  const position = [1.3733, 32.2903];

  const getStyle = (feature, layer) => {
    var worldDataMatch = mapModel.worldData.filter(record => { return record.gid===feature.id });
    
    // var countryLevelData = (worldDataMatch.length > 0) ? (worldDataMatch[0]['notes1'] === ''): false ;
    
    // var highResolutionData = mapModel.hasHighResolutionData(feature.id);
    var color = '';
    color = 'rgba(0, 0, 0, 0)';
    return {
      color: '#003347',
      weight: 1,
      fillColor: color,
      fillOpacity: 0.3
    }
  }

  var lastClickedCountry;

  const countryFeatureBehavior = (feature, layer) => {
    layer.on('mouseover', function () {
      this.setStyle({
        'fillColor': '#fff',
        'fillOpacity': 0.7
      });
    });
    layer.on('mouseout', function () {
          this.setStyle({
            'fillColor': 'rgba(0,0,0,0)',
            'fillOpacity': 0.3
          });
    });

    layer.on('click', function (e) {
      
      mapModel.countriesRef.current.leafletElement.resetStyle(lastClickedCountry);
      this.setStyle({
        'weight': 2
      });
      var layer = e.target;
      
      var worldDataMatch = mapModel.worldData.filter(record => { return record.gid === layer.feature.id });
      var countryLevelData = (worldDataMatch.length > 0) ? (worldDataMatch[0]['notes1'] === ''): false ;
      if (countryLevelData) {
        geoanalyticsModel.setCountry(worldDataMatch[0]);
        geoanalyticsModel.setGeometry(layer.feature.geometry);
        geoanalyticsModel.fetchStats();
        mapModel.eraseScenarios();
        mapModel.clearSelectedAreas();
        mapModel.addArea(layer.feature.id);
        mapModel.addAreaToDictionary(layer.feature.id, layer.feature.properties.name);
        mapModel.addScenario(null);
        mapModel.scenarios[0].payload.setOutputRecords(mapModel.worldOutput.filter(record => record.subarea === worldDataMatch[0].subarea));
      }
      else {
        geoanalyticsModel.setCountry(layer.feature.properties.name);
        geoanalyticsModel.setSidebarSelected('home');
      }
      lastClickedCountry = layer;
    });
  }

  const printOptions = {
    position: 'topright',
    sizeModes: ['A4Portrait', 'A4Landscape'],
    hideControlContainer: false
  };

  return (
    <ThemeProvider theme={theme()}>



      <Grid container style={{ height: '100vh' }}>
        <HeaderBar context={mapModel}></HeaderBar>
  <Grid item xs={12} md={12} lg={12} style={{ height: '64px' }}></Grid>

        <Grid item xs={12} 
           
          style={{ height: '100%', backgroundColor: (mapModel.worldData.length > 0) ? "#fff":"#eee",
          transition: "0.5s"
            // transition: theme.transitions.create("all", {
            // easing: theme.transitions.easing.sharp, 
            // duration: theme.transitions.duration.leavingScreen,
            // })
        }}>
          {(mapModel.worldData.length > 0 && !mapModel.animationRendering) ? [
          <GlobalSidebar context={geoanalyticsModel} map={mapModel}/>,
          <Map
            center={position}
            style={{ height: '100%', width:'100%' }}
            zoom={2}
            ref={mapModel.mapRef}
            maxBounds={mapModel.mapBounds}
            minZoom={2}
            key={Math.random()}
            className={(mapModel.scenarioMode ? 'scenarios' : '')}
            whenReady={() => {
              geoanalyticsModel.initGeoTiff(mapModel.worldTiff);
              
              mapModel.toggleInfoMapDialogState(false);
              mapModel.toggleColorLegendState(false);
              mapModel.toggleColorLegendState(true);
              geoanalyticsModel.toggleCountryDialogState(false);
              // if (geoanalyticsModel.geometry !== null ) {
              //   geoanalyticsModel.toggleCountryDialogState(true);
              // }
              mapModel.setMultiCountryMode(true);
              mapModel.setPathogen('Virus');
            }}
          >
            
            
            
            <ColorLegend model={mapModel} position={'bottomright'} anchor={[-200,-150]} />
            
            
            <CountryLegend context={geoanalyticsModel} />
            <InfoLegend map={mapModel}/>
            
            <FullscreenControl position="topright" />
            <PrintControl ref={(ref) => { mapModel.setPrintControl(ref) }} {...printOptions} />
            {skins.filter((skin) => skin.name === geoanalyticsModel.skin).map((skin) => <TileLayer
              url={skin.url}
              attribution={skin.attribution}
            />)}
              
            

            {(mapModel.worldTiff) &&
              <PlottyGeotiffLayer
                url={mapModel.worldTiff}
                options={{
                  displayMin: 0,//4.68986,
                  displayMax: 16.6,
                  name: "Pathogen Emissions",
                  colorScale: "rainbow",
                  opacity: 0.3,
                  clampLow: false,
                  clampHigh: false
                }}
              />
            }


           
              <GeoJSONFillable
                data={countries}
                style={getStyle}
                onEachFeature={countryFeatureBehavior}
                ref={mapModel.countriesRef}
                onClick={(e) => {
                  
                  // geoanalyticsModel.toggleCountryDialogState(true);
                  //mapModel.addPopup(e)
                }}
              >
                        
                          <AreaPopup context={mapModel}/>
              
              </GeoJSONFillable>

            
            
            {(mapModel.mapLoading || mapModel.loadingTiff) &&
              <div className="loading-overlay">
                <div className="loading-overlay-content">
                  <BounceLoader color="#fff"/>
                  {(mapModel.mapLoading) ? <h5>Loading high resolution data...</h5>:<h5>Running model...</h5>}
                </div>
              </div>
            }


          </Map>]:
          <div style={{padding: '20px'}}>
          </div>
          }

          
        </Grid>

      </Grid>
      
      <DisclaimerModal context={mapModel} />
      <InfoModal context={mapModel} />
      
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        open={mapModel.errorMessages.length > 0}
      >
        <Alert onClose={() => { mapModel.clearErrorMessages() }} severity="error">
          <ul>
            {mapModel.errorMessages.map((msg) => <li>{msg}</li>)}
          </ul>
        </Alert>
      </Snackbar>
    </ThemeProvider>
  )
});

export default GlobalView;