import React, { ReactDOMServer } from 'react';
import { Map, GeoJSON, TileLayer, withLeaflet } from 'react-leaflet';
import { PlottyGeotiffLayer } from "../components/GeoTiff";
import countries from "../config/countries.json";
// import marker from "../static/marker-icon.png";
// import marker2x from "../static/marker-icon-2x.png";
import Grid from '@material-ui/core/Grid';
import MapSideBar from "../components/MapSideBar";
import AreaPopup from "../components/legends/AreaPopup";
import Button from "@material-ui/core/Button";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import HeaderBar from "../components/HeaderBar";
import { observer } from "mobx-react-lite";
import theme from "../config/Theme.js";
import { ThemeProvider, makeStyles } from "@material-ui/core/styles";
import Drawer from '@material-ui/core/Drawer';
import FullscreenControl from 'react-leaflet-fullscreen';
import 'react-leaflet-fullscreen/dist/styles.css';
import GeoSearch from "../components/helpers/GeoSearch";
import StyleTwoToneIcon from '@material-ui/icons/StyleTwoTone';
import DataViewDrawer from "../components/DataViewDrawer";
import ViewModuleTwoToneIcon from '@material-ui/icons/ViewModuleTwoTone';
import PublicTwoToneIcon from '@material-ui/icons/PublicTwoTone';
import ColorLegend from "../components/legends/ColorLegend";
import JMPLegend from "../components/legends/JMPLegend";
import InfoLegend from "../components/legends/InfoLegend";
import ScenarioMetadataModal from '../components/dialogs/ScenarioMetadataModal';
import SummaryData from '../components/SummaryData';
import InfoModal from '../components/dialogs/InfoModal';
import DisclaimerModal from '../components/dialogs/DisclaimerModal';
import {BounceLoader} from 'react-spinners';
import DialogDefault from 'react-leaflet-dialog';
import "leaflet-geotiff";
import "leaflet-geotiff/leaflet-geotiff-plotty";
import PrintControlDefault from 'react-leaflet-easyprint';
import { GeoJSONFillable, Patterns } from 'react-leaflet-geojson-patterns';
import GenerateReportModal from '../components/dialogs/GenerateReportModal';
import { getColor } from 'reaviz';


const useStyles = makeStyles(theme => ({
  offset: theme.mixins.toolbar,
}))

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const Dialog = withLeaflet(DialogDefault);

const PrintControl = withLeaflet(PrintControlDefault);

const MapView = observer(({ context }) => {

  const mapModel = context;

  const position = [1.3733, 32.2903];

  const getStyle = (feature, layer) => {
    var worldDataMatch = mapModel.worldData.filter(record => { return record.gid===feature.id });
    
    var countryLevelData = (worldDataMatch.length > 0) ? (worldDataMatch[0]['notes1'] === ''): false ;
    
    var highResolutionData = mapModel.hasHighResolutionData(feature.id);
    var color = '';
    if (!mapModel.scenarioMode) {
      if (countryLevelData) {
        if (highResolutionData) {
        color = '#31d4ae'; 
        }
        else {
          color = '#42919e';
        }
      }
      else {
        color = '#fff';
      }
    }
    else {
      color = 'rgba(0, 0, 0, 0)';
    }
    return {
      color: (mapModel.scenarioMode && (mapModel.selectedAreasList.includes(feature.id) ||
      mapModel.selectedAreasList.includes(feature.properties["GID_" + mapModel.resolution]) ||
      mapModel.focusedCountry === feature.id)) ? '#42919e':'#003347',
      fillPattern: (mapModel.scenarioMode && mapModel.scenarios[mapModel.activeScenario].payload.outputRecords.length === 0 && (mapModel.selectedAreasList.includes(feature.id) ||
      mapModel.selectedAreasList.includes(feature.properties["GID_" + mapModel.resolution]) ||
      mapModel.focusedCountry === feature.id)) ? Patterns.StripePattern({
        x: 7,
        y: 7,
        radius: 5,
        fill: true,
        width: 15,
        height: 15,
        color: "#42919e",
        key: "stripe",
        angle: 45
      }) : '',
      weight: (mapModel.scenarioMode && (mapModel.selectedAreasList.includes(feature.id) ||
      mapModel.selectedAreasList.includes(feature.properties["GID_" + mapModel.resolution]) ||
      mapModel.focusedCountry === feature.id)) ? 4: 1,
      fillColor: color,
      fillOpacity: ((mapModel.selectedAreasList.includes(feature.id) ||
        mapModel.selectedAreasList.includes(feature.properties["GID_" + mapModel.resolution]) ||
        mapModel.focusedCountry === feature.id) && !mapModel.scenarioMode
        ) ? 0.85 : 0.5
    }
  }

  const belongsToSelection = (feature) => {
    return (mapModel.selectedAreasList.includes(feature.properties.name) ||
    mapModel.selectedAreasList.includes(feature.properties["GID_" + mapModel.resolution]) ||
    mapModel.focusedCountry === feature.properties.id);
  }

  const getAreaStyle = (feature, layer) => {
    return {
      color: (mapModel.scenarioMode && belongsToSelection(feature)) ? '#42919e':'#003347',
      weight: (mapModel.scenarioMode && belongsToSelection(feature)) ? 4: 1,
      fillPattern: (mapModel.scenarioMode && mapModel.scenarios[mapModel.activeScenario].payload.outputRecords.length === 0 && belongsToSelection(feature)) ? Patterns.StripePattern({
        x: 7,
        y: 7,
        radius: 5,
        fill: true,
        width: 15,
        height: 15,
        color: "#42919e",
        key: "stripe",
        angle: 45
      }) : '',
      fillColor: (belongsToSelection(feature) ? 
         (mapModel.scenarioMode) ? (mapModel.resolution === 4 && mapModel.scenarios[mapModel.activeScenario].payload.outputRecords.length !== 0) ? getOutputColor(feature) : 'rgba(0, 0, 0, 0)' : '#003347': 'rgba(0, 0, 0, 0)' ),
      fillOpacity: (belongsToSelection(feature) && !mapModel.scenarioMode
        ) ? 0.85 : 0.5
    }
  }

  const getOutputColor = (feature) => {
    const value = mapModel.scenarios[mapModel.activeScenario].payload.outputRecords.filter( record => record.subarea === feature.properties['NAME_4'])[0].total_log;
    const rainbow = ['#96005A', '#4B0091', '#0000C8', '#000DE4', '#0019FF', '#0059FF', '#0098FF', '#16CCCB', '#2CFF96', '#62FF4B', '#97FF00', '#CBF500', '#FFEA00', '#FFAD00', '#FF6F00', '#FF3800', '#FF0000'];
    if (value) {
      return rainbow[Math.floor(value)];
    }
    return 'rgba(0, 0, 0, 0)';
  }


  var lastClickedCountry;
  var lastClickedArea;

  

  const countryFeatureBehavior = (feature, layer) => {
    layer.on('mouseover', function () {
      this.setStyle({
        'fillOpacity': 0.7
      });
    });
    layer.on('mouseout', function () {
      if (mapModel.geofencingMode) {
        if (mapModel.selectedAreasList.indexOf(layer.feature.id) === -1) {
          this.setStyle({
            'fillOpacity': 0.5
          });
        }
        else {
          this.setStyle({
            'fillOpacity': 0.85
          });
        }
      }
      else {
        if (layer.feature.id != mapModel.focusedCountry) {
          this.setStyle({
            'fillOpacity': 0.5
          });
        }
        else {
          this.setStyle({
            'fillOpacity': 0.85
          });
        }

      }

    });

    layer.on('click', function (e) {
      var layer = e.target;
      if (mapModel.geofencingMode) {
        if (mapModel.selectedAreasList.indexOf(layer.feature.id) !== -1) {
          mapModel.removeAreaByKey(mapModel.selectedAreasList.indexOf(layer.feature.id));
        }
        else {
          var worldDataMatch = mapModel.worldData.filter(record => { return record.gid===feature.id });
          var countryLevelData = (worldDataMatch.length > 0) ? (worldDataMatch[0]['notes1'] === ''): false ;
          if (countryLevelData) {
            mapModel.addArea(layer.feature.id);
            mapModel.addAreaToDictionary(layer.feature.id, layer.feature.properties.name);
          }
        }
        mapModel.countriesRef.current.leafletElement.resetStyle(layer);
        mapModel.countriesRef.current.leafletElement.resetStyle(lastClickedCountry);
      }
      else {
        if (!mapModel.scenarioMode) {
          mapModel.setFocusedCountry(layer.feature.id);
          mapModel.countriesRef.current.leafletElement.resetStyle(lastClickedCountry);
        }
      }

      lastClickedCountry = layer;
    });
  }


  const areaFeatureBehavior = (feature, layer) => {
    var areaIndex = mapModel.selectedAreasList.indexOf(layer.feature.properties["GID_" + mapModel.resolution]);
    if (!mapModel.scenarioMode) {
  
      layer.on('mouseover', function() {
        if (!mapModel.scenarioMode) {
            this.setStyle({
              'fillOpacity': 0.6
            });
          
        }
        });
      layer.on('mouseout', function() {
        if (mapModel.geofencingMode) {
          // if (areaIndex === -1) {
          //   this.setStyle({
          //     'fillOpacity': 0.1
          //   });
          // }
          // else {
            this.setStyle({
              'fillOpacity': 0.85
            });
          // }
        }
      });
    }

    
  }

  const printOptions = {
    position: 'topright',
    sizeModes: ['A4Portrait', 'A4Landscape'],
    hideControlContainer: false
  };

  return (
    <ThemeProvider theme={theme()}>



      <Grid container style={{ height: '100vh' }}>
        <HeaderBar context={mapModel}></HeaderBar>
  <Grid item xs={12} md={12} lg={12} style={{ height: '64px' }}></Grid>

        <Grid item xs={12} 
          md={(!mapModel.summaryMode) ? 8 : 6 } 
          lg={(!mapModel.summaryMode) ? 7 : 5 } 
          style={{ height: '100%', backgroundColor: (mapModel.worldData.length > 0) ? "#fff":"#eee",
          transition: "0.5s"
            // transition: theme.transitions.create("all", {
            // easing: theme.transitions.easing.sharp, 
            // duration: theme.transitions.duration.leavingScreen,
            // })
        }}>
          {(mapModel.worldData.length > 0 && !mapModel.summaryMode && !mapModel.animationRendering) ?
          
          <Map
            center={position}
            style={{ height: '100%', width:'100%' }}
            zoom={2}
            ref={mapModel.mapRef}
            maxBounds={mapModel.mapBounds}
            className={(mapModel.scenarioMode ? 'scenarios' : '')}
            whenReady={() => {
              if (mapModel.scenarioMode) {
                mapModel.flyToBounds();
              }; 
              mapModel.toggleInfoMapDialogState(false);
              if (!mapModel.scenarioMode && !mapModel.summaryMode && !mapModel.geofencingMode ) {
                mapModel.toggleJMPDialogState(true);
              }
              else {
                mapModel.toggleJMPDialogState(false);
              }
              if (mapModel.scenarioMode && mapModel.scenarios.filter((scenario) => scenario.payload.geoTiff !== '').length > 0) {
                mapModel.toggleColorLegendState(true);
              }
              else {
                mapModel.toggleColorLegendState(false);
              }
            }}
          >
            
            
            <JMPLegend model={mapModel} />
            <ColorLegend model={mapModel} position='topleft' anchor={[0,0]}/>
            <InfoLegend map={mapModel}/>
            
            <FullscreenControl position="topright" />
            <PrintControl ref={(ref) => { mapModel.setPrintControl(ref) }} {...printOptions} />
            {(!mapModel.scenarioMode && !mapModel.geofencingMode) &&
              <GeoSearch countriesRef={mapModel.countriesRef} model={mapModel} />
            }
            <TileLayer
              url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
              attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
            />
            {(mapModel.scenarioMode) &&
              mapModel.scenarios.map((scenario) => {
                if (scenario.payload.geoTiff != '' && mapModel.activeScenario == scenario.id) {
                  return <PlottyGeotiffLayer
                    url={scenario.payload.geoTiff}
                    options={{
                      displayMin: 0,//4.68986,
                      displayMax: 16.6,
                      name: "Pathogen Emissions",
                      colorScale: "rainbow",
                      opacity: 0.3,
                      clampLow: false,
                      clampHigh: true
                    }}
                  />
                }
                return null;
              })
            }


            {(mapModel.searchedCountry === '') &&
              <GeoJSONFillable
                data={countries}
                style={getStyle}
                onEachFeature={countryFeatureBehavior}
                ref={mapModel.countriesRef}
                onClick={(e) => mapModel.addPopup(e) }
              >
                  {/* {(mapModel.popup && 
                        mapModel.scenarioMode && 
                        mapModel.scenarios[mapModel.activeScenario].payload.outputRecords.length > 0 ) && */}
                        
                          <AreaPopup context={mapModel}/>
                  {/* } */}
              
              </GeoJSONFillable>

            }

            {(mapModel.resolutionsData && !mapModel.resolutionDataLoading) &&
                 <GeoJSONFillable
                        data={mapModel.resolutionsData}
                        style={getAreaStyle}
                        key={mapModel.resolution}
                        onEachFeature={areaFeatureBehavior}
                        onClick={(e) => mapModel.addPopup(e)}
                        ref={mapModel.areasRef}
                      >
                        {(mapModel.popup && 
                        mapModel.scenarioMode && 
                        mapModel.scenarios[mapModel.activeScenario].payload.outputRecords.length > 0 ) &&
                        
                          <AreaPopup context={mapModel}/>
                        }

                      </GeoJSONFillable>
                    }
                
            



            
            {(mapModel.mapLoading || mapModel.loadingTiff) &&
              <div className="loading-overlay">
                <div className="loading-overlay-content">
                  <BounceLoader color="#fff"/>
                  {(mapModel.mapLoading) ? <h5>Loading high resolution data...</h5>:<h5>Running model...</h5>}
                </div>
              </div>
            }

            

          </Map>:
          <div style={{padding: '20px'}}>
            {(mapModel.summaryMode) ?
              <SummaryData context={mapModel}/>:
              // <SummaryChart context={mapModel}/>:
              <div className="loading-overlay-inverted">
                <div className="loading-overlay-content-inverted">
                  <BounceLoader color="#42919e"/>
                  <h5>Compiling world data...</h5>
                </div>
              </div>
          }
          </div>
          }

          {(mapModel.scenarios.length > 0) &&
            <div>
              <Button size="large" startIcon={<ViewModuleTwoToneIcon />} style={{ position: 'fixed', left: '20px', bottom: '20px', zIndex: '1000' }} variant='contained' onClick={() => mapModel.toggleDataView()}>View detailed Data</Button>
          <Button size="large" disabled={mapModel.animationRendering || mapModel.baselineScenario === -1} startIcon={(mapModel.summaryMode) ? <PublicTwoToneIcon />: <StyleTwoToneIcon />} style={{ position: 'fixed', left: '220px', bottom: '20px', zIndex: '1000' }} color='secondary' variant='contained' onClick={() => mapModel.toggleSummaryMode()}>{(mapModel.summaryMode) ? 'Back to map': 'Results summary'}</Button>

              <Drawer anchor='bottom' open={mapModel.dataViewEnabled} onClose={() => mapModel.toggleDataView()}>
                <DataViewDrawer scenario={mapModel.scenarios[mapModel.activeScenario]} />

              </Drawer>
            </div>
          }
        </Grid>


        <MapSideBar context={mapModel} />
      </Grid>
      {(mapModel.scenarioMode && mapModel.scenarios.length > 0 && mapModel.activeScenario >= 0) &&
        <ScenarioMetadataModal map={mapModel} />
      }
      
      <InfoModal context={mapModel} />
      <DisclaimerModal context={mapModel} />
      <GenerateReportModal context={mapModel} />
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        open={mapModel.errorMessages.length > 0}
      >
        <Alert onClose={() => { mapModel.clearErrorMessages() }} severity="error">
          <ul>
            {mapModel.errorMessages.map((msg) => <li>{msg}</li>)}
          </ul>
        </Alert>
      </Snackbar>
    </ThemeProvider>
  )
});

export default MapView;